package services;

public class WalletServiceTest {

//	@Mock
//	private IWalletDao walletDao;
//	@Mock
//	private ICustomerDao customerDao;
//	@Mock
//	private IOrderDao orderDao;
//	@Mock
//	private IRestaurantNotificationService restaurantNotificationService;
//	@Mock
//	private IRestaurantService restaurantService;
//
//	@InjectMocks
//	private WalletService walletService;
//
//	@Before
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		walletService = new WalletService(walletDao, customerDao, orderDao, restaurantNotificationService, restaurantService);
//	}
//
//	@Test
//	public void IfNullPaymentIsPassed() throws BaseException {
//		walletService.addPayment(null);
//		verify(customerDao, never()).getTById(0);
//	}
//
//	@Test
//	public void IfNoCustomerFound() {
//		try {
//			walletService.addPayment(getEmptyPaymentRequest());
//		} catch (BaseException e) {
//			assertEquals(e.getResponseCode(), ResponseCode.INVALID_CUSTOMER_ID);
//		}
//		verify(customerDao, atMost(1)).getTById(0);
//	}
//
//	@Test
//	public void IfIncorrectJsonMetaData() {
//		try {
//			AddPaymentRequestDto addPaymentRequestDto = getEmptyPaymentRequest();
//			addPaymentRequestDto.setCustomerId(1);
//			when(customerDao.getTById(1)).thenReturn(new CustomerEntity());
//			walletService.addPayment(addPaymentRequestDto);
//		} catch (BaseException e) {
//			assertEquals(e.getResponseCode(), ResponseCode.INVALID_PAYMENT_JSON_FORMAT);
//		}
//		verify(customerDao, atMost(1)).getTById(1);
//	}
//	
//	@Captor
//    private ArgumentCaptor<WalletTransactionEntity> capturedTransaction;
//	@Captor
//	private ArgumentCaptor<CustomerEntity> capturedCustomer;
//
//	@Test
//	public void testDeposit() throws BaseException {
//		AddPaymentRequestDto addPaymentRequestDto = getCheckinPaymentRequestForDeposit();
//		CustomerEntity customer = getCustomerEntity();
//		addPaymentRequestDto.setCustomerId(customer.getId());
//
//		when(customerDao.getTById(customer.getId())).thenReturn(customer);
//		when(orderDao.getByCheckinId(addPaymentRequestDto.getCustomerSessionId())).thenReturn(null);
//		
//		walletService.addPayment(addPaymentRequestDto);
//		
//		verify(customerDao, atMost(1)).getTById(customer.getId());
//		verify(walletDao).saveT(capturedTransaction.capture());
//		verify(customerDao).updateT(capturedCustomer.capture());
//		
//		assertEquals(TransactionType.DEPOSIT, capturedTransaction.getValue().getTransactionType());
//		assertEquals(customer, capturedCustomer.getValue());
//	}
//	
//	@Test
//	public void testForOrderId() throws BaseException {
//		AddPaymentRequestDto addPaymentRequestDto = getOrderPaymentRequestForDeposit();
//		CustomerEntity customer = getCustomerEntity();
//		OrderEntity order = getSingleOrderEntity();
//		
//		customer.setWalletBalance(0);
//		addPaymentRequestDto.setCustomerId(customer.getId());
//		order.setCheckinId(addPaymentRequestDto.getCustomerSessionId());
//		
//		when(customerDao.getTById(customer.getId())).thenReturn(customer);
//		when(orderDao.getTById(addPaymentRequestDto.getOrderId())).thenReturn(order);
//		
//		// when paid amount is exactly equal to order amount
//		order.setAmount(300);
//		order.setRemainingAmount(300);
//		order.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
//		walletService.addPayment(addPaymentRequestDto);
//		assertEquals(PaymentStatus.PAID.getCode(), order.getPaymentStatusCode());
//		verify(customerDao, times(2)).updateT(capturedCustomer.capture());
//		assertEquals(0, customer.getWalletBalance(), 0);
//		assertEquals(0, order.getRemainingAmount(), 0);
//		
//		// when paid amount is greater than order amount, blanace should be there in wallet
//		order.setRemainingAmount(200);
//		order.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
//		walletService.addPayment(addPaymentRequestDto);
//		assertEquals(PaymentStatus.PAID.getCode(), order.getPaymentStatusCode());
//		verify(customerDao, times(4)).updateT(capturedCustomer.capture());
//		assertEquals(0, customer.getWalletBalance(), 100);
//		assertEquals(0, order.getRemainingAmount(), 0);
//		
//		// when paid amount is less than order amount, order should be partial, and have remaining amount
//		customer.setWalletBalance(0);
//		order.setRemainingAmount(400);
//		order.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
//		walletService.addPayment(addPaymentRequestDto);
//		assertEquals(PaymentStatus.PARTIAL_PAID.getCode(), order.getPaymentStatusCode());
//		verify(customerDao, times(6)).updateT(capturedCustomer.capture());
//		assertEquals(0, customer.getWalletBalance(), 0);
//		assertEquals(0, order.getRemainingAmount(), 100);
//		
//		// when order is already partially paid
//		customer.setWalletBalance(50);
//		order.setRemainingAmount(100);
//		order.setPaymentStatusCode(PaymentStatus.PARTIAL_PAID.getCode());
//		walletService.addPayment(addPaymentRequestDto);
//		assertEquals(PaymentStatus.PAID.getCode(), order.getPaymentStatusCode());
//		verify(customerDao, times(8)).updateT(capturedCustomer.capture());
//		assertEquals(0, customer.getWalletBalance(), 250);
//		assertEquals(0, order.getRemainingAmount(), 0);
//		
//		//order unpaid, amount is not sufficient, no wallet balance
//		PaytmResponse paytmResponse = getPaytmResponse();
//		paytmResponse.setTxnAmount("0.00");
//		addPaymentRequestDto.setTransactionMetaData(paytmResponse);
//		customer.setWalletBalance(0);
//		order.setRemainingAmount(200);
//		order.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
//		walletService.addPayment(addPaymentRequestDto);
//		assertEquals(PaymentStatus.PENDING.getCode(), order.getPaymentStatusCode());
//		verify(customerDao, times(10)).updateT(capturedCustomer.capture());
//		assertEquals(0, customer.getWalletBalance(), 0);
//		assertEquals(0, order.getRemainingAmount(), 200);
//	}
//
//	private AddPaymentRequestDto getEmptyPaymentRequest() {
//		AddPaymentRequestDto response = new AddPaymentRequestDto();
//		response.setTransactionMetaData(null);
//		response.setMetaData("{}");
//		response.setPaymentGatewayName("PAYTM");
//		return response;
//	}
//
//	private AddPaymentRequestDto getCheckinPaymentRequestForDeposit() {
//		AddPaymentRequestDto response = getEmptyPaymentRequest();
//		response.setIdType(PaymentIdType.SESSION_ID.getCode());
//		response.setCustomerSessionId(1);
//		response.setTransactionMetaData(getPaytmResponse());
//		return response;
//	}
//	
//	private AddPaymentRequestDto getOrderPaymentRequestForDeposit() {
//		AddPaymentRequestDto response = getEmptyPaymentRequest();
//		response.setIdType(PaymentIdType.ORDER_ID.getCode());
//		response.setCustomerSessionId(1);
//		response.setOrderId(1);
//		PaytmResponse paytmResponse = getPaytmResponse();
//		paytmResponse.setTxnAmount("300.00");
//		response.setTransactionMetaData(paytmResponse);
//		return response;
//	}
//
//	private CustomerEntity getCustomerEntity() {
//		CustomerEntity response = new CustomerEntity();
//		response.setId(1);
//		response.setWalletBalance(100);
//		return response;
//	}
//	
//	private OrderEntity getSingleOrderEntity() {
//		OrderEntity response = new OrderEntity();
//		response.setId(1);
//		response.setOrderName("Test order 1");
//		response.setStatusCode(OrderStatus.COMPLETED.getCode());
//		response.setData("");
//		response.setAmount(300);
//		response.setRemainingAmount(300);
//		response.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
//		return response;
//	}
//	
//	private PaytmResponse getPaytmResponse() {
//		PaytmResponse response = new PaytmResponse();
//		response.setBankTxnId("1049101");
//		response.setmId("SUNKAS04754552199447");
//		response.setCurrency("INR");
//		response.setBankName("ORDER200003015-SES-124");
//		response.setPaymentMode("PPI");
//		response.setCheckSum("a0q3KYRnDnu5QNjsPCbAia7ToPwwQ/GJQn4Gm06sBKPqOd3GiwVMS+neVU+MT8aut5pJyuCocWqDZBfSJx+0LCFtCv1M/xclJxce5UsFAgQ=");
//		response.setStatus("");
//		response.setGatewayName("WALLET");
//		response.setResponseCode("01");
//		response.setResponseMsg("Txn Successful.");
//		response.setTxnDate("2018-02-12 14:09:50.0");
//		response.setTxnId("70000618042");
//		response.setTxnAmount("1900.00");
//		return response;
//	}
}
