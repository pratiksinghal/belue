package api.belue.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;

import api.belue.constants.FileType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FileUtil {
	private static String SAVE_FOLDER = System.getProperty("catalina.home") + File.separator;
	private static String IMAGES_FOLDER = SAVE_FOLDER + "images" + File.separator;

	static {
		File fileDir = new File(IMAGES_FOLDER);
		if (!fileDir.exists())
			fileDir.mkdirs();
	}

	public static String saveFile(MultipartFile file, FileType fileType) throws IOException {
		return saveBytesAsFile(file.getBytes(), file.getOriginalFilename(), fileType);
	}
	
	public static String saveBytesAsFile(byte[] bytesToSave, String fileName, FileType fileType) throws IOException {
		if(bytesToSave == null || fileName == null) {
			return null;
		}
		Path path = null;
		switch (fileType) {
		case IMAGE:
			path = Paths.get(IMAGES_FOLDER + fileName);
			Files.write(path, bytesToSave);
		default:
			break;
		}
		return (path != null) ? path.toAbsolutePath().toString() : null;
	}
	
	@SuppressWarnings("rawtypes")
	public static Map uploadToCloudinary(Cloudinary client, Object filePath, Map uploadParams) throws IOException, RuntimeException {
		if(client == null || filePath == null) {
			return null;
		}
		if(uploadParams == null) {
			uploadParams = new HashMap();
		}
		Map uploadResult = client.uploader().upload(filePath, uploadParams);
		return uploadResult;
	}
	
	public static byte[] downloadFile(String fileUrl, FileType fileType) throws IOException {
		if(fileUrl == null) {
			return null;
		}
		byte[] responseBytes = null;
		switch(fileType) {
		case IMAGE :
			Request request = new Request.Builder().url(fileUrl).get().build();
			Response response = new OkHttpClient().newCall(request).execute();
			responseBytes = response.body().bytes();
		default:
			break;
		}
		return responseBytes;
	}
}
