package api.belue.utils;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public abstract class CodeUtil {
	
	public static <E,R> List<R> getFieldAsList(List<E> list, Function<E, R> fieldGiver) {
		if(CollectionUtils.isEmpty(list) || fieldGiver == null) {
			return null;
		}
		return list.stream().parallel()
				.map(fieldGiver)
				.collect(Collectors.toList());
	}
	
	public static <E,R> List<R> getFieldAsList(List<E> list, Predicate<E> filter, Function<E, R> fieldGiver) {
		if(CollectionUtils.isEmpty(list) || filter == null || fieldGiver == null) {
			return null;
		}
		return list.stream().parallel()
				.filter(filter)
				.map(fieldGiver)
				.collect(Collectors.toList());
	}

	public static String formatAsCurrency(double amount) {
		return NumberFormat.getCurrencyInstance(new Locale("en", "in")).format(amount);
	}

}
