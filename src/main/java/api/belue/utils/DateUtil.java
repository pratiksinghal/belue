package api.belue.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static final String TIMESTAMP_PATTERN = "dd-MM-yyyy HH:mm:ss";
	public static final String DATE_PATTERN = "dd-MM-yyyy";
	
	public static String formateDate(Date date, String format) {
		if(date == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static Date parseDate(String date, String format) {
		try {
			return new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

}
