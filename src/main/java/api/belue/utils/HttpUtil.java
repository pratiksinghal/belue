package api.belue.utils;

import java.io.IOException;
import java.net.MalformedURLException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpUtil {

	public interface ContentType {
		public String APPLICATION_JSON = "application/json";
	}
	
	public enum RequestMethod {
		POST;
	}

	public static class HttpBuilder {

		private OkHttpClient client;
		private Builder requestBuilder;
		private RequestMethod method;

		public HttpBuilder(String url) throws MalformedURLException, IOException {
			 this.client = new OkHttpClient();
			 requestBuilder = new Request.Builder();
			 requestBuilder.url(url);
		}

		public HttpBuilder setRequestProperty(String key, String value) {
			requestBuilder.addHeader(key, value);
			return this;
		}

		public HttpBuilder setContentType(String contentType) {
			setRequestProperty("Content-Type", contentType);
			return this;
		}
		
		public HttpBuilder setRequestMethod(RequestMethod post) {
			method = post;
			return this;
		}

		public HttpBuilder setData(String data, String contentType){
			if(method == null) {
				method = RequestMethod.POST;
			}
			RequestBody body = RequestBody.create(MediaType.parse(contentType), data);
			switch(method) {
			case POST :
				requestBuilder.post(body);
			}
			return this;
		}

		public Response sendRequest() throws IOException {
			Request request = requestBuilder.build();
			return client.newCall(request).execute();
		}

	}

	public static HttpBuilder builder(String url) throws MalformedURLException, IOException {
		return new HttpBuilder(url);
	}

}
