package api.belue.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;

public class JsonUtils {
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public static<T> String getJson(T data){
		try {
			return objectMapper.writeValueAsString(data);
		} catch (NullPointerException npe) {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static<T> T fromJson(String json, Class<T> clazz){
		try {
			return objectMapper.readValue(json, clazz);
		} catch (NullPointerException npe) {
			
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return null;
	}

	public static String convertListToJSON(List<?> listToConvert) throws BaseException {
		try {
			return (new ObjectMapper()).writeValueAsString(listToConvert);
		} catch (NullPointerException npe) {
			throw new BaseException(ResponseCode.ERROR_IN_PARSING);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException(ResponseCode.ERROR_IN_PARSING);
		}
	}

	public static <T> List<T> convertJsonToList(String json, Class<T> classT) {
		List<T> response = null;
		try {
			CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(List.class, classT);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			response = mapper.readValue(json, typeReference);
		} catch (NullPointerException npe) {
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return response;
	}
	
	public static class JsonBuilder {

		private Map<Object, Object> data;

		public Map<Object, Object> getData() {
			return data;
		}

		public JsonBuilder() {
			this.data = new HashMap<>();
		}

		public JsonBuilder add(Object key, Object value) {
			data.put(key, value);
			return this;
		}

		public JsonBuilder add(Object key, JsonBuilder builder) {
			data.put(key, builder.getData());
			return this;
		}

		public String build() {
			return getJson(data);
		}

	}

	public static JsonBuilder builder() {
		return new JsonBuilder();
	}
}
