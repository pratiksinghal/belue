package api.belue.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import okhttp3.Response;

import api.belue.constants.FCMNotifTo;
import api.belue.constants.Params;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;

public abstract class FCMNotifUtil {
	public static String send(Object data, List<String> tokens) throws IOException, BaseException {
		return send(data, tokens, FCMNotifTo.TO_MOB);
	}

	public static String trySend(Object data, List<String> tokens, FCMNotifTo notifType) {
		try {
			return send(data, tokens, notifType);
		} catch (Exception ex) {
			return null;
		}
	}

	public static String send(Object data, List<String> tokens, FCMNotifTo notifType)
			throws IOException, BaseException {
		tokens = filterTokens(tokens);
		String notificationJson = getNotificationJson(data, tokens, notifType);
		Map<String, String> fcmProperties = PropertiesUtil.getPropertiesAsMap(Params.Fcm.FCM_PROPS_FILE);
		Response response = HttpUtil.builder(fcmProperties.get(Params.Fcm.URL))
				.setRequestMethod(HttpUtil.RequestMethod.POST)
				.setRequestProperty("Authorization", "key=" + fcmProperties.get(Params.Fcm.AUTH_KEY))
				.setContentType(HttpUtil.ContentType.APPLICATION_JSON)
				.setData(notificationJson, HttpUtil.ContentType.APPLICATION_JSON)
				.sendRequest();
		return response.body().string();
	}

	private static List<String> filterTokens(List<String> tokens) throws BaseException {
		if(CollectionUtils.isEmpty(tokens)) {
			throw new BaseException(ResponseCode.NO_NOTIFICATION_TOKEN_FOUND);
		}
		tokens = tokens.parallelStream()
				.filter(s -> s != null)
				.collect(Collectors.toList());
		if(CollectionUtils.isEmpty(tokens)) {
			throw new BaseException(ResponseCode.NO_NOTIFICATION_TOKEN_FOUND);
		}
		return tokens;
	}

	private static String getNotificationJson(Object data, List<String> tokens, FCMNotifTo notifType) {
		String notificationJson = null;
		if(notifType == FCMNotifTo.TO_MOB) {
			notificationJson = JsonUtils.builder()
					.add("registration_ids", tokens)
					.add("data", data)
					.build();
		} else if(notifType == FCMNotifTo.TO_WEB) {
			notificationJson = JsonUtils.builder()
					.add("registration_ids", tokens)
					.add("notification", data)
					.build();
		}
		return notificationJson;
	}

}
