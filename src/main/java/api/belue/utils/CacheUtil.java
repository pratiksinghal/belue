package api.belue.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class CacheUtil<T,R> {
	
	private Map<T,R> data;
	
	public CacheUtil() {
		this.data = new ConcurrentHashMap<>();
	}

	public void add(T key, R value) {
		data.put(key, value);
	}
	
	public R get(T key) {
		return data.get(key);
	}
	
	public R getAndAdd(T key, Function<T, R> fetcher) {
		return data.computeIfAbsent(key, fetcher);
	}
	
	public void clear() {
		data.clear();
	}


}
