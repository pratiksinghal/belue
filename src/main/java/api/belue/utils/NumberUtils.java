package api.belue.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public abstract class NumberUtils {
	public static double getPercentageValue(double value, double percentage) {
		return (value * percentage) / 100;
	}
	
	public static double roundOffValue(double value) {
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_EVEN);
		return Double.parseDouble(df.format(value));
	}
}
