package api.belue.exceptions;

import api.belue.constants.ResponseCode;

@SuppressWarnings("serial")
public class BaseException extends Exception {
	
	private ResponseCode responseCode;

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public BaseException(ResponseCode responseCode) {
		super();
		this.responseCode = responseCode;
	}
	
}
