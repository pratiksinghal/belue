package api.belue.exceptions;

import api.belue.constants.ResponseCode;

@SuppressWarnings("serial")
public class DtoValidationException extends BaseException {

	public DtoValidationException(ResponseCode responseCode) {
		super(responseCode);
	}

}
