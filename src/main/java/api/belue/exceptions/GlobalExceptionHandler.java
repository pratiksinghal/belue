package api.belue.exceptions;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import api.belue.constants.ResponseCode;
import api.belue.response.BaseResponse;

@ControllerAdvice
public class GlobalExceptionHandler extends DefaultHandlerExceptionResolver {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
	@ResponseBody
	public <T> BaseResponse<T> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException me,
			HttpServletResponse response) {
		logger.error("HttpMediaTypeNotSupportedException ", me);
		
		return new BaseResponse<T>(true, "400", me.getMessage(), null);
	}
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = HttpMessageNotReadableException.class)
	@ResponseBody
	public <T> BaseResponse<T> handleJacksonException(HttpMessageNotReadableException me,
			HttpServletResponse response) {
		logger.error("HttpMessageNotReadableException ", me);
		if(me.getCause() instanceof JsonMappingException || me.getCause() instanceof JsonParseException) {
			return new BaseResponse<T>(true, null, ResponseCode.INVALID_JSON_FORMAT);
		}
		return new BaseResponse<T>(true, null, ResponseCode.BAD_REQUEST);
	}

	@ExceptionHandler(value = BaseException.class)
	@ResponseBody
	public <T> BaseResponse<T> handleMyException(BaseException me, HttpServletResponse response) {
		logger.error("Base Exception", me);
		return new BaseResponse<T>(me);
	}

	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public <T> BaseResponse<T> handleAllException(Exception e, HttpServletResponse response) {
		logger.error("Global Exception", e);
		return new BaseResponse<T>(true, null, ResponseCode.ERROR, e.getMessage());
	}

}
