package api.belue.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.response.BaseResponse;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestExceptionHandler extends DefaultHandlerExceptionResolver {

	private static final Logger logger = LoggerFactory.getLogger(RequestExceptionHandler.class);

	@ExceptionHandler(value = ServletRequestBindingException.class)
	@ResponseBody
	public <T> BaseResponse<T> handleServletRequestBindingException(
			ServletRequestBindingException ex,
			HttpServletRequest request) {
		logger.error("ServletRequestBindingException ", ex);
		if(request.getHeader(PathMappings.HeaderValues.TOKEN) == null) {
			return new BaseResponse<>(true, null, ResponseCode.TOKEN_NOT_PRESENT);
		}
		return new BaseResponse<>(true, null, ResponseCode.ERROR);
	}

	@ExceptionHandler(value = MissingServletRequestPartException.class)
	@ResponseBody
	public <T> BaseResponse<T> handleMultipartException(
			MissingServletRequestPartException e,
			HttpServletResponse response) {
		if(e.getRequestPartName().equals(PathMappings.FileUpload.FILE_PARAM_VAR)) {
			return new BaseResponse<T>(true, null, ResponseCode.FILE_NOT_PRESENT);
		}
		logger.error("MultipartException ", e);
		return new BaseResponse<T>(true, null, ResponseCode.ERROR);
	}
}
