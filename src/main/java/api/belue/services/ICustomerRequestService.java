package api.belue.services;

import java.util.List;

import api.belue.constants.CustomerRequestStatus;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerRequestRequestDto;
import api.belue.request.dto.UpdateCustomerRequestStatusRequestDto;
import api.belue.response.dto.CustomerRequestReponseDto;

public interface ICustomerRequestService extends IAbstractService<CustomerRequestReponseDto, AddCustomerRequestRequestDto, UpdateCustomerRequestStatusRequestDto> {
	public List<CustomerRequestReponseDto> getAllByRestaurantIdAndStatus(int restaurantId, CustomerRequestStatus requestStatus) throws BaseException;
	public void cancelRequestBySession(int sessionId);
}
