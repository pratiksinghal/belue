package api.belue.services;

import java.util.List;

import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerSessionRequestDto;
import api.belue.request.dto.CustomerSessionCheckoutRequestDto;
import api.belue.request.dto.UpdateCustomerSessionRequestDto;
import api.belue.response.dto.CustomerSessionResponseDto;

public interface ICustomerSessionService {
	public CustomerSessionResponseDto addUserSession(AddCustomerSessionRequestDto sessionToAdd) throws Exception;
	public CustomerSessionResponseDto updateUserSession(UpdateCustomerSessionRequestDto sessionToAdd) throws Exception;
	public CustomerSessionResponseDto checkoutCustomer(CustomerSessionCheckoutRequestDto checkinId) throws Exception;
	public void deleteById(int customerSessionId) throws BaseException;
	public boolean isExist(int customerSessionId);
	public List<CustomerSessionResponseDto> getAll(SessionStatus sessionStatus, RoleType roleType, int userId) throws BaseException;
	public CustomerSessionResponseDto getById(int customerSessionId);
}
