package api.belue.services;

import java.util.List;

import api.belue.exceptions.BaseException;
import api.belue.pojo.MenuItemInOrderPojo;
import api.belue.request.dto.AddMenuItemRequestDto;
import api.belue.request.dto.UpdateMenuItemRequestDto;
import api.belue.response.dto.MenuItemResponseDto;

public interface IMenuItemService extends IAbstractService<MenuItemResponseDto, AddMenuItemRequestDto, UpdateMenuItemRequestDto> {
	public List<MenuItemResponseDto> getAllByMenuCategogoryId(int menuCategoryId) throws BaseException;
	public List<MenuItemResponseDto> getAllByMenuId(int menuId) throws BaseException;
	public void calculateOrderItemAmounts(List<MenuItemInOrderPojo> orderItem) throws BaseException;
}
