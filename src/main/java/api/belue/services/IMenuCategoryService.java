package api.belue.services;

import java.util.List;

import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddMenuCategoryRequestDto;
import api.belue.request.dto.UpdateMenuCategoryRequestDto;
import api.belue.response.dto.MenuCategoryResponseDto;

public interface IMenuCategoryService extends IAbstractService<MenuCategoryResponseDto, AddMenuCategoryRequestDto, UpdateMenuCategoryRequestDto> {
	public List<MenuCategoryResponseDto> getAllByMenuId(int menuId) throws BaseException;
}
