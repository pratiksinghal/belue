package api.belue.services;

import java.util.List;

import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddRestaurantTableRequestDto;
import api.belue.request.dto.UpdateRestaurantTableRequestDto;
import api.belue.response.dto.RestaurantTableResponseDto;

public interface IRestaurantTableService extends IAbstractService<RestaurantTableResponseDto, AddRestaurantTableRequestDto, UpdateRestaurantTableRequestDto> {
	public List<RestaurantTableResponseDto> getAllTableByRestaurantId(int id) throws BaseException;
}
