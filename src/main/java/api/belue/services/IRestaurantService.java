package api.belue.services;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import api.belue.entites.RestaurantEntity;
import api.belue.exceptions.BaseException;
import api.belue.pojo.RestaurantStat;
import api.belue.request.dto.AddRestaurantRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.ChangePasswordRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.RestaurantLoginRequestDto;
import api.belue.request.dto.UpdateRestaurantRequestDto;
import api.belue.response.dto.RestaurantLoginResponseDto;
import api.belue.response.dto.RestuarantResponseDto;

public interface IRestaurantService extends IAbstractService<RestuarantResponseDto, AddRestaurantRequestDto, UpdateRestaurantRequestDto> {
	public RestaurantLoginResponseDto login(RestaurantLoginRequestDto request) throws BaseException;

	void changePassword(ChangePasswordRequestDto request) throws BaseException;

	public RestaurantStat getStats(int id, Date startDate, Date endDate);

	public void addUpdateNotificationToken(AddUpdateNotificationTokenRequestDto request) throws BaseException;
	
	public String sendPushNotification(PushNotificationRequestDto request) throws IOException, BaseException;

	List<String> fetchTokens(List<Integer> restaurantIds) throws BaseException;

	public RestaurantEntity getEntityBySessionId(int checkinId);
}
