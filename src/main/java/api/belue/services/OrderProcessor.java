package api.belue.services;

import java.util.List;
import java.util.function.Consumer;

import api.belue.constants.PaymentStatus;
import api.belue.entites.OrderEntity;

public interface OrderProcessor {
	public double process(OrderEntity order, double currentWalletBalance, Consumer<OrderEntity> transactionAdder);

	public static double processOrders(List<OrderEntity> orders, double currentWalletBalance,
			OrderProcessor orderProcessor, Consumer<OrderEntity> transactionAdder) {
		for(OrderEntity order : orders) {
			currentWalletBalance = orderProcessor.process(order, currentWalletBalance, transactionAdder);
		}
		return currentWalletBalance;
	}

	public static OrderProcessor partialOrderProcessor = (order, currentWalletBalance,
			transactionAdder) -> order.getPaymentStatusCode() == PaymentStatus.PARTIAL_PAID.getCode()
					? setOrderPaid(order, currentWalletBalance, transactionAdder)
					: currentWalletBalance;

	public static OrderProcessor unpaidOrderProcessor = (order, currentWalletBalance,
			transactionAdder) -> order.getPaymentStatusCode() != PaymentStatus.PAID.getCode()
					? setOrderPaid(order, currentWalletBalance, transactionAdder)
					: currentWalletBalance;

	public OrderProcessor payOrderPartiallyProcessor = (order, currentWalletBalance, transactionAdder) -> {
		if(order.getPaymentStatusCode() != PaymentStatus.PAID.getCode() && currentWalletBalance > 0) {
			order.setPaymentStatusCode(PaymentStatus.PARTIAL_PAID.getCode());
			order.setRemainingAmount(order.getRemainingAmount() - currentWalletBalance);
			currentWalletBalance = 0;
			transactionAdder.accept(order);
		}
		return currentWalletBalance;
	};

	public static double setOrderPaid(OrderEntity order, double currentWalletBalance, Consumer<OrderEntity> transactionAdder) {
		if(order.getRemainingAmount() <= currentWalletBalance) {
			order.setPaymentStatusCode(PaymentStatus.PAID.getCode());
			currentWalletBalance -= order.getRemainingAmount();
			order.setRemainingAmount(0);
			transactionAdder.accept(order);
		}
		return currentWalletBalance;
	}

}
