package api.belue.services;

import java.util.List;

import api.belue.constants.FCMNotifTemplate;
import api.belue.entites.RestaurantNotificationsEntity;
import api.belue.exceptions.BaseException;
import api.belue.response.dto.RestaurantNotificationResposeDto;

public interface IRestaurantNotificationService {
	public void setRead(int restaurantId, boolean isRead) throws BaseException;
	public List<RestaurantNotificationResposeDto> getByRestaurantId(int restaurantId);
	public int save(RestaurantNotificationsEntity notif);
	void sendAndSave(FCMNotifTemplate template, String token, int restaurantId);
	List<RestaurantNotificationResposeDto> getByRestaurantIdAndReadStatus(int restaurantId, boolean readStatus);
}
