package api.belue.services;

import java.io.IOException;
import java.util.List;

import api.belue.entites.CustomerEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.CustomerLoginRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.UpdateCustomerRequestDto;
import api.belue.response.dto.CustomerLoginResponseDto;
import api.belue.response.dto.CustomerResponseDto;

public interface ICustomerService extends IAbstractService<CustomerResponseDto, AddCustomerRequestDto, UpdateCustomerRequestDto> {
	public CustomerLoginResponseDto loginCustomer(CustomerLoginRequestDto request) throws BaseException;

	public CustomerLoginResponseDto saveWithLoginResponse(AddCustomerRequestDto itemToSave)
			throws BaseException, IOException;

	public boolean isPhoneNumberExists(String phoneNumber) throws BaseException;

	public String sendPushNotification(PushNotificationRequestDto request) throws IOException, BaseException;

	public void addUpdateNotificationToken(AddUpdateNotificationTokenRequestDto request) throws BaseException;

	public CustomerEntity validateIsCustomerVerified(int customerId) throws BaseException;

	List<String> fetchTokens(List<Integer> customerIds) throws BaseException;
}
