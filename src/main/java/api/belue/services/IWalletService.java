package api.belue.services;

import java.util.List;

import api.belue.constants.TransactionType;
import api.belue.entites.WalletTransactionEntity;
import api.belue.exceptions.BaseException;
import api.belue.response.dto.TransactionResponseDto;

public interface IWalletService {
	public List<TransactionResponseDto> getTransactionsByCustomerIdAndType(int customerId, TransactionType type);
	public void saveTransaction(WalletTransactionEntity response);
	public WalletTransactionEntity getEntityById(int transactionId);
	public void deposit(WalletTransactionEntity txn) throws BaseException;
	public WalletTransactionEntity getByUniqueId(String uniqueId);
	public void update(WalletTransactionEntity txn);
}
