package api.belue.services.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.CustomerRequestStatus;
import api.belue.constants.CustomerRequestType;
import api.belue.constants.FCMNotifTemplate;
import api.belue.constants.FCMNotifTo;
import api.belue.constants.ResponseCode;
import api.belue.dao.ICustomerDao;
import api.belue.dao.ICustomerRequestDao;
import api.belue.dao.ICustomerSessionDao;
import api.belue.entites.CustomerRequestEntity;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.RestaurantEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.CustomerRequestMapper;
import api.belue.request.dto.AddCustomerRequestRequestDto;
import api.belue.request.dto.UpdateCustomerRequestStatusRequestDto;
import api.belue.response.dto.CustomerRequestReponseDto;
import api.belue.services.ICustomerRequestService;
import api.belue.services.ICustomerService;
import api.belue.services.IRestaurantNotificationService;
import api.belue.services.IRestaurantService;
import api.belue.services.IRestaurantTableService;
import api.belue.utils.CodeUtil;
import api.belue.utils.FCMNotifUtil;

@Service
public class CustomerRequestService implements ICustomerRequestService {

	@Autowired
	private ICustomerRequestDao customerRequestDao;
	@Autowired
	private ICustomerSessionDao customersessionDao;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IRestaurantTableService tableService;
	@Autowired
	private IRestaurantNotificationService restaurantNotificationService;
	@Autowired
	private IRestaurantService restaurantService;
	@Autowired
	private ICustomerDao customerDao;

	@Override
	public List<CustomerRequestReponseDto> getAll() {
		return getResponseListOrNull(customerRequestDao.getAllT());
	}

	@Override
	public CustomerRequestReponseDto getById(int requestId) {
		return getResponseOrNull(customerRequestDao.getTById(requestId));
	}

	@Override
	public CustomerRequestReponseDto save(AddCustomerRequestRequestDto itemToSave) throws BaseException, IOException {
		CustomerRequestEntity requestToAdd = CustomerRequestMapper.mapAddRequestToEntity(itemToSave);
		if(requestToAdd == null) {
			return null;
		}
		if(!customersessionDao.isExist(itemToSave.getCheckinId())) {
			throw new BaseException(ResponseCode.INVALID_ORDER_SESSION_ID);
		}
		customerRequestDao.saveT(requestToAdd);
		sendNewRequestNotification(requestToAdd);
		return getById(requestToAdd.getId());
	}

	private void sendNewRequestNotification(CustomerRequestEntity requestToAdd) {
		FCMNotifTemplate notifTemplate = FCMNotifTemplate.getCustomerRequestTemplate();
		String msg = "Your request for %s is accepted";
		notifTemplate.setBody(String.format(msg, CustomerRequestType.getByCode(requestToAdd.getType()).getMessage()));
//		notifTemplate.formatBody(CustomerRequestType.getByCode(requestToAdd.getType()).getMessage());
		notifTemplate.setReferenceId(requestToAdd.getId());
		RestaurantEntity restaurantEntity = restaurantService.getEntityBySessionId(requestToAdd.getCheckinId());
		if(restaurantEntity != null) {
			msg = "New request for %s";
			notifTemplate.setBody(String.format(msg, CustomerRequestType.getByCode(requestToAdd.getType()).getMessage()));
			restaurantNotificationService.sendAndSave(notifTemplate,
					restaurantEntity.getNotificationToken(), restaurantEntity.getId());
		}
	}

	@Override
	public CustomerRequestReponseDto update(UpdateCustomerRequestStatusRequestDto itemToUpdate)
			throws BaseException, IOException {
		CustomerRequestEntity requestToUpdate = CustomerRequestMapper.mapUpdateRequestToEntity(itemToUpdate);
		if(requestToUpdate == null) {
			return null;
		}
		CustomerRequestEntity requestEntity = customerRequestDao.getTById(requestToUpdate.getId());
		if(requestEntity == null) {
			throw new BaseException(ResponseCode.INVALID_REQUEST_ID);
		}
		requestToUpdate.setCreated(requestEntity.getCreated());
		requestToUpdate.setType(requestEntity.getType());
		requestToUpdate.setCheckinId(requestEntity.getCheckinId());
		sendCustomerRequestNotification(requestToUpdate);
		customerRequestDao.updateT(requestToUpdate);
		return getById(requestToUpdate.getId());
	}

	private void sendCustomerRequestNotification(CustomerRequestEntity requestEntity) {
		FCMNotifTemplate customerRequestTemplate = FCMNotifTemplate.getCustomerRequestTemplate();
		customerRequestTemplate.formatBody(CustomerRequestType.getByCode(requestEntity.getType()).getMessage());
		CustomerSessionEntity sessionEntity = customersessionDao.getTById(requestEntity.getCheckinId());
		List<String> tokens = customerDao
				.getNotificationTokensByCustomerIds(Arrays.asList(sessionEntity.getCustomerId()));
		FCMNotifUtil.trySend(customerRequestTemplate, tokens, FCMNotifTo.TO_MOB);
	}

	@Override
	public void deleteById(int requestId) throws BaseException {
		if(!isExist(requestId)) {
			throw new BaseException(ResponseCode.INVALID_REQUEST_ID);
		}
		customerRequestDao.deleteT(requestId);
	}

	@Override
	public boolean isExist(int id) {
		return customerRequestDao.isExist(id);
	}

	@Override
	public List<CustomerRequestReponseDto> getAllByRestaurantIdAndStatus(int restaurantId,
			CustomerRequestStatus requestStatus) throws BaseException {
		List<CustomerSessionEntity> customerSessionEntities = customersessionDao.getByRestaurantId(restaurantId);
		List<Integer> checkinIds = CodeUtil.getFieldAsList(customerSessionEntities, entity -> entity.getId());
		if(CollectionUtils.isEmpty(checkinIds)) {
			return null;
		}
		List<CustomerRequestEntity> requests = customerRequestDao.getByCheckinIdsAndStatus(checkinIds,
				requestStatus.getCode());
		return getResponseListOrNull(requests);
	}

	private CustomerRequestReponseDto getResponseOrNull(CustomerRequestEntity request) {
		CustomerRequestReponseDto response = CustomerRequestMapper.mapEntityToResponse(request);
		if(response == null) {
			return null;
		}
		CustomerSessionEntity sessionEntity = customersessionDao.getTById(response.getCheckinId());
		if(sessionEntity != null) {
			response.setCustomer(customerService.getById(sessionEntity.getCustomerId()));
			response.setTable(tableService.getById(sessionEntity.getTableId()));
		}
		return response;
	}

	private List<CustomerRequestReponseDto> getResponseListOrNull(List<CustomerRequestEntity> request) {
		if(CollectionUtils.isEmpty(request)) {
			return null;
		}
		return request.parallelStream()
				.map(entity -> getResponseOrNull(entity))
				.filter(response -> response != null)
				.collect(Collectors.toList());
	}

	@Override
	public void cancelRequestBySession(int sessionId) {
		List<CustomerRequestEntity> requestToBeCancelled = customerRequestDao
				.getByCheckinIdsAndStatus(Arrays.asList(sessionId), CustomerRequestStatus.INITIATED.getCode());
		if(!CollectionUtils.isEmpty(requestToBeCancelled)) {
			customerRequestDao.setStatusByIds(CodeUtil.getFieldAsList(requestToBeCancelled, r -> r.getId()),
					CustomerRequestStatus.CANCELLED.getCode());
		}
	}

}
