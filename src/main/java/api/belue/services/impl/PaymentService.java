package api.belue.services.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.paytm.merchant.CheckSumServiceHelper;

import api.belue.constants.OrderStatus;
import api.belue.constants.PaymentIdType;
import api.belue.constants.PaymentParameters;
import api.belue.constants.PaymentStatus;
import api.belue.constants.PropertyConstants;
import api.belue.constants.ResponseCode;
import api.belue.constants.TransactionStatus;
import api.belue.constants.TransactionType;
import api.belue.dao.ICustomerDao;
import api.belue.dao.IOrderDao;
import api.belue.entites.CustomerEntity;
import api.belue.entites.OrderEntity;
import api.belue.entites.WalletTransactionEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddPaymentRequestDto;
import api.belue.request.dto.GetPaytmParamsRequestDto;
import api.belue.response.PaytmResponse;
import api.belue.response.dto.PaymentParamsResponse;
import api.belue.services.IPaymentService;
import api.belue.services.IPropertyService;
import api.belue.services.IWalletService;
import api.belue.utils.JsonUtils;
import api.belue.utils.NumberUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;

@Service
public class PaymentService implements IPaymentService {

	private static final String GATEWAY_NAME = "PAYTM";
	private static final String TXN = "-TXN-";
	@Autowired
	private IOrderDao orderDao;
	@Autowired
	private IPropertyService propertyService;
	@Autowired
	private ICustomerDao customerDao;
	@Autowired
	private IWalletService walletService;

	private Logger logger = LoggerFactory.getLogger(PaymentService.class);

	@Override
	public PaymentParamsResponse getPaytmParams(GetPaytmParamsRequestDto request) throws BaseException {
		if(request == null) {
			return null;
		}
		TreeMap<String, String> paytmParamaters = fetchPaytmParams(request);

		double amountDouble = getOrdersTotalAmount(request.getIdType(), request.getCheckinId(), request.getOrderId());
		paytmParamaters.put(PaymentParameters.KEY_TXN_AMOUNT, String.valueOf(amountDouble));

		String orderIdForPaytm = addInitiatedTransaction(request, amountDouble);
		logger.info("Fetch paytm params request, txnId:{} request : {}, finalAmount:{}",
				new Object[] { String.valueOf(orderIdForPaytm), request, amountDouble });
		paytmParamaters.put(PaymentParameters.KEY_ORDER_ID, orderIdForPaytm);

		String merchantKey = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_MERCHANT_KEY, null);
		String checksum = calChecksum(merchantKey, paytmParamaters);
		paytmParamaters.put(PaymentParameters.KEY_CHECKSUMHASH, checksum);

		logger.info("Final get paytm params : map : {}", paytmParamaters);
		PaymentParamsResponse response = new PaymentParamsResponse();
		response.setParamsMap(paytmParamaters);
		return response;
	}

	private TreeMap<String, String> fetchPaytmParams(GetPaytmParamsRequestDto request) {
		String merchantId = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_MERCHANT_ID, null);
		String industryTypeId = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_INDUSTRY_TYPE_ID,
				null);
		String channelId = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_CHANNEL_ID, null);
		String website = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_WEBSITE, null);
		String callbackUrl = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_CALLBACK_URL, null);
		CustomerEntity customer = customerDao.getTById(request.getCustomerId());

		TreeMap<String, String> paramaters = new TreeMap<>();
		paramaters.put(PaymentParameters.KEY_MERCHANT_ID, merchantId);
		paramaters.put(PaymentParameters.KEY_CUSTOMER_ID, String.valueOf(customer.getId()));
		paramaters.put(PaymentParameters.KEY_INDUSTRY_TYPE_ID, industryTypeId);
		paramaters.put(PaymentParameters.KEY_CHANNEL_ID, channelId);
		paramaters.put(PaymentParameters.KEY_WEBSITE, website);
		paramaters.put(PaymentParameters.KEY_CALLBACK_URL, callbackUrl);
		paramaters.put(PaymentParameters.MOBILE_NO, customer.getPhoneNumber());
		return paramaters;
	}

	private double getOrdersTotalAmount(int idType, int checkinId, int orderId) throws BaseException {
		PaymentIdType paymentIdType = PaymentIdType.getByCode(idType);
		if(paymentIdType == null) {
			paymentIdType = PaymentIdType.SESSION_ID;
			// throw new BaseException(ResponseCode.INVALID_ID_TYPE);
		}
		List<OrderEntity> orders = paymentIdType == PaymentIdType.SESSION_ID
				? orderDao.getByCheckinId(checkinId)
				: Arrays.asList(orderDao.getTById(orderId));
		if(CollectionUtils.isEmpty(orders)) {
			throw new BaseException(ResponseCode.NO_ORDER_FOUND);
		}
		double amountDouble = orders.parallelStream()
				.filter(order -> order.getStatusCode() != OrderStatus.CANCELLED.getCode())
				.filter(order -> order.getPaymentStatusCode() != PaymentStatus.PAID.getCode())
				.mapToDouble(order -> order.getRemainingAmount())
				.sum();
		return NumberUtils.roundOffValue(amountDouble);
	}

	private String calChecksum(String merchantKey, TreeMap<String, String> paramaters) throws BaseException {
		try {
			return CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(merchantKey, paramaters);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException(ResponseCode.ERROR);
		}
	}

	private String addInitiatedTransaction(GetPaytmParamsRequestDto request, double amount) {
		WalletTransactionEntity txn = new WalletTransactionEntity();
		txn.setCustomerId(request.getCustomerId());
		txn.setCustomerSessionId(request.getCheckinId());
		txn.setOrderId(request.getOrderId());
		if(PaymentIdType.getByCode(request.getIdType()) == null) {
			request.setIdType(PaymentIdType.SESSION_ID.getCode());
		}
		txn.setOrderOrSessionType(request.getIdType());
		txn.setPaymentGatewayName(GATEWAY_NAME);
		txn.setTransactionType(TransactionType.DEPOSIT);
		txn.setStatus(TransactionStatus.INITIATED);
		txn.setAmount(amount);
		walletService.saveTransaction(txn);
		Random r = new Random(System.currentTimeMillis());
		String uniqueId = "ORDER" + (1 + r.nextInt(2)) * 10000 + r.nextInt(10000) + TXN + txn.getId();
		txn.setUniqueId(uniqueId);
		walletService.update(txn);
		return uniqueId;
	}

	@Override
	public void addPayment(AddPaymentRequestDto transaction) throws BaseException {
		if(transaction == null) {
			return;
		}
		WalletTransactionEntity txn = fetchTransaction(transaction);
		txn.setStatus(TransactionStatus.COMPLETED);
		walletService.deposit(txn);
	}

	private WalletTransactionEntity fetchTransaction(AddPaymentRequestDto transaction) throws BaseException {
		String txnMetaDataJson = JsonUtils.getJson(transaction.getTransactionMetaData());
		PaytmResponse fromJson = JsonUtils.fromJson(txnMetaDataJson, PaytmResponse.class);
		if(fromJson == null) {
			throw new BaseException(ResponseCode.INVALID_PAYMENT_JSON_FORMAT);
		}
		WalletTransactionEntity txn = walletService.getByUniqueId(fromJson.getOrderId());
		if(txn == null) {
			throw new BaseException(ResponseCode.INVALID_PAYMENT);
		}
		txn.setPaymentGatewayMetaData(txnMetaDataJson);
		txn.setPaymentGatewayTransactionId(fromJson.getTxnId());
		txn.setMetaData(JsonUtils.getJson(transaction.getMetaData()));
		return txn;
	}
	
	@Override
	public Object verifyTransaction(WalletTransactionEntity entity) throws BaseException, IOException {
		PaytmResponse paytmResponse = JsonUtils.fromJson(entity.getPaymentGatewayMetaData(), PaytmResponse.class);
		TreeMap<String, String> params = new TreeMap<>();
		params.put(PaymentParameters.KEY_MERCHANT_ID, paytmResponse.getmId());
		params.put(PaymentParameters.KEY_ORDER_ID, paytmResponse.getOrderId());
		String merchantKey = propertyService.getStringPropertyValue(PropertyConstants.PROPERTY_MERCHANT_KEY, null);
		String checksum = calChecksum(merchantKey, params);
		params.put(PaymentParameters.KEY_CHECKSUMHASH, checksum);
		String json = JsonUtils.getJson(params);
		String substring = json.substring(1, json.length()-1);
		String encoded = URLEncoder.encode(substring, "UTF-8");
		String URL = "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?JsonData={" + encoded + "}";
		System.out.println(URL);
		Request request = new Request.Builder().url(URL).get().build();
		return (new OkHttpClient()).newCall(request).execute().body().string();
	}

}
