package api.belue.services.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.FCMNotifTemplate;
import api.belue.constants.FCMNotifTo;
import api.belue.constants.OrderStatus;
import api.belue.constants.PaymentStatus;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.dao.ICustomerSessionDao;
import api.belue.dao.IOrderDao;
import api.belue.dao.IRestaurantDao;
import api.belue.dao.IRestaurantTableDao;
import api.belue.entites.CustomerEntity;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.OrderEntity;
import api.belue.entites.RestaurantEntity;
import api.belue.entites.RestaurantTableEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.CustomerSessionMapper;
import api.belue.request.dto.AddCustomerSessionRequestDto;
import api.belue.request.dto.CustomerSessionCheckoutRequestDto;
import api.belue.request.dto.UpdateCustomerSessionRequestDto;
import api.belue.response.dto.CustomerResponseDto;
import api.belue.response.dto.CustomerSessionResponseDto;
import api.belue.response.dto.RestaurantTableResponseDto;
import api.belue.response.dto.RestuarantResponseDto;
import api.belue.services.ICustomerRequestService;
import api.belue.services.ICustomerService;
import api.belue.services.ICustomerSessionService;
import api.belue.services.IGetCachedResponseService;
import api.belue.services.IOrderService;
import api.belue.services.IRestaurantNotificationService;
import api.belue.services.IRestaurantService;
import api.belue.services.IRestaurantTableService;
import api.belue.utils.CacheUtil;
import api.belue.utils.FCMNotifUtil;
import api.belue.utils.JsonUtils;

@Service
public class CustomerSessionService implements ICustomerSessionService, IGetCachedResponseService<CustomerSessionEntity, CustomerSessionResponseDto> {

	@Autowired
	private ICustomerSessionDao customerSessionDao;
	@Autowired
	private IRestaurantTableDao restaurantTableDao;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IOrderDao orderDao;
	@Autowired
	private ICustomerRequestService customerRequestService;
	@Autowired
	IRestaurantService restaurantService;
	@Autowired
	private IRestaurantDao restaurantDao;
	@Autowired
	private IRestaurantNotificationService restaurantNotificationService;
	@Autowired
	private IOrderService orderService;
	@Autowired
	private IRestaurantTableService tableService;
	
	CacheUtil<Integer, CustomerResponseDto> customerCache = new CacheUtil<>();
	CacheUtil<Integer, RestaurantTableResponseDto> tableCache = new CacheUtil<>();
	CacheUtil<Integer, RestuarantResponseDto> restaurantCache = new CacheUtil<>();
	
	@Override
	public List<CustomerSessionResponseDto> getAll(SessionStatus sessionStatus, RoleType roleType, int userId)
			throws BaseException {
		List<CustomerSessionResponseDto> response = null;
		switch(roleType) {
		case CUSTOMER:
			response = getResponseListOrNull(
					customerSessionDao.getByCustomerIdAndSessionStatus(userId, sessionStatus.getCode()));
			break;
		case RESTAURANT:
			response = getResponseListOrNull(
					customerSessionDao.getByRestaurantIdAndSessionStatus(userId, sessionStatus.getCode()));
			break;
		}
		return response;
	}

	@Override
	public CustomerSessionResponseDto addUserSession(AddCustomerSessionRequestDto request) throws Exception {
		CustomerSessionEntity sessionToSave = CustomerSessionMapper.mapAddRequestToEntity(request);
		if(sessionToSave == null) {
			return null;
		}
		CustomerEntity customerEntity = customerService.validateIsCustomerVerified(sessionToSave.getCustomerId());
		List<CustomerSessionEntity> oldSessions = customerSessionDao
				.getByCustomerIdAndSessionStatus(sessionToSave.getCustomerId(), SessionStatus.CHECKED_IN.getCode());
		if(!CollectionUtils.isEmpty(oldSessions)) {
			Optional<CustomerSessionEntity> session = oldSessions.parallelStream()
					.filter(s -> s.getTableId() == sessionToSave.getTableId())
					.findFirst();
			if(session.isPresent()) {
				return getById(session.get().getId());
			}
			throw new BaseException(ResponseCode.ALREADY_CHECKED_IN);
		}
		RestaurantTableEntity restaurantTableEntity = restaurantTableDao.getTById(sessionToSave.getTableId());
		if(restaurantTableEntity == null) {
			throw new BaseException(ResponseCode.INVALID_QR_CODE); // table id is recieved from qrcode, so it is invalid
			// qrcode, not invalid id
		}
		if(restaurantTableEntity.isOccupied()) {
			throw new BaseException(ResponseCode.TABLE_OCCUPIED);
		}
		setTableOccupancy(restaurantTableEntity, true);

		sessionToSave.setRestaurantId(restaurantTableEntity.getRestaurantId());
		sessionToSave.setSessionStatusCode(SessionStatus.CHECKED_IN.getCode());
		customerSessionDao.saveT(sessionToSave);
		sendCheckInNotification(customerEntity, sessionToSave, restaurantTableEntity.getTableNumber());
		return getCachedResponseOrNull(sessionToSave);
	}

	@Override
	public CustomerSessionResponseDto checkoutCustomer(CustomerSessionCheckoutRequestDto request) throws Exception {
		if(request == null) {
			return null;
		}
		CustomerSessionEntity customerSessionEntity = customerSessionDao.getTById(request.getCheckInId());
		if(customerSessionEntity == null) {
			throw new BaseException(ResponseCode.INVALID_CHECKIN_ID);
		}
		if(customerSessionEntity.getSessionStatusCode() != SessionStatus.CHECKED_OUT.getCode()) {
			setTableOccupancy(restaurantTableDao.getTById(customerSessionEntity.getTableId()), false);
			handleOrdersOnSessionCheckout(request.getCheckInId());
			customerRequestService.cancelRequestBySession(customerSessionEntity.getId());
			customerSessionEntity.setSessionStatusCode(SessionStatus.CHECKED_OUT.getCode());
			customerSessionEntity.setCheckOutTimeStamp(new Date());
			customerSessionEntity.setPaidInfo(request.getPaidInfo());
			customerSessionDao.updateT(customerSessionEntity);
			sendCheckOutNotification(customerSessionEntity.getCustomerId());
		}
		return getCachedResponseOrNull(customerSessionEntity);
	}

	private void handleOrdersOnSessionCheckout(int sessionId) {
		List<OrderEntity> orders = orderDao.getByCheckinId(sessionId);
		if(CollectionUtils.isEmpty(orders)) {
			return;
		}
		for(OrderEntity order : orders) {
			if(order.getStatusCode() == OrderStatus.COMPLETED.getCode()) {
				if(order.getPaymentStatusCode() != PaymentStatus.PAID.getCode()) {
					order.setPaymentStatusCode(PaymentStatus.PAID.getCode());
					order.setRemainingAmount(0);
					orderDao.updateT(order);
				}
			} else {
				order.setStatusCode(OrderStatus.CANCELLED.getCode());
				orderDao.updateT(order);
			}
		}
	}

	private void setTableOccupancy(RestaurantTableEntity restaurantTableEntity, boolean isOccupied) {
		restaurantTableEntity.setOccupied(isOccupied);
		restaurantTableDao.updateT(restaurantTableEntity);
	}

	@Override
	public void deleteById(int customerSessionId) throws BaseException {
		if(!isExist(customerSessionId)) {
			throw new BaseException(ResponseCode.INVALID_ORDER_SESSION_ID);
		}
		customerSessionDao.deleteT(customerSessionId);
	}

	@Override
	public boolean isExist(int customerSessionId) {
		return customerSessionDao.isExist(customerSessionId);
	}

	@Override
	public CustomerSessionResponseDto getById(int customerSessionId) {
		return getCachedResponseOrNull(customerSessionDao.getTById(customerSessionId));
	}

	@Override
	public CustomerSessionResponseDto updateUserSession(UpdateCustomerSessionRequestDto sessionToAdd) throws Exception {
		if(sessionToAdd == null) {
			return null;
		}
		CustomerSessionEntity sessionEntity = customerSessionDao.getTById(sessionToAdd.getId());
		if(sessionEntity == null) {
			throw new BaseException(ResponseCode.INVALID_ORDER_SESSION_ID);
		}
		sessionEntity.setTaxes(JsonUtils.getJson(sessionToAdd.getTaxes()));
		sessionEntity.setDiscounts(JsonUtils.getJson(sessionToAdd.getDiscounts()));
		customerSessionDao.updateT(sessionEntity);
		return getById(sessionEntity.getId());
	}

	private void sendCheckInNotification(CustomerEntity customerEntity, CustomerSessionEntity sessionToSave, int tableNumber) {
		// TO Customer
		FCMNotifTemplate checkInTemplate = FCMNotifTemplate.CheckInTemplate();
		checkInTemplate.formatBody(tableNumber);
		FCMNotifUtil.trySend(checkInTemplate, Arrays.asList(customerEntity.getNotificationToken()),
				FCMNotifTo.TO_MOB);
		// TO Restaurant
		checkInTemplate.setReferenceId(sessionToSave.getId());
		RestaurantEntity restaurantEntity = restaurantDao.getTById(sessionToSave.getRestaurantId());
		restaurantNotificationService.sendAndSave(checkInTemplate, restaurantEntity.getNotificationToken(), restaurantEntity.getId());
	}

	private void sendCheckOutNotification(int customerId) throws BaseException {
		FCMNotifUtil.trySend(FCMNotifTemplate.getCheckoutTemplate(),
				customerService.fetchTokens(Arrays.asList(customerId)), FCMNotifTo.TO_MOB);
	}

	@Override
	public CustomerSessionResponseDto getResponseOrNull(CustomerSessionEntity request) {
		CustomerSessionResponseDto response = CustomerSessionMapper.mapEntityToResponse(request);
		if(response == null) {
			return null;
		}
		try {
			response.setOrders(orderService.getByCheckinId(request.getId()));
		} catch (BaseException e) {
		}
		response.setCustomer(customerCache.getAndAdd(request.getCustomerId(), customerId -> customerService.getById(customerId)));
		response.setTable(tableCache.getAndAdd(request.getTableId(), tableId -> tableService.getById(tableId)));
		response.setRestaurant(restaurantCache.getAndAdd(request.getRestaurantId(), restaurantId -> restaurantService.getById(restaurantId)));
		return response;
	}

	@Override
	public void clearCaches() {
		customerCache.clear();
		tableCache.clear();
		restaurantCache.clear();
	}

}
