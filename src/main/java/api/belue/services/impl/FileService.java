package api.belue.services.impl;

import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.cloudinary.Cloudinary;

import api.belue.constants.FileType;
import api.belue.constants.Params;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.services.IFileService;
import api.belue.utils.FileUtil;
import api.belue.utils.PropertiesUtil;

@Service
public class FileService implements IFileService {
	private Cloudinary cloudinaryClient;

	@PostConstruct
	private void init() throws IOException {
		@SuppressWarnings("rawtypes")
		Map params = PropertiesUtil.getPropertiesAsMap(Params.Cloudinary.CLOUDINARY_CLIENT_PROPS_FILE);
		if (params != null) {
			cloudinaryClient = new Cloudinary(params);
		}
	}

	public String saveFile(byte[] file, String fileName, FileType fileType) throws IOException, RuntimeException {
		return FileUtil.saveBytesAsFile(file, fileName, fileType);
	}

	@Override
	public byte[] getFileFromUrl(String url, FileType fileType) throws IOException {
		if (url == null) {
			return null;
		}
		byte[] response = null;
		switch (fileType) {
		case IMAGE:
			response = FileUtil.downloadFile(url, fileType);
			break;
		default:
			
			break;
		}
		return response;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String uploadFile(Object file, FileType fileType) throws IOException, BaseException {
		String response = null;
		switch (fileType) {
		case IMAGE:
			if (cloudinaryClient != null) {
				Map params = PropertiesUtil.getPropertiesAsMap(Params.Cloudinary.CLOUDINARY_UPLOAD_PROPS_FILE);
				Map result = FileUtil.uploadToCloudinary(cloudinaryClient, file, params);
				if (result == null) {
					return null;
				}
				response = result.get(Params.Cloudinary.Result.RESULT_URL).toString();
			}
			break;
		default:
			throw new BaseException(ResponseCode.FILE_TYPE_NOT_SUPPORTED);
		}
		return response;
	}
}
