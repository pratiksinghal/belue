package api.belue.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.ResponseCode;
import api.belue.dao.IMenuCategoryDao;
import api.belue.dao.IMenuDao;
import api.belue.dao.IMenuItemDao;
import api.belue.entites.MenuCategoryEntity;
import api.belue.entites.MenuItemEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.MenuItemMapper;
import api.belue.pojo.MenuItemInOrderPojo;
import api.belue.request.dto.AddMenuItemRequestDto;
import api.belue.request.dto.UpdateMenuItemRequestDto;
import api.belue.response.dto.MenuItemResponseDto;
import api.belue.services.IMenuItemService;
import api.belue.utils.NumberUtils;

@Service
public class MenuItemService implements IMenuItemService {

	@Autowired
	private IMenuItemDao menuItemDao;
	@Autowired
	private IMenuCategoryDao menuCategoryDao;
	@Autowired
	private IMenuDao menuDao;

	@Override
	public List<MenuItemResponseDto> getAll() {
		return MenuItemMapper.mapEntityListToResponseList(menuItemDao.getAllT());
	}

	@Override
	public MenuItemResponseDto getById(int id) {
		return MenuItemMapper.mapEntityToResponse(menuItemDao.getTById(id));
	}

	@Override
	public MenuItemResponseDto save(AddMenuItemRequestDto request) throws BaseException, IOException {
		MenuItemEntity menuItemToSave = MenuItemMapper.mapAddRequestToEntity(request);
		if(menuItemToSave == null) {
			return null;
		}
		MenuCategoryEntity menuCategoryEntity = menuCategoryDao.getTById(menuItemToSave.getCategoryId());
		if(menuCategoryEntity == null) {
			throw new BaseException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		MenuItemMapper.setCategoryDetails(menuItemToSave, menuCategoryEntity);
		menuItemDao.saveT(menuItemToSave);
		return MenuItemMapper.mapEntityToResponse(menuItemToSave);
	}

	@Override
	public MenuItemResponseDto update(UpdateMenuItemRequestDto request) throws BaseException, IOException {
		MenuItemEntity menuItemToUpdate = MenuItemMapper.mapUpdateRequestToEntity(request);
		if(menuItemToUpdate == null) {
			return null;
		}
		MenuItemEntity menuItemEntity = menuItemDao.getTById(menuItemToUpdate.getId());
		if(menuItemEntity == null) {
			throw new BaseException(ResponseCode.INVALID_MENU_ITEM_ID);
		}
		if(!menuCategoryDao.isExist(menuItemToUpdate.getCategoryId())) {
			throw new BaseException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		MenuItemMapper.setCategoryDetails(menuItemToUpdate, menuItemEntity);
		menuItemDao.updateT(menuItemToUpdate);
		return MenuItemMapper.mapEntityToResponse(menuItemToUpdate);
	}

	@Override
	public void deleteById(int menuIemId) throws BaseException {
		if(!isExist(menuIemId)) {
			throw new BaseException(ResponseCode.INVALID_MENU_ITEM_ID);
		}
		menuItemDao.deleteT(menuIemId);
	}

	@Override
	public boolean isExist(int id) {
		return menuItemDao.isExist(id);
	}

	@Override
	public List<MenuItemResponseDto> getAllByMenuCategogoryId(int menuCategoryId) throws BaseException {
		if(!menuCategoryDao.isExist(menuCategoryId)) {
			throw new BaseException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		return MenuItemMapper.mapEntityListToResponseList(menuItemDao.getAllByMenuCategogoryId(menuCategoryId));
	}

	@Override
	public List<MenuItemResponseDto> getAllByMenuId(int menuId) throws BaseException {
		if(!menuDao.isExist(menuId)) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		return MenuItemMapper.mapEntityListToResponseList(menuItemDao.getAllByMenuId(menuId));
	}

	@Override
	public void calculateOrderItemAmounts(List<MenuItemInOrderPojo> orderItems) throws BaseException {
		if(CollectionUtils.isEmpty(orderItems)) {
			return;
		}
		for(MenuItemInOrderPojo orderItem : orderItems) {
			MenuItemEntity menuItemEntity = menuItemDao.getTById(orderItem.getId());
			if(menuItemEntity == null) {
				throw new BaseException(ResponseCode.INVALID_MENU_ITEM_ID);
			}
			MenuItemMapper.setEntityDetailsToOrderItem(orderItem, menuItemEntity);
			orderItem.setAmount(orderItem.getUnitPrice() * orderItem.getQuantity());
			orderItem.setTaxAmount(NumberUtils.getPercentageValue(orderItem.getAmount(), orderItem.getTotalTax()));
			orderItem.setTotalAmount(orderItem.getAmount() + orderItem.getTaxAmount());	
		}
	}
	
}
