package api.belue.services.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.belue.constants.FCMNotifTemplate;
import api.belue.constants.FCMNotifTo;
import api.belue.constants.ResponseCode;
import api.belue.dao.IRestaurantNotificationDao;
import api.belue.entites.RestaurantNotificationsEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.RestaurantNotificationsMapper;
import api.belue.response.dto.RestaurantNotificationResposeDto;
import api.belue.services.IRestaurantNotificationService;
import api.belue.utils.FCMNotifUtil;

@Service
public class RestaurantNotificationService implements IRestaurantNotificationService {
	
	@Autowired
	IRestaurantNotificationDao notifDao;

	@Override
	public void setRead(int notifId, boolean isRead) throws BaseException {
		RestaurantNotificationsEntity notificationsEntity = notifDao.getTById(notifId);
		if(notificationsEntity == null) {
			throw new BaseException(ResponseCode.INVALID_NOTIFICATION_ID);
		}
		notificationsEntity.setIsRead(isRead);
		notifDao.updateT(notificationsEntity);
	}

	@Override
	public List<RestaurantNotificationResposeDto> getByRestaurantId(int restaurantId) {
		return RestaurantNotificationsMapper.mapEntityListToResponseList(notifDao.getByRestaurantId(restaurantId));
	}
	@Override
	public List<RestaurantNotificationResposeDto> getByRestaurantIdAndReadStatus(int restaurantId, boolean readStatus) {
		return RestaurantNotificationsMapper.mapEntityListToResponseList(notifDao.getByRestaurantIdAndReadStatus(restaurantId, readStatus));
	}
	
	@Override
	public int save(RestaurantNotificationsEntity notif) {
		notifDao.saveT(notif);
		return notif.getId();
	}
	
	@Override
	public void sendAndSave(FCMNotifTemplate template, String token, int restaurantId) {
		RestaurantNotificationsEntity notificationsEntity = RestaurantNotificationsMapper.mapNotifTemplateToEntity(template, restaurantId);
		save(notificationsEntity);
		template.setNotifId(notificationsEntity.getId());
		FCMNotifUtil.trySend(template, Arrays.asList(token), FCMNotifTo.TO_WEB);
	}
	
}
