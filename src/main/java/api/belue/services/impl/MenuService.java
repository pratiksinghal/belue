package api.belue.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.belue.constants.ResponseCode;
import api.belue.dao.IMenuDao;
import api.belue.dao.IRestaurantDao;
import api.belue.entites.MenuEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.MenuMapper;
import api.belue.request.dto.AddMenuRequestDto;
import api.belue.request.dto.UpdateMenuRequestDto;
import api.belue.response.dto.MenuResponseDto;
import api.belue.services.IMenuService;

@Service
public class MenuService implements IMenuService {

	@Autowired
	private IMenuDao menuDao;
	@Autowired
	private IRestaurantDao restaurantDao;

	@Override
	public List<MenuResponseDto> getAll() {
		return MenuMapper.mapEntityListToResponseList(menuDao.getAllT());
	}

	@Override
	public MenuResponseDto getById(int id) {
		return MenuMapper.mapEntityToResponse(menuDao.getTById(id));
	}

	@Override
	public MenuResponseDto save(AddMenuRequestDto request) throws BaseException, IOException {
		MenuEntity menuToSave = MenuMapper.mapAddRequestToEntity(request);
		if (menuToSave == null) {
			return null;
		}
		if (!restaurantDao.isExist(menuToSave.getRestaurantId())) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		menuDao.saveT(menuToSave);
		return MenuMapper.mapEntityToResponse(menuToSave);
	}

	@Override
	public MenuResponseDto update(UpdateMenuRequestDto request) throws BaseException, IOException {
		MenuEntity menuToUpdate = MenuMapper.mapUpdateRequestToEntity(request);
		if (menuToUpdate == null) {
			return null;
		}
		MenuEntity menuEntity = menuDao.getTById(menuToUpdate.getId());
		if (menuEntity == null) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		if (menuEntity.getRestaurantId() != menuToUpdate.getRestaurantId()) { // RestaurantId cannot be changed
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		menuDao.updateT(menuToUpdate);
		return MenuMapper.mapEntityToResponse(menuToUpdate);
	}

	@Override
	public void deleteById(int menuId) throws BaseException {
		if (!isExist(menuId)) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		menuDao.deleteT(menuId);
	}

	@Override
	public List<MenuResponseDto> getAllByRestaurantId(int restauratId) throws BaseException {
		if (!restaurantDao.isExist(restauratId)) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		return MenuMapper.mapEntityListToResponseList(menuDao.getAllByRestaurantId(restauratId));
	}

	@Override
	public boolean isExist(int id) {
		return menuDao.isExist(id);
	}

}
