package api.belue.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.belue.constants.ResponseCode;
import api.belue.dao.IMenuCategoryDao;
import api.belue.dao.IMenuDao;
import api.belue.entites.MenuCategoryEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.MenuCategoryMapper;
import api.belue.request.dto.AddMenuCategoryRequestDto;
import api.belue.request.dto.UpdateMenuCategoryRequestDto;
import api.belue.response.dto.MenuCategoryResponseDto;

@Service
public class MenuCategoryService implements api.belue.services.IMenuCategoryService {

	@Autowired
	private IMenuCategoryDao menuCategoryDao;
	@Autowired
	private IMenuDao menuDao;
	
	@Override
	public List<MenuCategoryResponseDto> getAll() {
		return MenuCategoryMapper.mapEntityListToResponseList(menuCategoryDao.getAllT());
	}

	@Override
	public MenuCategoryResponseDto getById(int id) {
		return MenuCategoryMapper.mapEntityToResponse(menuCategoryDao.getTById(id));
	}

	@Override
	public MenuCategoryResponseDto save(AddMenuCategoryRequestDto request) throws BaseException, IOException {
		MenuCategoryEntity menuCategoryToSave = MenuCategoryMapper.mapAddRequestToEntity(request);
		if(menuCategoryToSave == null) {
			return null;
		}
		if(!menuDao.isExist(menuCategoryToSave.getMenuId())) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		menuCategoryDao.saveT(menuCategoryToSave);
		return MenuCategoryMapper.mapEntityToResponse(menuCategoryToSave);
	}

	@Override
	public MenuCategoryResponseDto update(UpdateMenuCategoryRequestDto request) throws BaseException, IOException {
		MenuCategoryEntity menuCategoryToUpdate = MenuCategoryMapper.mapUpdateRequestToEntity(request);
		if(menuCategoryToUpdate == null) {
			return null;
		}
		if(!isExist(menuCategoryToUpdate.getId())) {
			throw new BaseException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		if(!menuDao.isExist(menuCategoryToUpdate.getMenuId())) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		menuCategoryDao.updateT(menuCategoryToUpdate);
		return MenuCategoryMapper.mapEntityToResponse(menuCategoryToUpdate);
	}

	@Override
	public void deleteById(int id) throws BaseException {
		if(!isExist(id)) {
			throw new BaseException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		menuCategoryDao.deleteT(id);
	}

	@Override
	public boolean isExist(int id) {
		return menuCategoryDao.isExist(id);
	}

	@Override
	public List<MenuCategoryResponseDto> getAllByMenuId(int menuId) throws BaseException {
		if(!menuDao.isExist(menuId)) {
			throw new BaseException(ResponseCode.INVALID_MENU_ID);
		}
		return MenuCategoryMapper.mapEntityListToResponseList(menuCategoryDao.getAllByMenuId(menuId));
	}

}
