package api.belue.services.impl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.dao.ICustomerDao;
import api.belue.entites.CustomerEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.CustomerMapper;
import api.belue.request.dto.AddCustomerRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.CustomerLoginRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.UpdateCustomerRequestDto;
import api.belue.response.dto.CustomerLoginResponseDto;
import api.belue.response.dto.CustomerResponseDto;
import api.belue.services.ICustomerService;
import api.belue.services.ITokenService;
import api.belue.utils.FCMNotifUtil;
import api.belue.utils.JsonUtils;

@Service
public class CustomerService implements ICustomerService {

	@Autowired
	private ICustomerDao customerDao;
	@Autowired
	private ITokenService tokenService;

	@Override
	public List<CustomerResponseDto> getAll() {
		return CustomerMapper.mapEntityListToResponseList(customerDao.getAllT());
	}

	@Override
	public CustomerResponseDto getById(int id) {
		return CustomerMapper.mapEntityToResponse(customerDao.getTById(id));
	}

	@Override
	public CustomerLoginResponseDto loginCustomer(CustomerLoginRequestDto loginRequest) throws BaseException {
		CustomerEntity customerEntity = customerDao.getByPhoneNumber(loginRequest.getPhoneNumber());
		if(customerEntity == null) {
			throw new BaseException(ResponseCode.PHONE_NOT_EXISTS);
		}
		CustomerLoginResponseDto response = new CustomerLoginResponseDto();
		response.setToken(tokenService.getToken(customerEntity.getId(), RoleType.CUSTOMER));
		response.setCustomerData(CustomerMapper.mapEntityToResponse(customerEntity));
		return response;
	}

	@Override
	public CustomerResponseDto save(AddCustomerRequestDto itemToSave) throws BaseException, IOException {
		CustomerEntity customerToSave = CustomerMapper.mapAddRequestToEntity(itemToSave);
		if(customerToSave == null) {
			return null;
		}
		if(customerDao.getByPhoneNumber(itemToSave.getPhoneNumber()) != null) {
			throw new BaseException(ResponseCode.PHONE_ALREADY_EXISTS);
		}
		customerDao.saveT(customerToSave);
		return CustomerMapper.mapEntityToResponse(customerToSave);
	}
	
	@Override
	public CustomerLoginResponseDto saveWithLoginResponse(AddCustomerRequestDto itemToSave)
			throws BaseException, IOException {
		CustomerResponseDto customerResponseDto = save(itemToSave);
		CustomerLoginRequestDto loginRequest = new CustomerLoginRequestDto();
		loginRequest.setPhoneNumber(customerResponseDto.getPhoneNumber());
		return loginCustomer(loginRequest);
	}
	
	@Override
	public CustomerResponseDto update(UpdateCustomerRequestDto request) throws BaseException {
		CustomerEntity customerToUpdate = CustomerMapper.mapUpdateRequestToEntity(request);
		if(customerToUpdate == null) {
			return null;
		}
		CustomerEntity customerEntity = customerDao.getTById(customerToUpdate.getId());
		if(customerEntity == null) {
			throw new BaseException(ResponseCode.INVALID_ID);
		}
		customerToUpdate.setNotificationToken(customerEntity.getNotificationToken());
		customerToUpdate.setPhoneNumber(customerEntity.getPhoneNumber());
		customerToUpdate.setCreated(customerEntity.getCreated());
		customerToUpdate.setWalletBalance(customerEntity.getWalletBalance());
		customerToUpdate.setPlatform(customerEntity.getPlatform());
		customerDao.updateT(customerToUpdate);
		return CustomerMapper.mapEntityToResponse(customerToUpdate);
	}

	@Override
	public void deleteById(int customerId) throws BaseException {
		if(!isExist(customerId)) {
			throw new BaseException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		customerDao.deleteT(customerId);
	}

	@Override
	public boolean isExist(int customerId) {
		return customerDao.isExist(customerId);
	}

	@Override
	public boolean isPhoneNumberExists(String phoneNumber) throws BaseException {
		if(phoneNumber == null) {
			return false;
		}
		return customerDao.getByPhoneNumber(phoneNumber) == null ? false : true;
	}

	@Override
	public String sendPushNotification(PushNotificationRequestDto request) throws IOException, BaseException {
		List<String> tokens =  (request.isIsAll()) 
			? customerDao.getAllNotificationTokens()
		    : fetchTokens(request.getCustomerIds());
		if(CollectionUtils.isEmpty(tokens)) {
			throw new BaseException(ResponseCode.NO_NOTIFICATION_TOKEN_FOUND);
		}
		tokens = tokens.parallelStream()
				.filter(s -> s != null)
				.collect(Collectors.toList());
		String dataJson = JsonUtils.builder()
				.add("body", request.getBody())
				.add("title", request.getTitle())
				.add("message", request.getMessage()).build();
		return FCMNotifUtil.send(dataJson, tokens);
	}

	@Override
	public List<String> fetchTokens(List<Integer> customerIds) throws BaseException {
		if(CollectionUtils.isEmpty(customerIds)) {
			throw new BaseException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		return customerDao.getNotificationTokensByCustomerIds(customerIds);
	}

	@Override
	public void addUpdateNotificationToken(AddUpdateNotificationTokenRequestDto request) throws BaseException {
		CustomerEntity customerEntity = customerDao.getTById(request.getCustomerId());
		if(customerEntity == null) {
			throw new BaseException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		customerEntity.setPlatform(request.getPlatform());
		customerEntity.setNotificationToken(request.getNotificationToken());
		customerDao.updateT(customerEntity);
	}

	@Override
	public CustomerEntity validateIsCustomerVerified(int customerId) throws BaseException {
		CustomerEntity customerEntity = customerDao.getTById(customerId);
		if(customerEntity == null) {
			throw new BaseException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		if(!customerEntity.isVerified()) {
			throw new BaseException(ResponseCode.CUSTOMER_NOT_VERIFIED);
		}
		return customerEntity;
	}
	
	

}