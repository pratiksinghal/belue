package api.belue.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.services.IGlobalDataService;
import api.belue.services.IPropertyService;

@Service
public class GlobalDataService implements IGlobalDataService {

	@Autowired
	private IPropertyService propertyService;
	private final String GLOBAL_DATA = "GLOBAL.DATA";

	@Override
	public String get() throws BaseException {
		String globalData = propertyService.getStringPropertyValue(GLOBAL_DATA, null);
		if(StringUtils.isEmpty(globalData)) {
			throw new BaseException(ResponseCode.NO_GLOBAL_DATA_FOUND);
		}
		return globalData;
	}

	@Override
	public String save(String data) {
		propertyService.addOrModifyProperty(GLOBAL_DATA, data);
		return data;
	}

}
