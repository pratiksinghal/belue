package api.belue.services.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.FCMNotifTo;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.dao.ICustomerSessionDao;
import api.belue.dao.IOrderDao;
import api.belue.dao.IRestaurantDao;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.RestaurantEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.RestaurantMapper;
import api.belue.pojo.RestaurantStat;
import api.belue.request.dto.AddRestaurantRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.ChangePasswordRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.RestaurantLoginRequestDto;
import api.belue.request.dto.UpdateRestaurantRequestDto;
import api.belue.response.dto.RestaurantLoginResponseDto;
import api.belue.response.dto.RestuarantResponseDto;
import api.belue.services.IRestaurantService;
import api.belue.services.ITokenService;
import api.belue.utils.FCMNotifUtil;
import api.belue.utils.JsonUtils;

@Service
public class RestaurantService implements IRestaurantService {
	@Autowired
	private IRestaurantDao restaurantDao;
	@Autowired
	private ITokenService tokenService;
	@Autowired
	private IOrderDao orderDao;
	@Autowired
	private ICustomerSessionDao customerSessionDao;

	@Override
	public List<RestuarantResponseDto> getAll() {
		return RestaurantMapper.mapEntityListToResponseList(restaurantDao.getAllT());
	}

	@Override
	public RestuarantResponseDto getById(int id) {
		return RestaurantMapper.mapEntityToResponse(restaurantDao.getTById(id));
	}

	@Override
	public RestuarantResponseDto save(AddRestaurantRequestDto request) throws BaseException {
		RestaurantEntity restaurantToSave = RestaurantMapper.mapAddRequestToEntity(request);
		if(restaurantToSave == null) {
			return null;
		}
		RestaurantEntity restaurantEntity = restaurantDao.getByUsername(restaurantToSave.getUsername());
		if(restaurantEntity != null) { // restaurant with this username already exists
			throw new BaseException(ResponseCode.USERNAME_EXISTS);
		}
		restaurantDao.saveT(restaurantToSave);
		return RestaurantMapper.mapEntityToResponse(restaurantToSave);

	}

	@Override
	public RestuarantResponseDto update(UpdateRestaurantRequestDto request) throws BaseException {
		RestaurantEntity restaurantToUpdate = RestaurantMapper.mapUpdateRequestToEntity(request);
		if(restaurantToUpdate == null) {
			return null;
		}
		RestaurantEntity restaurantEntity = restaurantDao.getByUsername(restaurantToUpdate.getUsername());
		if(restaurantEntity != null && restaurantToUpdate.getId() != restaurantEntity.getId()) { // restaurant with
			throw new BaseException(ResponseCode.USERNAME_EXISTS);
		}
		restaurantEntity = restaurantDao.getTById(restaurantToUpdate.getId());
		if(restaurantEntity == null) { // no restaurant with provided id is present
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		restaurantToUpdate.setCreated(restaurantEntity.getCreated());
		restaurantToUpdate.setPassword(restaurantEntity.getPassword());
		restaurantDao.updateT(restaurantToUpdate);
		return RestaurantMapper.mapEntityToResponse(restaurantToUpdate);
	}

	@Override
	public void deleteById(int restaurantId) throws BaseException {
		if(!isExist(restaurantId)) { // no restaurant with provided id is present
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		restaurantDao.deleteT(restaurantId);
	}

	@Override
	public RestaurantLoginResponseDto login(RestaurantLoginRequestDto request) throws BaseException {
		if(request == null) {
			return null;
		}
		RestaurantEntity restaurantToLogin = restaurantDao.getByUsername(request.getUsername());
		if(restaurantToLogin == null || !restaurantToLogin.getPassword().equals(request.getPassword())) {
			throw new BaseException(ResponseCode.INVALID_DETAILS);
		}
		RestaurantLoginResponseDto response = new RestaurantLoginResponseDto();
		response.setToken(tokenService.getToken(restaurantToLogin.getId(), RoleType.RESTAURANT));
		response.setRestaurantDetail(getById(restaurantToLogin.getId()));
		return response;
	}

	@Override
	public boolean isExist(int id) {
		return restaurantDao.isExist(id);
	}

	@Override
	public void changePassword(ChangePasswordRequestDto request) throws BaseException {
		RestaurantEntity restaurantEntity = restaurantDao.getTById(request.getId());
		if(restaurantEntity == null) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		if(!restaurantEntity.getPassword().equals(request.getOldPassword())) {
			throw new BaseException(ResponseCode.INCORRECT_PASSWORD);
		}
		restaurantEntity.setPassword(request.getNewPassword());
		restaurantDao.updateT(restaurantEntity);
	}

	@Override
	public RestaurantStat getStats(int id, Date startDate, Date endDate) {
		return orderDao.getStat(id, startDate, endDate);
	}
	
	@Override
	public void addUpdateNotificationToken(AddUpdateNotificationTokenRequestDto request) throws BaseException {
		RestaurantEntity restaurantEntity = restaurantDao.getTById(request.getRestaurantId());
		if(restaurantEntity == null) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		restaurantEntity.setNotificationToken(request.getNotificationToken());
		restaurantDao.updateT(restaurantEntity);
	}
	
	@Override
	public String sendPushNotification(PushNotificationRequestDto request) throws IOException, BaseException {
		List<String> tokens =  (request.isIsAll()) 
			? restaurantDao.getAllNotificationTokens()
		    : fetchTokens(request.getRestaurantIds());
		Map<Object, Object> dataJson = JsonUtils.builder()
				.add("body", request.getBody())
				.add("title", request.getTitle())
				.add("message", request.getMessage()).getData();
		return FCMNotifUtil.send(dataJson, tokens, FCMNotifTo.TO_WEB);
	}
	
	@Override
	public List<String> fetchTokens(List<Integer> restaurantIds) throws BaseException {
		if(CollectionUtils.isEmpty(restaurantIds)) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		return restaurantDao.getNotificationTokensByRestaurantIds(restaurantIds);
	}

	@Override
	public RestaurantEntity getEntityBySessionId(int checkinId) {
		CustomerSessionEntity sessionEntity = customerSessionDao.getTById(checkinId);
		if(sessionEntity == null) {
			return null;
		}
		return restaurantDao.getTById(sessionEntity.getRestaurantId());
	}

}
