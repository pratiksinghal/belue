package api.belue.services.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.dao.ITokenDao;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;
import api.belue.services.ITokenService;

@Service
public class tokenService implements ITokenService {

	@Autowired
	private ITokenDao tokenDao;
	
	@Override
	public boolean isExist(String token, RoleType roleType) {
		TokenEntity tokenEntity = tokenDao.getByTokenValue(token);
		return (tokenEntity != null && RoleType.getByCode(tokenEntity.getRoleTypeCode()) == roleType) ? true : false;
	}

	@Override
	public String getToken(int userId, RoleType roleType) {
		TokenEntity generatedToken = new TokenEntity();
		generatedToken.setTokenValue(UUID.randomUUID().toString());
		generatedToken.setUserId(userId);
		generatedToken.setRoleTypeCode(roleType.getCode());
		if(tokenDao.getByUserIdAndRoleType(userId, roleType.getCode()) == null) {
			tokenDao.saveT(generatedToken);
		} else {
			tokenDao.updateT(generatedToken);
		}
		return generatedToken.getTokenValue();
	}

	@Override
	public TokenEntity getFromTokenValue(String token) throws BaseException {
		TokenEntity tokenEntity = tokenDao.getByTokenValue(token);
		if(tokenEntity == null) {
			throw new BaseException(ResponseCode.INVALID_TOKEN);
		}
		return tokenEntity;
	}

	@Override
	public void validateToken(String tokenValue) throws BaseException {
		getFromTokenValue(tokenValue);
	}
	
	

}
