package api.belue.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.FCMNotifTemplate;
import api.belue.constants.FCMNotifTo;
import api.belue.constants.OrderStatus;
import api.belue.constants.PaymentStatus;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.dao.ICustomerSessionDao;
import api.belue.dao.IOrderDao;
import api.belue.dao.IRestaurantDao;
import api.belue.entites.CustomerEntity;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.OrderEntity;
import api.belue.entites.RestaurantEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.CustomerMapper;
import api.belue.mapper.OrderMapper;
import api.belue.mapper.RestaurantMapper;
import api.belue.pojo.Discount;
import api.belue.pojo.MenuItemInOrderPojo;
import api.belue.request.dto.AddOrderRequestDto;
import api.belue.request.dto.OrderStatusRequestDto;
import api.belue.request.dto.UpdateOrderRequestDto;
import api.belue.response.dto.CustomerResponseDto;
import api.belue.response.dto.OrderResponseDto;
import api.belue.response.dto.RestaurantTableResponseDto;
import api.belue.response.dto.RestuarantResponseDto;
import api.belue.services.ICustomerService;
import api.belue.services.IGetResponseService;
import api.belue.services.IMenuItemService;
import api.belue.services.IOrderService;
import api.belue.services.IRestaurantNotificationService;
import api.belue.services.IRestaurantTableService;
import api.belue.utils.FCMNotifUtil;
import api.belue.utils.JsonUtils;
import api.belue.utils.NumberUtils;

@Service
public class OrderService implements IOrderService, IGetResponseService<OrderEntity, OrderResponseDto> {

	@Autowired
	private IOrderDao orderDao;
	@Autowired
	private ICustomerSessionDao customerSessionDao;
	@Autowired
	private IRestaurantDao restaurantDao;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IRestaurantTableService restaurantTableService;
	@Autowired
	private IRestaurantNotificationService restaurantNotificationService;
	@Autowired
	private IMenuItemService menuItemService;

	@Override
	public List<OrderResponseDto> getAll() {
		return OrderMapper.mapEntityListToResponseList(orderDao.getAllT());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderResponseDto> getAll(OrderStatus orderStatus, RoleType userRole, int userId, boolean history)
			throws BaseException {
		List<Object[]> orders = null;
		SessionStatus sessionStatus = (history) ? SessionStatus.CHECKED_OUT : SessionStatus.CHECKED_IN;
		if(history) {
			orders = (List<Object[]>) orderDao.getWithCustomer(userRole, userId, sessionStatus);
		} else {
			orders = orderStatus == OrderStatus.ALL
					? (List<Object[]>) orderDao.getWithCustomer(userRole, userId, sessionStatus)
					: (List<Object[]>) orderDao.getWithCustomer(userRole, userId, orderStatus);
		}
		if(CollectionUtils.isEmpty(orders)) {
			return null;
		}
		List<OrderResponseDto> response = new ArrayList<>(orders.size());
		orders.forEach(o -> {
			OrderResponseDto orderResponseDto = OrderMapper.mapEntityToResponse((OrderEntity) o[0]);
			CustomerSessionEntity session = (CustomerSessionEntity) o[1];
			RestaurantTableResponseDto table = restaurantTableService.getById(session.getTableId());
			orderResponseDto.setTable(table);
			orderResponseDto.setCustomer(CustomerMapper.mapEntityToResponse((CustomerEntity) o[2]));
			response.add(orderResponseDto);
		});
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderResponseDto> getAllNew(OrderStatus orderStatus, RoleType userRole, int userId, boolean history)
			throws BaseException {
		SessionStatus sessionStatus = (history) ? SessionStatus.CHECKED_OUT : SessionStatus.CHECKED_IN;
		List<Object[]> orders = (List<Object[]>) orderDao.getWithCustomer(userRole, userId, sessionStatus, orderStatus);
		if(CollectionUtils.isEmpty(orders)) {
			return null;
		}
		List<OrderResponseDto> response = new ArrayList<>(orders.size());
		orders.forEach(o -> {
			OrderResponseDto orderResponseDto = OrderMapper.mapEntityToResponse((OrderEntity) o[0]);
			CustomerSessionEntity session = (CustomerSessionEntity) o[1];
			RestaurantTableResponseDto table = restaurantTableService.getById(session.getTableId());
			orderResponseDto.setTable(table);
			orderResponseDto.setCustomer(CustomerMapper.mapEntityToResponse((CustomerEntity) o[2]));
			response.add(orderResponseDto);
		});
		return response;
	}

	@Override
	public OrderResponseDto getById(int id) {
		return getResponseOrNull(orderDao.getTById(id));
	}

	@Override
	public OrderResponseDto save(AddOrderRequestDto itemToSave) throws BaseException, IOException {
		OrderEntity orderToSave = OrderMapper.mapAddRequestToEntity(itemToSave);
		if(orderToSave == null) {
			return null;
		}
		CustomerSessionEntity customerSessionEntity = customerSessionDao.getTById(orderToSave.getCheckinId());
		if(customerSessionEntity == null) {
			throw new BaseException(ResponseCode.INVALID_CHECKIN_ID);
		}
		if(customerSessionEntity.getSessionStatusCode() == SessionStatus.CHECKED_OUT.getCode()) {
			throw new BaseException(ResponseCode.ALREADY_CHECKED_OUT);
		}
		orderToSave.setPaymentStatusCode(PaymentStatus.PENDING.getCode());
		setOrderAmounts(orderToSave, customerSessionEntity);
		orderToSave.setStatusCode(OrderStatus.INITIATED.getCode());
		orderDao.saveT(orderToSave);
		orderToSave.setOrderName("Order " + orderToSave.getId());
		orderDao.updateT(orderToSave);
		sendOrderCreationNotifications(orderToSave, customerSessionEntity);
		return getById(orderToSave.getId());
	}

	private void sendOrderCreationNotifications(OrderEntity orderToSave, CustomerSessionEntity customerSessionEntity) {
		FCMNotifTemplate orderCreatedTemplate = FCMNotifTemplate.OrderCreatedTemplate();
		orderCreatedTemplate.setReferenceId(customerSessionEntity.getId());
		RestaurantEntity restaurantEntity = restaurantDao.getTById(customerSessionEntity.getRestaurantId());
		restaurantNotificationService.sendAndSave(orderCreatedTemplate, restaurantEntity.getNotificationToken(), restaurantEntity.getId());
	}

	@Override
	public OrderResponseDto update(UpdateOrderRequestDto itemToUpdate) throws BaseException, IOException {
		OrderEntity orderToUpdate = OrderMapper.mapUpdateRequestToEntity(itemToUpdate);
		if(orderToUpdate == null) {
			return null;
		}
		OrderEntity orderEntity = orderDao.getTById(orderToUpdate.getId());
		if(orderEntity == null) {
			throw new BaseException(ResponseCode.INVALID_ORDER_ID);
		}
		if(orderEntity.getStatusCode() == OrderStatus.COMPLETED.getCode()) {
			throw new BaseException(ResponseCode.ORDER_ALREADY_COMPLETED);
		}
		setOrderAmounts(orderToUpdate, null);
//		orderToUpdate.setAmount(calculateAmount(orderToUpdate.getData()));
		orderToUpdate.setCheckinId(orderEntity.getCheckinId());
		orderToUpdate.setCreated(orderEntity.getCreated());
		orderToUpdate.setStatusCode(orderEntity.getStatusCode());
		orderDao.updateT(orderToUpdate);
		return getById(orderToUpdate.getId());
	}

	@Override
	public void deleteById(int orderId) throws BaseException {
		if(!isExist(orderId)) {
			throw new BaseException(ResponseCode.INVALID_ORDER_ID);
		}
		orderDao.deleteT(orderId);
	}

	@Override
	public boolean isExist(int id) {
		return orderDao.isExist(id);
	}

	@Override
	public OrderResponseDto setOrderStatus(OrderStatusRequestDto orderStatusRequestDto) throws BaseException {
		OrderEntity orderEntity = orderDao.getTById(orderStatusRequestDto.getId());
		if(orderEntity == null) {
			throw new BaseException(ResponseCode.INVALID_ORDER_ID);
		}
		if(orderEntity.getStatusCode() == OrderStatus.COMPLETED.getCode()) {
			throw new BaseException(ResponseCode.ORDER_ALREADY_COMPLETED);
		}
		orderEntity.setStatusCode(orderStatusRequestDto.getOrderStatusCode());
		orderDao.updateT(orderEntity);
		sendOrderStatusChangesNotification(orderEntity);
		return getById(orderStatusRequestDto.getId());
	}

	private void sendOrderStatusChangesNotification(OrderEntity orderEntity) {
		FCMNotifTemplate orderStatusTemplate = FCMNotifTemplate.OrderStatusTemplate();
		CustomerSessionEntity sessionEntity = customerSessionDao.getTById(orderEntity.getCheckinId());
		CustomerResponseDto customer = customerService.getById(sessionEntity.getCustomerId());
		orderStatusTemplate.formatBody(OrderStatus.getByCode(orderEntity.getStatusCode()).toString());
		FCMNotifUtil.trySend(orderStatusTemplate, Arrays.asList(customer.getNotificationToken()), FCMNotifTo.TO_MOB);
	}

	@Override
	public List<OrderResponseDto> getByCheckinId(int checkinId) throws BaseException {
		CustomerSessionEntity session = customerSessionDao.getTById(checkinId);
		if(session == null) {
			throw new BaseException(ResponseCode.INVALID_CHECKIN_ID);
		}
		CustomerResponseDto customer = customerService.getById(session.getCustomerId());
		RestaurantTableResponseDto table = restaurantTableService.getById(session.getTableId());
		List<OrderResponseDto> orders = OrderMapper.mapEntityListToResponseList(orderDao.getByCheckinId(checkinId));
		orders.forEach(order -> {
			order.setCustomer(customer);
			order.setTable(table);
		});
		return orders;
	}

	@Override
	public OrderResponseDto getResponseOrNull(OrderEntity request) {
		OrderResponseDto response = OrderMapper.mapEntityToResponse(request);
		if(response == null) {
			return null;
		}
		CustomerSessionEntity session = customerSessionDao.getTById(request.getCheckinId());
		if(session == null) {
			return null;
		}
		CustomerResponseDto customer = customerService.getById(session.getCustomerId());
		RestaurantTableResponseDto table = restaurantTableService.getById(session.getTableId());
		response.setCustomer(customer);
		response.setTable(table);
		return response;
	}

	private void setOrderAmounts(OrderEntity order, CustomerSessionEntity customerSessionEntity) throws BaseException {
		if(order == null) {
			return;
		}
		List<MenuItemInOrderPojo> orderItems = JsonUtils.convertJsonToList(order.getData(), MenuItemInOrderPojo.class);
		if(CollectionUtils.isEmpty(orderItems)) {
			throw new BaseException(ResponseCode.INVALID_DATA);
		}
		menuItemService.calculateOrderItemAmounts(orderItems);
		double orderAmountWithTax = 0;
		double orderTaxAmount = 0;
		double orderAmountWithoutTax = 0;
		for(MenuItemInOrderPojo orderItem : orderItems) {
			orderAmountWithTax += orderItem.getTotalAmount();
			orderTaxAmount += orderItem.getTaxAmount();
			orderAmountWithoutTax += orderItem.getAmount();
		}
		order.setData(JsonUtils.getJson(orderItems));
		order.setTaxAmount(NumberUtils.roundOffValue(orderTaxAmount));
		if(customerSessionEntity == null) {
			customerSessionEntity = customerSessionDao.getTById(order.getCheckinId());
			if(customerSessionEntity == null) {
				throw new BaseException(ResponseCode.INVALID_CHECKIN_ID);
			}
		}
		RestuarantResponseDto restaurant = RestaurantMapper.mapEntityToResponse(restaurantDao.getTById(customerSessionEntity.getRestaurantId()));
		double discountAmount = getDiscount(orderAmountWithoutTax, restaurant);
		order.setDiscountAmount(NumberUtils.roundOffValue(discountAmount));
		order.setAmount(NumberUtils.roundOffValue(orderAmountWithTax - discountAmount));
		order.setRemainingAmount(order.getAmount());
	}

	private double getDiscount(double orderAmountWithoutTax, RestuarantResponseDto restaurant) {
		List<Discount> discounts = restaurant.getDiscounts();
		if(CollectionUtils.isEmpty(discounts)) {
			return 0;
		}
		double discountAmount = 0;
		for(Discount discount : restaurant.getDiscounts()) {
			switch(discount.getType()) {
			case ON_BILL:
					discountAmount += discount.isPercentage()
					? NumberUtils.getPercentageValue(orderAmountWithoutTax, discount.getValue())
					: discount.getValue();
				break;
			}
		}
		return discountAmount;
	}
	
}
