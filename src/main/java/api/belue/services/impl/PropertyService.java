package api.belue.services.impl;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import api.belue.dao.IPropertyDao;
import api.belue.entites.Property;
import api.belue.services.IPropertyService;

@Service
@Transactional
public class PropertyService implements IPropertyService {

	@Autowired
	private IPropertyDao propertyDao;
	
	@Override
	public String getStringPropertyValue(String propertyName, String defaultValue) {
		Property property = propertyDao.get(propertyName);
		try {
			return property.getPropertyValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	@Override
	public Integer getIntegerPropertyValue(String propertyName, int defaultValue) {
		Property property = propertyDao.get(propertyName);
		try {
			return Integer.parseInt(property.getPropertyValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	@Override
	public Long getLongPropertyValue(String propertyName, long defaultValue) {
		Property property = propertyDao.get(propertyName);
		try {
			return Long.parseLong(property.getPropertyValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	@Override
	public Double getDoublePropertyValue(String propertyName, double defaultValue) {
		Property property = propertyDao.get(propertyName);
		try {
			return Double.parseDouble(property.getPropertyValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	@Override
	public Boolean getBooleanPropertyValue(String propertyName, boolean defaultValue) {
		Property property = propertyDao.get(propertyName);
		try {
			return Boolean.parseBoolean(property.getPropertyValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	@Override
	public void addOrModifyProperty(String propertyName, String proprtyValue) {
		propertyDao.addOrModify(propertyName, proprtyValue);
	}

	@Override
	public List<Property> getAll() {
		List<Property> properties = propertyDao.getAll();
		return properties;
	}

	@Override
	public List<String> getStringListPropertyValue(String propertyName, String defaultValue) {
		String value = getStringPropertyValue(propertyName, defaultValue);
		if(!StringUtils.isEmpty(value)){
			return Arrays.asList(value.split(","));
		}
		return null;
	}

}
