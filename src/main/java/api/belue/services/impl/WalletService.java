package api.belue.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.FCMNotifTemplate;
import api.belue.constants.FCMNotifTo;
import api.belue.constants.PaymentIdType;
import api.belue.constants.ResponseCode;
import api.belue.constants.TransactionType;
import api.belue.dao.ICustomerDao;
import api.belue.dao.IOrderDao;
import api.belue.dao.IWalletDao;
import api.belue.entites.CustomerEntity;
import api.belue.entites.OrderEntity;
import api.belue.entites.RestaurantEntity;
import api.belue.entites.WalletTransactionEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.WalletTransactionMapper;
import api.belue.response.dto.TransactionResponseDto;
import api.belue.services.IRestaurantNotificationService;
import api.belue.services.IRestaurantService;
import api.belue.services.IWalletService;
import api.belue.services.OrderProcessor;
import api.belue.utils.CodeUtil;
import api.belue.utils.FCMNotifUtil;

@Service
public class WalletService implements IWalletService {

	@Autowired
	protected IWalletDao walletDao;
	@Autowired
	protected ICustomerDao customerDao;
	@Autowired
	private IOrderDao orderDao;
	@Autowired
	protected IRestaurantNotificationService restaurantNotificationService;
	@Autowired
	private IRestaurantService restaurantService;

	private void withdraw(CustomerEntity customerEntity, WalletTransactionEntity walletTransactionEntity,
			List<OrderEntity> orders) {
		if(CollectionUtils.isEmpty(orders)) {
			return;
		}
		double walletBalance = customerEntity.getWalletBalance();
		orders.sort((o1, o2) -> Double.compare(o2.getRemainingAmount(), o1.getRemainingAmount())); // descending
		Consumer<OrderEntity> withdrawlTransactionAdder = order -> addWithdrawlTransaction(walletTransactionEntity,order);
		walletBalance = OrderProcessor.processOrders(orders, walletBalance, OrderProcessor.partialOrderProcessor, withdrawlTransactionAdder);
		walletBalance = OrderProcessor.processOrders(orders, walletBalance, OrderProcessor.unpaidOrderProcessor, withdrawlTransactionAdder);
		walletBalance = OrderProcessor.processOrders(orders, walletBalance, OrderProcessor.payOrderPartiallyProcessor, withdrawlTransactionAdder);
		updateBalance(customerEntity, walletBalance);
	}

	@Override
	public List<TransactionResponseDto> getTransactionsByCustomerIdAndType(int customerId, TransactionType type) {
		return WalletTransactionMapper.mapEntityListToResponseList(walletDao.getByCustomerIdAndType(customerId, type));
	}

	@Override
	public WalletTransactionEntity getEntityById(int transactionId) {
		return walletDao.getTById(transactionId);
	}

	@Override
	public void deposit(WalletTransactionEntity txn) throws BaseException {
		walletDao.updateT(txn);
		CustomerEntity customerEntity = customerDao.getTById(txn.getCustomerId());
		double newBalance = customerEntity.getWalletBalance() + txn.getAmount();
		updateBalance(customerEntity, newBalance);
		sendMoneyAddedNotification(txn, customerEntity);
		withdraw(customerEntity, txn, getOrders(txn));
	}

	@Override
	public WalletTransactionEntity getByUniqueId(String uniqueId) {
		return walletDao.getByUniqueId(uniqueId);
	}

	@Override
	public void saveTransaction(WalletTransactionEntity walletTransactionEntity) {
		walletDao.saveT(walletTransactionEntity);
	}

	@Override
	public void update(WalletTransactionEntity txn) {
		walletDao.updateT(txn);
	}

	protected List<OrderEntity> getOrders(WalletTransactionEntity walletTransactionEntity) throws BaseException {
		switch(PaymentIdType.getByCode(walletTransactionEntity.getOrderOrSessionType())) {
		case SESSION_ID:
			walletTransactionEntity.setOrderId(0);
			return orderDao.getByCheckinId(walletTransactionEntity.getCustomerSessionId());
		case ORDER_ID:
			OrderEntity orderEntity = orderDao.getTById(walletTransactionEntity.getOrderId());
			walletTransactionEntity.setCustomerSessionId(orderEntity.getCheckinId());
			return Arrays.asList(orderEntity);
		default:
			throw new BaseException(ResponseCode.INVALID_ID_TYPE);
		}
	}

	protected void addWithdrawlTransaction(WalletTransactionEntity walletTransactionEntity, OrderEntity order) {
		orderDao.updateT(order);
		WalletTransactionEntity walletTransactionEntityToSave = WalletTransactionMapper
				.cloneEntity(walletTransactionEntity);
		walletTransactionEntityToSave.setAmount(order.getAmount() - order.getRemainingAmount());
		walletTransactionEntityToSave.setOrderId(order.getId());
		walletTransactionEntityToSave.setTransactionType(TransactionType.WITHDRAWL);
		saveTransaction(walletTransactionEntityToSave);
	}

	protected void updateBalance(CustomerEntity customerEntity, double walletBalance) {
		customerEntity.setWalletBalance(walletBalance);
		customerDao.updateT(customerEntity);
	}

	private void sendMoneyAddedNotification(WalletTransactionEntity walletTransactionEntity,
			CustomerEntity customerEntity) {
		FCMNotifTemplate moneyPaidTemplate = FCMNotifTemplate.getMoneyPaidTemplate();
		moneyPaidTemplate.formatBody(CodeUtil.formatAsCurrency(walletTransactionEntity.getAmount()));
		// TO Customer
		FCMNotifUtil.trySend(moneyPaidTemplate, Arrays.asList(customerEntity.getNotificationToken()),
				FCMNotifTo.TO_MOB);
		moneyPaidTemplate.setReferenceId(walletTransactionEntity.getCustomerSessionId());
		RestaurantEntity restaurantEntity = restaurantService
				.getEntityBySessionId(walletTransactionEntity.getCustomerSessionId());
		if(restaurantEntity != null) {
			// TO restaurant
			restaurantNotificationService.sendAndSave(moneyPaidTemplate, restaurantEntity.getNotificationToken(),
					restaurantEntity.getId());
		}

	}

}
