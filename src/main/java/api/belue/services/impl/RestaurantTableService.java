package api.belue.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import api.belue.constants.FileType;
import api.belue.constants.Params;
import api.belue.constants.ResponseCode;
import api.belue.constants.SessionStatus;
import api.belue.dao.ICustomerSessionDao;
import api.belue.dao.IRestaurantDao;
import api.belue.dao.IRestaurantTableDao;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.RestaurantTableEntity;
import api.belue.exceptions.BaseException;
import api.belue.mapper.RestaurantTableMapper;
import api.belue.request.dto.AddRestaurantTableRequestDto;
import api.belue.request.dto.UpdateRestaurantTableRequestDto;
import api.belue.response.dto.RestaurantTableResponseDto;
import api.belue.services.IFileService;
import api.belue.services.IGetResponseService;
import api.belue.services.IRestaurantTableService;

@Service
public class RestaurantTableService implements IRestaurantTableService, IGetResponseService<RestaurantTableEntity, RestaurantTableResponseDto> {

	@Autowired
	private IRestaurantTableDao restaurantTableDao;
	@Autowired
	private IRestaurantDao restaurantDao;
	@Autowired
	private IFileService fileService;
	@Autowired
	private ICustomerSessionDao customerSessionDao;
	@Value("${project.prop.storeQROnCloud}")
	private Boolean storeQROnCloud;

	@Override
	public List<RestaurantTableResponseDto> getAll() {
		return getResponseListOrNull(restaurantTableDao.getAllT());
	}

	@Override
	public RestaurantTableResponseDto getById(int id) {
		return getResponseOrNull(restaurantTableDao.getTById(id));
	}

	@Override
	public RestaurantTableResponseDto save(AddRestaurantTableRequestDto request) throws BaseException, IOException {
		RestaurantTableEntity restaurantTableToSave = RestaurantTableMapper.mapAddRequestToEntity(request);
		if (restaurantTableToSave == null) {
			return null;
		}
		if(!restaurantDao.isExist(restaurantTableToSave.getRestaurantId())) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		RestaurantTableEntity restaurantTableEntity = restaurantTableDao.getByRestaurantIdAndTableNumber(restaurantTableToSave.getRestaurantId(), restaurantTableToSave.getTableNumber());
		if(restaurantTableEntity != null) {
			throw new BaseException(ResponseCode.TABLE_NUMBER_PRESENT);
		}
		restaurantTableDao.saveT(restaurantTableToSave);
		String qrImageUrl = Params.QR.UPLOAD_URL + restaurantTableToSave.getId();
//		Properties props = PropertiesUtil.getProperties(Params.APP.APP_PROPS_FILE);
		if (storeQROnCloud) {
			try {
				qrImageUrl = fileService.uploadFile(qrImageUrl, FileType.IMAGE);
			} catch (IOException e) {
				restaurantTableDao.deleteT(restaurantTableToSave.getId());
				throw e;
			}
		}
		restaurantTableToSave.setQrCodeUrl(qrImageUrl);
		restaurantTableDao.updateT(restaurantTableToSave);
		return getResponseOrNull(restaurantTableToSave);
	}

	@Override
	public RestaurantTableResponseDto update(UpdateRestaurantTableRequestDto request) throws BaseException {
		RestaurantTableEntity restaurantTableToUpdate = RestaurantTableMapper.mapUpdateRequestToEntity(request);
		if (restaurantTableToUpdate == null) {
			return null;
		}
		RestaurantTableEntity restaurantTableEntity = restaurantTableDao.getTById(restaurantTableToUpdate.getId());
		if(restaurantTableEntity == null) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_TABLE_ID);
		}
		if(restaurantTableToUpdate.getRestaurantId() != restaurantTableEntity.getRestaurantId()) { // restaurant id cannot be changed
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		RestaurantTableEntity restaurantTableEntityToCheckWith = restaurantTableDao.getByRestaurantIdAndTableNumber(restaurantTableToUpdate.getRestaurantId(), restaurantTableToUpdate.getTableNumber());
		if(restaurantTableEntityToCheckWith != null && restaurantTableEntityToCheckWith.getId() != restaurantTableEntity.getId()) {
			throw new BaseException(ResponseCode.TABLE_NUMBER_PRESENT);
		}
		restaurantTableToUpdate.setCreated(restaurantTableEntity.getCreated());
		restaurantTableToUpdate.setQrCodeUrl(restaurantTableEntity.getQrCodeUrl());
		restaurantTableDao.updateT(restaurantTableToUpdate);
		return getResponseOrNull(restaurantTableToUpdate);
	}

	@Override
	public void deleteById(int restaurantTableId) throws BaseException {
		if(!isExist(restaurantTableId)) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_TABLE_ID);
		}
		restaurantTableDao.deleteT(restaurantTableId);
	}

	@Override
	public List<RestaurantTableResponseDto> getAllTableByRestaurantId(int restaurantId) throws BaseException {
		if(!restaurantDao.isExist(restaurantId)) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		return getResponseListOrNull(restaurantTableDao.getAllTablesByRestaurantId(restaurantId));
	}

	@Override
	public boolean isExist(int id) {
		return restaurantTableDao.isExist(id);
	}

	@Override
	public RestaurantTableResponseDto getResponseOrNull(RestaurantTableEntity request) {
		RestaurantTableResponseDto response = RestaurantTableMapper.mapEntityToResponse(request);
		if(response == null) {
			return null;
		}
		if(response.isOccoupied()) {
			List<CustomerSessionEntity> sessions = customerSessionDao.getByTableIdAndSessionStatus(response.getId(), SessionStatus.CHECKED_IN.getCode());
			if(!CollectionUtils.isEmpty(sessions)) {
				response.setCheckInId(sessions.get(0).getId());
			}
		}
		return response;
	}

}
