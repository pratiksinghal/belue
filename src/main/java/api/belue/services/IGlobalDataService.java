package api.belue.services;

import api.belue.exceptions.BaseException;

public interface IGlobalDataService {
	
	public String get() throws BaseException;
	public String save(String data);

}
