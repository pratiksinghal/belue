package api.belue.services;

import java.io.IOException;

import api.belue.constants.FileType;
import api.belue.exceptions.BaseException;

public interface IFileService {

	public String saveFile(byte[] file, String fileName, FileType fileType) throws IOException, RuntimeException;
	public byte[] getFileFromUrl(String url, FileType fileType) throws IOException;
	public String uploadFile(Object file, FileType fileType) throws IOException, BaseException;
}
