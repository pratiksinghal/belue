package api.belue.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public interface IGetResponseService <Entity, Response> {
	
	public Response getResponseOrNull(Entity request);
	
	public default List<Response> getResponseListOrNull(List<Entity> request) {
		if(CollectionUtils.isEmpty(request)) {
			return null;
		}
		return request.parallelStream()
				.map(entity -> getResponseOrNull(entity))
				.filter(response -> response != null)
				.collect(Collectors.toList());
	}
}
