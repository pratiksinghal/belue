package api.belue.services;

import java.io.IOException;
import java.util.List;

import api.belue.exceptions.BaseException;

public interface IAbstractService<R, S, U> {
	public List<R> getAll();
	public R getById(int id);
	public R save(S itemToSave) throws BaseException, IOException;
	public R update(U itemToUpdate) throws BaseException, IOException;
	public void deleteById(int id) throws BaseException;
	public boolean isExist(int id);
}
