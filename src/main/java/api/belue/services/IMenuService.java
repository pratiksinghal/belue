package api.belue.services;

import java.util.List;

import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddMenuRequestDto;
import api.belue.request.dto.UpdateMenuRequestDto;
import api.belue.response.dto.MenuResponseDto;

public interface IMenuService extends IAbstractService<MenuResponseDto, AddMenuRequestDto, UpdateMenuRequestDto> {
	public List<MenuResponseDto> getAllByRestaurantId(int restauratId) throws BaseException;
}
