package api.belue.services;

import java.io.IOException;

import api.belue.entites.WalletTransactionEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddPaymentRequestDto;
import api.belue.request.dto.GetPaytmParamsRequestDto;
import api.belue.response.dto.PaymentParamsResponse;

public interface IPaymentService {
	public PaymentParamsResponse getPaytmParams(GetPaytmParamsRequestDto request) throws BaseException;
	void addPayment(AddPaymentRequestDto transaction) throws BaseException;
	Object verifyTransaction(WalletTransactionEntity entity) throws BaseException, IOException;
}
