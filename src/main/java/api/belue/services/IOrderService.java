package api.belue.services;

import java.util.List;

import api.belue.constants.OrderStatus;
import api.belue.constants.RoleType;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddOrderRequestDto;
import api.belue.request.dto.OrderStatusRequestDto;
import api.belue.request.dto.UpdateOrderRequestDto;
import api.belue.response.dto.OrderResponseDto;

public interface IOrderService extends IAbstractService<OrderResponseDto, AddOrderRequestDto, UpdateOrderRequestDto> {
	public List<OrderResponseDto> getByCheckinId(int checkinId) throws BaseException;
	public OrderResponseDto setOrderStatus(OrderStatusRequestDto orderStatusRequestDto) throws BaseException;
	public List<OrderResponseDto> getAll(OrderStatus orderStatus, RoleType userRole, int userId, boolean history) throws BaseException;
	List<OrderResponseDto> getAllNew(OrderStatus orderStatus, RoleType userRole, int userId, boolean history)
			throws BaseException;
}
