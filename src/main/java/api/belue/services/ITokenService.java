package api.belue.services;

import api.belue.constants.RoleType;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;

public interface ITokenService {
	public boolean isExist(String token, RoleType roleType);
	public String getToken(int userId, RoleType roleType);
	public TokenEntity getFromTokenValue(String token) throws BaseException;
	public void validateToken(String tokenValue) throws BaseException;
}
