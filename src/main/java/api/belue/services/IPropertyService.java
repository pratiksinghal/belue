package api.belue.services;

import java.util.List;

import api.belue.entites.Property;

public interface IPropertyService {

	public String getStringPropertyValue(String propertyName, String defaultValue);
	public Integer getIntegerPropertyValue(String propertyName, int defaultValue);
	public Long getLongPropertyValue(String propertyName, long defaultValue);
	public Double getDoublePropertyValue(String propertyName, double defaultValue);
	public Boolean getBooleanPropertyValue(String propertyName, boolean defaultValue);
	public List<Property> getAll();
	public void addOrModifyProperty(String properyName, String proprtyValue);
	public List<String> getStringListPropertyValue(String properyName, String defaultValue);
	
}
