package api.belue.request.dto;

import api.belue.pojo.CustomerRequestPojo;

public class AddCustomerRequestRequestDto extends CustomerRequestPojo {
	private int type;
	private int checkinId;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(int checkinId) {
		this.checkinId = checkinId;
	}
}
