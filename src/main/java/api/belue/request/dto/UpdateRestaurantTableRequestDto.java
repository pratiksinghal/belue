package api.belue.request.dto;

import api.belue.pojo.RestaurantTablePojo;

public class UpdateRestaurantTableRequestDto extends RestaurantTablePojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
