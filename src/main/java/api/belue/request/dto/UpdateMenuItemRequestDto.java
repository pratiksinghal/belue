package api.belue.request.dto;

import api.belue.pojo.MenuItemPojo;

public class UpdateMenuItemRequestDto extends MenuItemPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
