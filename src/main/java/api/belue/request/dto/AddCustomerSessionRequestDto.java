package api.belue.request.dto;

import api.belue.pojo.CustomerSessionPojo;

public class AddCustomerSessionRequestDto extends CustomerSessionPojo {
	private String qrCodeId;
	private int customerId;
	public String getQrCodeId() {
		return qrCodeId;
	}
	public void setQrCodeId(String qrCodeId) {
		this.qrCodeId = qrCodeId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
}
