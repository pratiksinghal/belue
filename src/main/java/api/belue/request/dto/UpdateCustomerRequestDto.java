package api.belue.request.dto;

import api.belue.pojo.CustomerPojo;

public class UpdateCustomerRequestDto extends CustomerPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
