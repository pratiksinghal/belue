package api.belue.request.dto;

import api.belue.pojo.OrderPojo;

public class AddOrderRequestDto extends OrderPojo {
	private int checkinId;
	
	public int getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(int checkinId) {
		this.checkinId = checkinId;
	}

}
