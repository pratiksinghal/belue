package api.belue.request.dto;

public class CustomerSessionCheckoutRequestDto {
	private int checkInId;
	private String paidInfo;
	
	public int getCheckInId() {
		return checkInId;
	}
	public void setCheckInId(int checkInId) {
		this.checkInId = checkInId;
	}
	public String getPaidInfo() {
		return paidInfo;
	}
	public void setPaidInfo(String paidInfo) {
		this.paidInfo = paidInfo;
	}
}
