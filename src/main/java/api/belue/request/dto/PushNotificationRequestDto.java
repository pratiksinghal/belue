package api.belue.request.dto;

import java.util.List;

public class PushNotificationRequestDto {

	private List<Integer> customerIds;
	private List<Integer> restaurantIds;
	private boolean isAll;
	private String title;
	private String body;
	private String message;

	public List<Integer> getCustomerIds() {
		return customerIds;
	}

	public void setCustomerIds(List<Integer> customerIds) {
		this.customerIds = customerIds;
	}

	public List<Integer> getRestaurantIds() {
		return restaurantIds;
	}

	public void setRestaurantIds(List<Integer> restaurantIds) {
		this.restaurantIds = restaurantIds;
	}

	public boolean isIsAll() {
		return isAll;
	}

	public void setAll(boolean isAll) {
		this.isAll = isAll;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
