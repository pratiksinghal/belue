package api.belue.request.dto;

import api.belue.pojo.WalletTransactionPojo;

public class AddPaymentRequestDto extends WalletTransactionPojo {
	private Object transactionMetaData;
	
	public Object getTransactionMetaData() {
		return transactionMetaData;
	}

	public void setTransactionMetaData(Object transactionMetaData) {
		this.transactionMetaData = transactionMetaData;
	}
	
}
