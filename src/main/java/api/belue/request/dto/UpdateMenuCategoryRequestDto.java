package api.belue.request.dto;

import api.belue.pojo.MenuCategoryPojo;

public class UpdateMenuCategoryRequestDto extends MenuCategoryPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
