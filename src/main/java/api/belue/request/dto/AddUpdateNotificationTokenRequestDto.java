package api.belue.request.dto;

public class AddUpdateNotificationTokenRequestDto {
	private int customerId;
	private int restaurantId;
	private String notificationToken;
	private String platform;
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getNotificationToken() {
		return notificationToken;
	}
	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
}
