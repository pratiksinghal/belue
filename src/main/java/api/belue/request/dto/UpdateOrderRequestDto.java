package api.belue.request.dto;

import api.belue.pojo.OrderPojo;

public class UpdateOrderRequestDto extends OrderPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
