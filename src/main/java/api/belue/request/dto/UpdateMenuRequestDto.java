package api.belue.request.dto;

import api.belue.pojo.MenuPojo;

public class UpdateMenuRequestDto extends MenuPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
