package api.belue.request.dto;

import api.belue.pojo.CustomerRequestPojo;

public class UpdateCustomerRequestStatusRequestDto extends CustomerRequestPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
