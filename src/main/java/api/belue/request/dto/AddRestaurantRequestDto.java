package api.belue.request.dto;


import api.belue.pojo.RestaurantPojo;

public class AddRestaurantRequestDto extends RestaurantPojo {
	
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
