package api.belue.request.dto;

import api.belue.pojo.CustomerPojo;

public class CustomerLoginRequestDto extends CustomerPojo {
	private String phoneNumber;
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
