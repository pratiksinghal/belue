package api.belue.request.dto;

import api.belue.pojo.CustomerPojo;

public class AddCustomerRequestDto extends CustomerPojo {
	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
