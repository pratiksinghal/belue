package api.belue.request.dto;

import java.util.List;

import api.belue.pojo.SessionTax;

public class UpdateCustomerSessionRequestDto {
	private int id;
	private List<SessionTax> taxes;
	private List<SessionTax> discounts;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<SessionTax> getTaxes() {
		return taxes;
	}
	public void setTaxes(List<SessionTax> taxes) {
		this.taxes = taxes;
	}
	public List<SessionTax> getDiscounts() {
		return discounts;
	}
	public void setDiscounts(List<SessionTax> discounts) {
		this.discounts = discounts;
	}
}
