package api.belue.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import api.belue.constants.FileType;
import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.response.BaseResponse;
import api.belue.response.dto.SaveFileResponseDto;
import api.belue.services.IFileService;
import api.belue.validators.UploadFileValidator;

@Controller
@RequestMapping(path = PathMappings.FileUpload.BASE_MAPPING)
public class FileUploadController {

	@Autowired
	private IFileService fileService;

	@RequestMapping(
			path = PathMappings.BASE, 
			method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<SaveFileResponseDto> uploadSize(
			@RequestHeader("Content-Type") String contentType,
			@RequestParam(value = PathMappings.FileUpload.FILE_PARAM_VAR, required = true) MultipartFile file,
			@RequestParam(value = PathMappings.FileUpload.FILE_TYPE_PARAM_VAR, defaultValue = "0") String fileTypeCodeString)
			throws Exception {
		System.out.println("Content Type is "+contentType);
		int fileTypeCode = Integer.parseInt(fileTypeCodeString);
		UploadFileValidator.validateFile(file, fileTypeCode);

		String url = fileService.uploadFile(file.getBytes(), FileType.getByCode(fileTypeCode));
		if (url == null) {
			throw new BaseException(ResponseCode.ERROR_IN_UPLOADING);
		}

		SaveFileResponseDto response = new SaveFileResponseDto();
		response.setUrl(url);
		return new BaseResponse<SaveFileResponseDto>(false, response, ResponseCode.OK);
	}
}
