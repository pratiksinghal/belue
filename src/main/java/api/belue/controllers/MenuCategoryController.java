package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddMenuCategoryRequestDto;
import api.belue.request.dto.UpdateMenuCategoryRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.MenuCategoryResponseDto;
import api.belue.services.IMenuCategoryService;
import api.belue.validators.MenuCategoryValidator;

@Controller
@RequestMapping(path = PathMappings.MenuCategory.BASE_MAPPING)
public class MenuCategoryController {

	@Autowired
	private IMenuCategoryService menuCategoryService;

	@RequestMapping(path = PathMappings.ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuCategoryResponseDto>> getAll() throws Exception {
		List<MenuCategoryResponseDto> responseList = menuCategoryService.getAll();
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_CATEGORY_FOUND);
		}
		return new BaseResponse<List<MenuCategoryResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<MenuCategoryResponseDto> getById(@PathVariable(PathMappings.ID) int id) throws Exception {
		MenuCategoryResponseDto response = menuCategoryService.getById(id);
		if (response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_CATEGORY_FOUND);
		}
		return new BaseResponse<MenuCategoryResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.SAVE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO??
	public BaseResponse<MenuCategoryResponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddMenuCategoryRequestDto menuCategoryToSave) throws Exception {
		MenuCategoryValidator.validateAddRequest(menuCategoryToSave);
		MenuCategoryResponseDto response = menuCategoryService.save(menuCategoryToSave);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuCategoryResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO??
	public BaseResponse<MenuCategoryResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateMenuCategoryRequestDto menuCategoryToUpdate) throws Exception {
		MenuCategoryValidator.validateUpdateRequest(menuCategoryToUpdate);
		MenuCategoryResponseDto response = menuCategoryService.update(menuCategoryToUpdate);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuCategoryResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO??
	public BaseResponse<MenuCategoryResponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		menuCategoryService.deleteById(id);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.MenuCategory.MENU, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuCategoryResponseDto>> getAllByMenuId(@PathVariable(PathMappings.ID) int menuId)
			throws Exception {
		List<MenuCategoryResponseDto> responseList = menuCategoryService.getAllByMenuId(menuId);
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_CATEGORY_FOUND);
		}
		return new BaseResponse<List<MenuCategoryResponseDto>>(false, responseList, ResponseCode.OK);
	}

}
