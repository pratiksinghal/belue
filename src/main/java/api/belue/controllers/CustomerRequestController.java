package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.CustomerRequestStatus;
import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerRequestRequestDto;
import api.belue.request.dto.UpdateCustomerRequestStatusRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.CustomerRequestReponseDto;
import api.belue.services.ICustomerRequestService;
import api.belue.services.ITokenService;
import api.belue.validators.CustomerRequestValidator;

@Controller
@RequestMapping(path = PathMappings.CustomerRequest.BASE_MAPPING)
public class CustomerRequestController {

	@Autowired
	private ICustomerRequestService customerRequestService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(
			path = PathMappings.ALL,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<CustomerRequestReponseDto>> getAll(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue) throws Exception {
		tokenService.validateToken(tokenValue);
		List<CustomerRequestReponseDto> responseList = customerRequestService.getAll();
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_REQUEST_FOUND);
		}
		return new BaseResponse<List<CustomerRequestReponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerRequestReponseDto> getById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int requestId) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerRequestReponseDto response = customerRequestService.getById(requestId);
		if(response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_REQUEST_FOUND);
		}
		return new BaseResponse<CustomerRequestReponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerRequestReponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddCustomerRequestRequestDto request) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerRequestValidator.validateAddRequest(request);
		CustomerRequestReponseDto response = customerRequestService.save(request);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerRequestReponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerRequestReponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateCustomerRequestStatusRequestDto request) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerRequestValidator.validateUpdateRequest(request);
		CustomerRequestReponseDto response = customerRequestService.update(request);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerRequestReponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerRequestReponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int requestId) throws Exception {
		tokenService.validateToken(tokenValue);
		customerRequestService.deleteById(requestId);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.CustomerRequest.RESTAURANT_ID,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<CustomerRequestReponseDto>> getAll(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int restaurantId,
			@RequestParam(
					value = PathMappings.CustomerRequest.STATUS_PARAM_VAR,
					required = false,
					defaultValue = "1") int requestStatusCode)
			throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerRequestValidator.validateGetByRestaurantRequest(requestStatusCode);
		List<CustomerRequestReponseDto> response = customerRequestService.getAllByRestaurantIdAndStatus(restaurantId,
				CustomerRequestStatus.getByCode(requestStatusCode));
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_REQUEST_FOUND);
		}
		return new BaseResponse<List<CustomerRequestReponseDto>>(false, response, ResponseCode.OK);
	}

}
