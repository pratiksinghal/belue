package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.OrderStatus;
import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddOrderRequestDto;
import api.belue.request.dto.OrderStatusRequestDto;
import api.belue.request.dto.UpdateOrderRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.OrderResponseDto;
import api.belue.services.IOrderService;
import api.belue.services.ITokenService;
import api.belue.validators.OrderValidator;

@Controller
@RequestMapping(path = PathMappings.Order.BASE_MAPPING)
public class OrderController {

	@Autowired
	private IOrderService orderService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(
			path = PathMappings.BASE,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<OrderResponseDto>> getAll(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestParam(value = PathMappings.Order.USER_PARAM_VAR) int user,
			@RequestParam(value = PathMappings.Order.ID_PARAM_VAR) int userId,
			@RequestParam(value = PathMappings.Order.STATUS_PARAM_VAR, required=false) int orderStatusCode,
			@RequestParam(value = PathMappings.Order.HISTORY_PARAM_VAR, required=false) int history) throws Exception {
		boolean historyBool = (history == 0) ? false : true;
		OrderValidator.validateOrderGetAllRequest(orderStatusCode, user, userId, historyBool);
		List<OrderResponseDto> response = orderService.getAll(OrderStatus.getByCode(orderStatusCode),
				RoleType.getByCode(user), userId, historyBool);
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_ORDER_FOUND);
		}
		return new BaseResponse<List<OrderResponseDto>>(false, response, ResponseCode.OK);
	}
	
	@RequestMapping(
			path = PathMappings.BASE+"new",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<OrderResponseDto>> getAllNew(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestParam(value = PathMappings.Order.USER_PARAM_VAR) int user,
			@RequestParam(value = PathMappings.Order.ID_PARAM_VAR) int userId,
			@RequestParam(value = PathMappings.Order.STATUS_PARAM_VAR, required=false) int orderStatusCode,
			@RequestParam(value = PathMappings.Order.HISTORY_PARAM_VAR, required=false) int history) throws Exception {
		boolean historyBool = (history == 0) ? false : true;
		OrderValidator.validateOrderGetAllRequest(orderStatusCode, user, userId, historyBool);
		List<OrderResponseDto> response = orderService.getAllNew(OrderStatus.getByCode(orderStatusCode),
				RoleType.getByCode(user), userId, historyBool);
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_ORDER_FOUND);
		}
		return new BaseResponse<List<OrderResponseDto>>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<OrderResponseDto>> getAll() throws Exception {
		List<OrderResponseDto> response = orderService.getAll();
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_ORDER_FOUND);
		}
		return new BaseResponse<List<OrderResponseDto>>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<OrderResponseDto> getById(@PathVariable(value = PathMappings.ID) int orderId) throws Exception {
		OrderResponseDto response = orderService.getById(orderId);
		if(response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_ORDER_FOUND);
		}
		return new BaseResponse<OrderResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<OrderResponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddOrderRequestDto orderToSave) throws Exception {
		OrderValidator.validateAddRequest(orderToSave);
		OrderResponseDto response = orderService.save(orderToSave);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<OrderResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<OrderResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateOrderRequestDto orderToUpdate) throws Exception {
		OrderValidator.validateUpdateRequest(orderToUpdate);
		OrderResponseDto response = orderService.update(orderToUpdate);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<OrderResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<OrderResponseDto> deleteById(@PathVariable(value = PathMappings.ID) int orderId)
			throws Exception {
		orderService.deleteById(orderId);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Order.ORDER_STATUS,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<OrderResponseDto> setOrderStatus(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(value = PathMappings.ID) int orderId,
			@RequestBody OrderStatusRequestDto orderStatusRequestDto)
			throws Exception {
		orderStatusRequestDto.setId(orderId);
		OrderValidator.validateOrderSetStatusRequest(orderStatusRequestDto);
		OrderResponseDto response = orderService.setOrderStatus(orderStatusRequestDto);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<OrderResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Order.CHECKIN_ID,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<OrderResponseDto>> getByCheckinId(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(value = PathMappings.ID) int checkinId) throws Exception {
		tokenService.validateToken(tokenValue);
		List<OrderResponseDto> response = orderService.getByCheckinId(checkinId);
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_ORDER_FOUND);
		}
		return new BaseResponse<List<OrderResponseDto>>(false, response, ResponseCode.OK);
	}

}
