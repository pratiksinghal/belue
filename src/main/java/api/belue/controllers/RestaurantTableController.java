package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddRestaurantTableRequestDto;
import api.belue.request.dto.UpdateRestaurantTableRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.RestaurantTableResponseDto;
import api.belue.services.IRestaurantTableService;
import api.belue.services.ITokenService;
import api.belue.validators.RestaurantTableValidator;

@Controller
@RequestMapping(path = PathMappings.RestaurantTable.BASE_MAPPING)
public class RestaurantTableController {

	@Autowired
	private IRestaurantTableService restaurantTableService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(
			path = PathMappings.ALL, 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<RestaurantTableResponseDto>> getAll() throws Exception {
		List<RestaurantTableResponseDto> responseList = restaurantTableService.getAll();
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_RESTAURANT_TABLE_FOUND);
		}
		return new BaseResponse<List<RestaurantTableResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<RestaurantTableResponseDto> getById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestaurantTableResponseDto response = restaurantTableService.getById(id);
		if(response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_RESTAURANT_TABLE_FOUND);
		}
		return new BaseResponse<RestaurantTableResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<RestaurantTableResponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddRestaurantTableRequestDto restaurantTableToSave) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		RestaurantTableValidator.validateAddRequest(restaurantTableToSave);
		if(token.getRoleType() != RoleType.RESTAURANT
				|| restaurantTableToSave.getRestaurantId() != token.getUserId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestaurantTableResponseDto response = restaurantTableService.save(restaurantTableToSave);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<RestaurantTableResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<RestaurantTableResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateRestaurantTableRequestDto restaurantTableToUpdate) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		RestaurantTableValidator.validateUpdateRequest(restaurantTableToUpdate);
		if(token.getRoleType() != RoleType.RESTAURANT
				|| restaurantTableToUpdate.getRestaurantId() != token.getUserId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestaurantTableResponseDto response = restaurantTableService.update(restaurantTableToUpdate);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<RestaurantTableResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<RestaurantTableResponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		RestaurantTableResponseDto tableToDelete = restaurantTableService.getById(id);
		if(tableToDelete == null) {
			throw new BaseException(ResponseCode.INVALID_RESTAURANT_TABLE_ID);
		}
		// unauthorized if trying to delete table of other restaurant
		if(token.getRoleType() != RoleType.RESTAURANT || tableToDelete.getRestaurantId() != token.getUserId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		restaurantTableService.deleteById(id);
		return new BaseResponse<RestaurantTableResponseDto>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.RestaurantTable.RESTAURANT,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<List<RestaurantTableResponseDto>> getAllByRestaurantId(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int restaurantId) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT || restaurantId != token.getUserId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		List<RestaurantTableResponseDto> responseList = restaurantTableService.getAllTableByRestaurantId(restaurantId);
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_RESTAURANT_TABLE_FOUND);
		}
		return new BaseResponse<List<RestaurantTableResponseDto>>(false, responseList, ResponseCode.OK);
	}

}
