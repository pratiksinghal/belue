package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddMenuItemRequestDto;
import api.belue.request.dto.UpdateMenuItemRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.MenuItemResponseDto;
import api.belue.services.IMenuItemService;
import api.belue.validators.MenuItemValidator;

@Controller
@RequestMapping(path = PathMappings.MenuItem.BASE_MAPPING)
public class MenuItemController {

	@Autowired
	private IMenuItemService menuItemService;

	@RequestMapping(
			path = PathMappings.ALL,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuItemResponseDto>> getAll() throws Exception {
		List<MenuItemResponseDto> responseList = menuItemService.getAll();
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_ITEM_FOUND);
		}
		return new BaseResponse<List<MenuItemResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<MenuItemResponseDto> getById(@PathVariable(PathMappings.ID) int id) throws Exception {
		MenuItemResponseDto response = menuItemService.getById(id);
		if (response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_ITEM_FOUND);
		}
		return new BaseResponse<MenuItemResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuItemResponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddMenuItemRequestDto menuItemToSave) throws Exception {
		MenuItemValidator.validateAddRequest(menuItemToSave);
		MenuItemResponseDto response = menuItemService.save(menuItemToSave);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuItemResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuItemResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateMenuItemRequestDto menuItemToUpdate) throws Exception {
		MenuItemValidator.validateUpdateRequest(menuItemToUpdate);
		MenuItemResponseDto response = menuItemService.update(menuItemToUpdate);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuItemResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuItemResponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		menuItemService.deleteById(id);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.MenuItem.MENU_CATEGORY,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuItemResponseDto>> getAllByMenuCategoryId(
			@PathVariable(PathMappings.ID) int menuCategoryId) throws Exception {
		List<MenuItemResponseDto> responseList = menuItemService.getAllByMenuCategogoryId(menuCategoryId);
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_ITEM_FOUND);
		}
		return new BaseResponse<List<MenuItemResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.MenuItem.MENU,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuItemResponseDto>> getAllByMenuId(@PathVariable(PathMappings.ID) int menuId)
			throws Exception {
		List<MenuItemResponseDto> responseList = menuItemService.getAllByMenuId(menuId);
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_ITEM_FOUND);
		}
		return new BaseResponse<List<MenuItemResponseDto>>(false, responseList, ResponseCode.OK);
	}
}
