package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddMenuRequestDto;
import api.belue.request.dto.UpdateMenuRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.MenuResponseDto;
import api.belue.services.IMenuService;
import api.belue.validators.MenuValidator;
import api.belue.services.ITokenService;

@Controller
@RequestMapping(path = PathMappings.Menu.BASE_MAPPING)
public class MenuController {

	@Autowired
	private IMenuService menuService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(path = PathMappings.ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuResponseDto>> getAll() throws Exception {
		List<MenuResponseDto> responseList = menuService.getAll();
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_FOUND);
		}
		return new BaseResponse<List<MenuResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<MenuResponseDto> getById(@PathVariable(PathMappings.ID) int menuId) throws Exception {
		MenuResponseDto response = menuService.getById(menuId);
		if (response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_FOUND);
		}
		return new BaseResponse<MenuResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.SAVE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuResponseDto> save(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddMenuRequestDto menuToSave) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if (token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != menuToSave.getRestaurantId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		MenuValidator.validateAddRequest(menuToSave);
		MenuResponseDto response = menuService.save(menuToSave);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateMenuRequestDto menuToUpdate) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if (token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != menuToUpdate.getRestaurantId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		MenuValidator.validateUpdateRequest(menuToUpdate);
		MenuResponseDto response = menuService.update(menuToUpdate);
		if (response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<MenuResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<MenuResponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int menuId) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if (token.getRoleType() != RoleType.RESTAURANT) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		menuService.deleteById(menuId);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(path = PathMappings.Menu.RESTAURANT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<MenuResponseDto>> getAllByRestaurantId(@PathVariable(PathMappings.ID) int restaurantId)
			throws Exception {
		List<MenuResponseDto> responseList = menuService.getAllByRestaurantId(restaurantId);
		if (CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_MENU_FOUND);
		}
		return new BaseResponse<List<MenuResponseDto>>(false, responseList, ResponseCode.OK);
	}
}
