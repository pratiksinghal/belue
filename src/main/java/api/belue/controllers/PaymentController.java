package api.belue.controllers;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.TransactionType;
import api.belue.entites.TokenEntity;
import api.belue.entites.WalletTransactionEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddPaymentRequestDto;
import api.belue.request.dto.GetPaytmParamsRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.PaymentParamsResponse;
import api.belue.response.dto.TransactionResponseDto;
import api.belue.services.IPaymentService;
import api.belue.services.ITokenService;
import api.belue.services.IWalletService;
import api.belue.validators.PaymentValidator;

@Controller
@RequestMapping(path = PathMappings.PaymentMappings.BASE_MAPPING)
public class PaymentController {

	private Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	private ITokenService tokenService;
	@Autowired
	private IWalletService walletService;
	@Autowired
	private IPaymentService paymentService;

	@RequestMapping(
			value = PathMappings.PaymentMappings.GET_PAYTM_PARAMS,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<PaymentParamsResponse> getPaytmParameters(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody GetPaytmParamsRequestDto request) throws BaseException {
		logger.info("Fetch paytm params request : {}", request.toString());
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		request.setCustomerId(token.getUserId());
		PaymentParamsResponse response = paymentService.getPaytmParams(request);
		return new BaseResponse<PaymentParamsResponse>(false, response, ResponseCode.OK);
	}

	@PostMapping(
			path = PathMappings.SAVE,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> addPayment(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddPaymentRequestDto request) throws BaseException {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		request.setCustomerId(token.getUserId());
		PaymentValidator.validateAddPayment(request);
		paymentService.addPayment(request);
		return BaseResponse.getOkNullResponse();
	}
	
	@GetMapping(
			path = PathMappings.PaymentMappings.TRANSACTIONS,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<TransactionResponseDto>> getTransactions(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestParam(value = PathMappings.PaymentMappings.CUSTOMER_ID_PARAM) int customerId,
			@RequestParam(value = PathMappings.PaymentMappings.TRANSACTION_TYPE_PARAM) int transactionType) throws BaseException {
		tokenService.validateToken(tokenValue);
		TransactionType tType = TransactionType.getByCode(transactionType);
		if(tType == null) {
			throw new BaseException(ResponseCode.INVALID_TRANSACTION_TYPE);
		}
		List<TransactionResponseDto> response = walletService.getTransactionsByCustomerIdAndType(customerId, tType);
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(false, null, ResponseCode.NO_TRANSACTION_FOUND);
		}
		return new BaseResponse<>(false, response, ResponseCode.OK);
	}
	
	@GetMapping(
			path = PathMappings.PaymentMappings.TRANSACTIONS+"/verify",
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object verifyTransaction(
			@RequestParam int transactionId) throws BaseException, IOException {
		WalletTransactionEntity entity = walletService.getEntityById(transactionId);
		return paymentService.verifyTransaction(entity);
	}
	
	
}
