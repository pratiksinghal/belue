package api.belue.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.response.BaseResponse;
import api.belue.services.IGlobalDataService;
import api.belue.utils.JsonUtils;

@Controller
@RequestMapping(path=PathMappings.Global.BASE)
public class GlobalDataController {
	
	@Autowired
	private IGlobalDataService globalDataService;
	
	@RequestMapping(path=PathMappings.BASE, method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> get() throws Exception {
		String response = globalDataService.get();
		if(response == null) {
			throw new BaseException(ResponseCode.NO_GLOBAL_DATA_FOUND);
		}
		return new BaseResponse<>(false, JsonUtils.fromJson(response, Object.class), ResponseCode.OK);
	}
	
	@RequestMapping(path=PathMappings.BASE, method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> save(
			@RequestBody String values) throws Exception {
		String response = globalDataService.save(values);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<>(false, JsonUtils.fromJson(response, Object.class), ResponseCode.OK);
	}

}
