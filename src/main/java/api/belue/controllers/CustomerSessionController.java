package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerSessionRequestDto;
import api.belue.request.dto.CustomerSessionCheckoutRequestDto;
import api.belue.request.dto.UpdateCustomerSessionRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.CustomerSessionResponseDto;
import api.belue.services.ICustomerSessionService;
import api.belue.services.ITokenService;
import api.belue.validators.CustomerSessionValidator;

@Controller
@RequestMapping(path = PathMappings.CustomerSession.BASE_MAPPING)
public class CustomerSessionController {

	@Autowired
	private ICustomerSessionService customerSessionService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(
			path = PathMappings.ALL,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<CustomerSessionResponseDto>> getByRestaurantIdAndSessionStatus(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestParam(value = PathMappings.CustomerSession.USER_PARAM_VAR) int user,
			@RequestParam(value = PathMappings.CustomerSession.ID_PARAM_VAR) int userId,
			@RequestParam(value = PathMappings.CustomerSession.STATUS_PARAM_VAR) int sessionStatusCode)
			throws BaseException {
		tokenService.validateToken(tokenValue);
		CustomerSessionValidator.validateGetRequest(sessionStatusCode, user, userId);
		
		List<CustomerSessionResponseDto> response = customerSessionService
				.getAll(SessionStatus.getByCode(sessionStatusCode), RoleType.getByCode(user), userId);
		
		if(CollectionUtils.isEmpty(response)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_CUSTOMER_SESSION_FOUND);
		}
		return new BaseResponse<List<CustomerSessionResponseDto>>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN WHO?
	public BaseResponse<CustomerSessionResponseDto> saveSession(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddCustomerSessionRequestDto sessionToAdd) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		CustomerSessionValidator.validateAddRequest(sessionToAdd);
		if(token.getRoleType() == RoleType.CUSTOMER) {
			sessionToAdd.setCustomerId(token.getUserId());
		} else if(sessionToAdd.getCustomerId() <= 0) {
			throw new BaseException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		CustomerSessionResponseDto response = customerSessionService.addUserSession(sessionToAdd);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerSessionResponseDto>(false, response, ResponseCode.OK);
	}
	
	@PatchMapping(
			path=PathMappings.UPDATE,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerSessionResponseDto> updatesession(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateCustomerSessionRequestDto updateRequest) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerSessionResponseDto response = customerSessionService.updateUserSession(updateRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerSessionResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.CustomerSession.CHECKOUT,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerSessionResponseDto> checkoutSession(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody CustomerSessionCheckoutRequestDto checkOutRequest) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerSessionResponseDto response = customerSessionService.checkoutCustomer(checkOutRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerSessionResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(value = PathMappings.ID) int customerSessionId) throws Exception {
		tokenService.validateToken(tokenValue);
		customerSessionService.deleteById(customerSessionId);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}
	
	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerSessionResponseDto> getById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(value = PathMappings.ID) int customerSessionId) throws Exception {
		tokenService.validateToken(tokenValue);
		CustomerSessionResponseDto response = customerSessionService.getById(customerSessionId);
		if(response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_CUSTOMER_SESSION_FOUND);
		}
		return new BaseResponse<CustomerSessionResponseDto>(false, response, ResponseCode.OK);
	}
}
