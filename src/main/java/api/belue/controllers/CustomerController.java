package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddCustomerRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.CustomerLoginRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.UpdateCustomerRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.CustomerLoginResponseDto;
import api.belue.response.dto.CustomerResponseDto;
import api.belue.services.ICustomerService;
import api.belue.services.ITokenService;
import api.belue.utils.JsonUtils;
import api.belue.validators.CustomerValidator;

@Controller
@RequestMapping(path = PathMappings.Customer.BASE_MAPPING)
public class CustomerController {

	@Autowired
	private ICustomerService customerService;
	@Autowired
	private ITokenService tokenService;

	@RequestMapping(
			path = PathMappings.ALL,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<List<CustomerResponseDto>> getAll() throws Exception {
		List<CustomerResponseDto> responseList = customerService.getAll();
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(true, null, ResponseCode.NO_CUSTOMER_FOUND);
		}
		return new BaseResponse<List<CustomerResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<CustomerResponseDto> getById(@PathVariable(value = PathMappings.ID) int customerId)
			throws Exception {
		CustomerResponseDto response = customerService.getById(customerId);
		if(response == null) {
			return new BaseResponse<>(true, null, ResponseCode.NO_CUSTOMER_FOUND);
		}
		return new BaseResponse<CustomerResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerLoginResponseDto> addCustomer(@RequestBody AddCustomerRequestDto updateRequest)
			throws Exception {
		CustomerValidator.validateAddRequest(updateRequest);
		CustomerLoginResponseDto response = customerService.saveWithLoginResponse(updateRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerLoginResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<CustomerResponseDto> updateCustomer(@RequestBody UpdateCustomerRequestDto updateRequest)
			throws Exception {
		CustomerValidator.validateUpdateRequest(updateRequest);
		CustomerResponseDto response = customerService.update(updateRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	// TODO TOKEN REQUIRED??
	public BaseResponse<?> deleteById(@PathVariable(value = PathMappings.ID) int customerId) throws Exception {
		customerService.deleteById(customerId);
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Customer.LOGIN,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<CustomerLoginResponseDto> loginCustomer(@RequestBody CustomerLoginRequestDto loginRequest)
			throws Exception {
		CustomerValidator.validateLoginRequest(loginRequest);
		CustomerLoginResponseDto response = customerService.loginCustomer(loginRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<CustomerLoginResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Customer.PHONE_NUM_CHECK,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<Boolean> isPhoneNumberExists(@RequestBody AddCustomerRequestDto request) throws Exception {
		boolean phoneNumberExists = customerService.isPhoneNumberExists(request.getPhoneNumber());
		return new BaseResponse<Boolean>(false, phoneNumberExists, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Customer.SEND_PUSH_NOTIFICATION,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<Object> sendPushNotification(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody PushNotificationRequestDto request)
			throws Exception {
		tokenService.validateToken(tokenValue);
		String response = customerService.sendPushNotification(request);
		if(StringUtils.isEmpty(response)) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<Object>(false, JsonUtils.fromJson(response, Object.class), ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Customer.FCM_TOKEN,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<Object> addUpdateNotificationToken(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddUpdateNotificationTokenRequestDto request)
			throws Exception {
		int customerId = tokenService.getFromTokenValue(tokenValue).getUserId();
		request.setCustomerId(customerId);
		CustomerValidator.validateNotificationTokenRequest(request);
		customerService.addUpdateNotificationToken(request);
		return new BaseResponse<Object>(false, null, ResponseCode.OK);
	}
}
