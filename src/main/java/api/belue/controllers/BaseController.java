package api.belue.controllers;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.response.BaseResponse;

public abstract class BaseController<R, S, U> {

	@RequestMapping(path = PathMappings.ALL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public abstract BaseResponse<List<R>> getAll() throws Exception;

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public abstract BaseResponse<R> getById(@PathVariable(PathMappings.ID) int id) throws Exception;

	@RequestMapping(path = PathMappings.SAVE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public abstract BaseResponse<R> save(@RequestBody S itemToSave) throws Exception;

	@RequestMapping(path = PathMappings.UPDATE, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public abstract BaseResponse<R> update(@RequestBody U itemToUpdate) throws Exception;

	@RequestMapping(path = PathMappings.ID_PARAM, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public abstract BaseResponse<R> deleteById(@PathVariable(PathMappings.ID) int id) throws Exception;
}
