package api.belue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import api.belue.constants.PathMappings;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.entites.TokenEntity;
import api.belue.exceptions.BaseException;
import api.belue.pojo.RestaurantStat;
import api.belue.request.dto.AddRestaurantRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.ChangePasswordRequestDto;
import api.belue.request.dto.PushNotificationRequestDto;
import api.belue.request.dto.RestaurantLoginRequestDto;
import api.belue.request.dto.UpdateRestaurantRequestDto;
import api.belue.response.BaseResponse;
import api.belue.response.dto.RestaurantLoginResponseDto;
import api.belue.response.dto.RestaurantNotificationResposeDto;
import api.belue.response.dto.RestuarantResponseDto;
import api.belue.services.IRestaurantNotificationService;
import api.belue.services.IRestaurantService;
import api.belue.services.ITokenService;
import api.belue.utils.DateUtil;
import api.belue.utils.JsonUtils;
import api.belue.validators.RestaurantValidator;

@Controller
@RequestMapping(path = PathMappings.Restaurant.BASE_MAPPING)
public class RestaurantController {

	@Autowired
	private IRestaurantService restaurantService;
	@Autowired
	private ITokenService tokenService;
	@Autowired
	private IRestaurantNotificationService notificationService;

	@RequestMapping(
			path = PathMappings.ALL,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<RestuarantResponseDto>> getAll() throws Exception {
		List<RestuarantResponseDto> responseList = restaurantService.getAll();
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(false, null, ResponseCode.NO_RESTAURANT_FOUND);
		}
		return new BaseResponse<List<RestuarantResponseDto>>(false, responseList, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestuarantResponseDto> getById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != id) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestuarantResponseDto response = restaurantService.getById(id);
		if(response == null) {
			return new BaseResponse<RestuarantResponseDto>(true, null, ResponseCode.NO_RESTAURANT_FOUND);
		}
		return new BaseResponse<RestuarantResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.SAVE,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestuarantResponseDto> save(@RequestBody AddRestaurantRequestDto restaurantToSave)
			throws Exception {
		RestaurantValidator.validateAddRequest(restaurantToSave);
		RestuarantResponseDto response = restaurantService.save(restaurantToSave);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<RestuarantResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.UPDATE,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestuarantResponseDto> update(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody UpdateRestaurantRequestDto restaurantToUpdate) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != restaurantToUpdate.getId()) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestaurantValidator.validateUpdateRequest(restaurantToUpdate);
		RestuarantResponseDto response = restaurantService.update(restaurantToUpdate);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<RestuarantResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.ID_PARAM,
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestuarantResponseDto> deleteById(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != id) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		restaurantService.deleteById(id);
		return new BaseResponse<RestuarantResponseDto>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.LOGIN,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestaurantLoginResponseDto> login(@RequestBody RestaurantLoginRequestDto loginRequest)
			throws Exception {
		RestaurantValidator.validateLoginRequest(loginRequest);
		RestaurantLoginResponseDto response = restaurantService.login(loginRequest);
		if(response == null) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<RestaurantLoginResponseDto>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Restaurant.CHANGE_PASSWORD,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> changePassword(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody ChangePasswordRequestDto request)
			throws Exception {
		tokenService.validateToken(tokenValue);
		RestaurantValidator.validateChangePasswordRequest(request);
		restaurantService.changePassword(request);
		return BaseResponse.getOkNullResponse();
	}

	@RequestMapping(
			path = PathMappings.Restaurant.STATS,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<RestaurantStat> getStats(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int id,
			@RequestParam String startDate,
			@RequestParam String endDate) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		if(token.getRoleType() != RoleType.RESTAURANT || token.getUserId() != id) {
			throw new BaseException(ResponseCode.NOT_ALLOWED);
		}
		RestaurantStat response = restaurantService.getStats(id,
				DateUtil.parseDate(startDate, DateUtil.DATE_PATTERN),
				DateUtil.parseDate(endDate, DateUtil.DATE_PATTERN));
		return new BaseResponse<>(false, response, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Restaurant.SEND_PUSH_NOTIFICATION,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<Object> sendPushNotification(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody PushNotificationRequestDto request) throws Exception {
		tokenService.validateToken(tokenValue);
		String response = restaurantService.sendPushNotification(request);
		if(StringUtils.isEmpty(response)) {
			throw new BaseException(ResponseCode.ERROR);
		}
		return new BaseResponse<Object>(false, JsonUtils.fromJson(response, Object.class), ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Restaurant.FCM_TOKEN,
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<Object> addUpdateNotificationToken(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestBody AddUpdateNotificationTokenRequestDto request)
			throws Exception {
		int restaurantId = tokenService.getFromTokenValue(tokenValue).getUserId();
		request.setRestaurantId(restaurantId);
		RestaurantValidator.validateNotificationTokenRequest(request);
		restaurantService.addUpdateNotificationToken(request);
		return new BaseResponse<Object>(false, null, ResponseCode.OK);
	}

	@RequestMapping(
			path = PathMappings.Restaurant.NOTIFICATION_ID,
			method = RequestMethod.PATCH,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<?> setNotificationAsRead(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@PathVariable(PathMappings.ID) int notifId)
			throws Exception {
		tokenService.validateToken(tokenValue);
		notificationService.setRead(notifId, true);
		return BaseResponse.getOkNullResponse();
	}

	@RequestMapping(
			path = PathMappings.Restaurant.NOTIFICATION,
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse<List<RestaurantNotificationResposeDto>> getAllNotifications(
			@RequestHeader(value = PathMappings.HeaderValues.TOKEN) String tokenValue,
			@RequestParam(required = false, defaultValue = "-1") Integer read) throws Exception {
		TokenEntity token = tokenService.getFromTokenValue(tokenValue);
		List<RestaurantNotificationResposeDto> responseList = null;
		if(read.equals(-1)) {
			responseList = notificationService.getByRestaurantId(token.getUserId());
		} else {
			boolean isRead = read == 0 ? false : true;
			responseList = notificationService.getByRestaurantIdAndReadStatus(token.getUserId(), isRead);
		}
		if(CollectionUtils.isEmpty(responseList)) {
			return new BaseResponse<>(false, null, ResponseCode.NO_NOTIFICATION_FOUND);
		}
		return new BaseResponse<>(false, responseList, ResponseCode.OK);
	}

}
