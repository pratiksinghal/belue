package api.belue.dao;

import java.util.List;

import api.belue.entites.CustomerEntity;

public interface ICustomerDao extends IAbstractDao<CustomerEntity> {

	public CustomerEntity getByPhoneNumber(String phoneNumber);
	public List<String> getAllNotificationTokens();
	public List<String> getNotificationTokensByCustomerIds(List<Integer> customerIds);

}
