package api.belue.dao;

import java.util.Date;
import java.util.List;

import api.belue.constants.OrderStatus;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.entites.OrderEntity;
import api.belue.pojo.RestaurantStat;

public interface IOrderDao extends IAbstractDao<OrderEntity> {

	public List<OrderEntity> getByCheckinId(int checkinId);
	public List<OrderEntity> getByCheckinIds(List<Integer> checkinIds);
	public List<OrderEntity> getByOrderStatusAndCheckinIds(List<Integer> checkinIds, int orderStatus);
	public List<OrderEntity> getByOrderStatusAndCheckinIds(List<Integer> checkinIds, List<Integer> orderStatus);
	public boolean setOrderStatus(List<Integer> orderIds, int orderStatus);

	public List<?> getWithCustomer(RoleType roleType, int userId, SessionStatus sessionStatus, OrderStatus orderStatus);
	public List<?> getWithCustomer(RoleType roleType, int userId, OrderStatus orderStatus);
	public List<?> getWithCustomer(RoleType roleType, int userId, SessionStatus sessionStatus);
	public RestaurantStat getStat(int id, Date startDate, Date endDate);

}
