package api.belue.dao;

import java.util.List;

import api.belue.entites.RestaurantTableEntity;

public interface IRestaurantTableDao extends IAbstractDao<RestaurantTableEntity> {
	public List<RestaurantTableEntity> getAllTablesByRestaurantId(int restaurantId);
	public RestaurantTableEntity getByRestaurantIdAndTableNumber(int restaurantId, int tableNumber);
}
