package api.belue.dao;

import java.util.List;

import api.belue.entites.MenuEntity;

public interface IMenuDao extends IAbstractDao<MenuEntity> {
	public List<MenuEntity> getAllByRestaurantId(int restaurantId);
}
