package api.belue.dao;

import java.util.List;

import api.belue.entites.CustomerSessionEntity;

public interface ICustomerSessionDao extends IAbstractDao<CustomerSessionEntity> {
	public List<CustomerSessionEntity> getByCustomerIdAndSessionStatus(int customerId, int sessionStatusCode);
	public List<CustomerSessionEntity> getByRestaurantIdAndSessionStatus(int restaurantId, int sessionStatusCode);
	public List<CustomerSessionEntity> getByRestaurantId(int restaurantId);
	public List<CustomerSessionEntity> getByCustomerId(int customerId);
	public List<CustomerSessionEntity> getByTableIdAndSessionStatus(int tableId, int sessionstatusCode);
}