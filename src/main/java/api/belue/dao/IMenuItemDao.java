package api.belue.dao;

import java.util.List;

import api.belue.entites.MenuItemEntity;

public interface IMenuItemDao extends IAbstractDao<MenuItemEntity> {
	public List<MenuItemEntity> getAllByMenuCategogoryId(int menuCategoryId);
	public List<MenuItemEntity> getAllByMenuId(int menuId);
}
