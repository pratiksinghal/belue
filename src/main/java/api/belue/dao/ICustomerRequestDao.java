package api.belue.dao;

import java.util.List;

import api.belue.entites.CustomerRequestEntity;

public interface ICustomerRequestDao extends IAbstractDao<CustomerRequestEntity> {

	public List<CustomerRequestEntity> getByCheckinIdsAndStatus(List<Integer> checkinIds, int statusCode);
	public void setStatusByIds(List<Integer> requestIds, int statusCode); 
}
