package api.belue.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IPropertyDao;
import api.belue.entites.Property;

@Repository
@Transactional
public class PropertyDao extends AbstractDao<Property> implements IPropertyDao {

	public PropertyDao() {
		super(EntityDetails.Property.ENTITY_NAME,Property.class);
	}

	public void save(Property property) {
		saveT(property);
	}

	public void update(Property property) {
		updateT(property);
	}

	public List<Property> getAll() {
		return getAllT();
	}

	@Override
	public Property get(String propertyName) {
		String hql = String.format("from " + EntityDetails.Property.ENTITY_NAME + " where propertyName='%s'",propertyName);
		return getT(hql);
	}

	public Property get(int id) {
		return getTById(id);
	}

	public void delete(int id) {
		deleteT(id);
	}

	@Override
	public void delete(String propertyName) {
		String hql = String.format("delete " + FROM_ENTITY + " where propertyName='%s'",propertyName);
		super.executeUpdate(hql);
	}

	@Override
	public void addOrModify(String propertyName, String proprtyValue) {
		delete(propertyName);
		save(propertyName, proprtyValue);
	}

	@Override
	public void save(String propertyName, String propertyValue) {
		Property property = new Property();
		property.setPropertyName(propertyName);
		property.setPropertyValue(propertyValue);
		save(property);
	}

}
