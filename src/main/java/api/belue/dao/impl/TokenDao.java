package api.belue.dao.impl;


import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.ITokenDao;
import api.belue.entites.TokenEntity;

@Repository
public class TokenDao extends AbstractDao<TokenEntity> implements ITokenDao {

	
	
	public TokenDao() {
		super(EntityDetails.Token.ENTITY_NAME, TokenEntity.class);
	}

	@Override
	public TokenEntity getByTokenValue(String token) {
		String hql = "from " + EntityDetails.Token.ENTITY_NAME + " where " + 
				EntityDetails.Token.TOKEN_VALUE + " ='" + token + "' ";
		return super.getT(hql);
	}

	@Override
	public TokenEntity getByUserIdAndRoleType(int userId, int roleType) {
		String hql = "from " + EntityDetails.Token.ENTITY_NAME + " where " + 
				               EntityDetails.Token.USER_ID     + "="       + userId  + " and " +
				               EntityDetails.Token.ROLE_TYPE   + "="       + roleType;
		return super.getT(hql);
	}

	@Override
	public void updateT(TokenEntity tokenToUpdate) { 
		String hql = " update " + EntityDetails.Token.ENTITY_NAME + " set " +
								 EntityDetails.Token.TOKEN_VALUE + " ='" + tokenToUpdate.getTokenValue() + "' where " +
								 EntityDetails.Token.USER_ID     + "="       + tokenToUpdate.getUserId() + " and " + 
								 EntityDetails.Token.ROLE_TYPE + "=" + tokenToUpdate.getRoleTypeCode();
		super.executeUpdate(hql);
	}
	
}
