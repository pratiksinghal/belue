package api.belue.dao.impl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.constants.SortOrder;
import api.belue.dao.IRestaurantNotificationDao;
import api.belue.entites.RestaurantNotificationsEntity;

@Repository
public class RestaurantNotificationDao extends AbstractDao<RestaurantNotificationsEntity> implements IRestaurantNotificationDao, EntityDetails.RestaurantNotifications {

	public RestaurantNotificationDao() {
		super(ENTITY_NAME, RestaurantNotificationsEntity.class);
	}

	@Override
	public List<RestaurantNotificationsEntity> getByRestaurantId(int restaurantId) {
		String hql = String.format(FROM_ENTITY + " WHERE %s=%d" + ORDER_BY + SortOrder.DESC.getQueryOrderString(), RESTAURANT_ID, restaurantId, CREATED);
		return getAllT(hql);
	}

	@Override
	public List<RestaurantNotificationsEntity> getByRestaurantIdAndReadStatus(int restaurantId, boolean readStatus) {
		String hql = String.format(FROM_ENTITY + " WHERE %s=%d AND %s=:isRead" + ORDER_BY + SortOrder.DESC.getQueryOrderString(), RESTAURANT_ID, restaurantId, IS_READ, CREATED);
		Query<RestaurantNotificationsEntity> query = getQueryObject(hql);
		query.setParameter("isRead", readStatus);
		return getAllT(query);
	}
}