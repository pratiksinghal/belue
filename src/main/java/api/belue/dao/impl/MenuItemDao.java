package api.belue.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IMenuItemDao;
import api.belue.entites.MenuItemEntity;

@Repository
public class MenuItemDao extends AbstractDao<MenuItemEntity> implements IMenuItemDao {

	public MenuItemDao() {
		super(EntityDetails.MenuItem.ENTITY_NAME, MenuItemEntity.class);
	}

	@Override
	public List<MenuItemEntity> getAllByMenuCategogoryId(int menuCategoryId) {
		String hql = "from " + EntityDetails.MenuItem.ENTITY_NAME + " where " + 
					EntityDetails.MenuItem.CATEGORY_ID + "=" + menuCategoryId;
		return super.getAllT(hql);
	}

	@Override
	public List<MenuItemEntity> getAllByMenuId(int menuId) {
		String hql = "from " + EntityDetails.MenuItem.ENTITY_NAME + " where " + 
				EntityDetails.MenuItem.MENU_ID + "=" + menuId;
	return super.getAllT(hql);
	}
	
}
