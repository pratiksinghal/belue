package api.belue.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IRestaurantDao;
import api.belue.entites.RestaurantEntity;

@Repository
@Transactional
public class RestaurantDao extends AbstractDao<RestaurantEntity> implements IRestaurantDao {

	private static final String ENTITY_NAME = RestaurantEntity.class.getSimpleName();
	private static final String USERNAME = "username";
	
	public RestaurantDao() {
		super(ENTITY_NAME, RestaurantEntity.class);
	}

	@Override
	public RestaurantEntity getByUsername(String username) {
		String hql = "from " + ENTITY_NAME + " where " + USERNAME + "='" + username + "'";
		return super.getT(hql);
	}
	
	@Override
	public List<String> getAllNotificationTokens() {
		String hql = "Select " + EntityDetails.Restaurant.NOTIFICATION_TOKEN + " " + FROM_ENTITY;
		return getSession().createQuery(hql, String.class).list();
	}

	@Override
	public List<String> getNotificationTokensByRestaurantIds(List<Integer> restaurantIds) {
		String hql = "Select " + EntityDetails.Restaurant.NOTIFICATION_TOKEN + " " + FROM_ENTITY + " where " + 
				EntityDetails.Restaurant.ID + " in(:restaurantIds)";
		Query<String> query = getSession().createQuery(hql, String.class);
		query.setParameter("restaurantIds", restaurantIds);
		return query.list();
	}
}
