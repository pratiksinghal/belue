package api.belue.dao.impl;

import api.belue.constants.EntityDetails;
import api.belue.dao.IDeviceDao;
import api.belue.entites.DeviceEntity;

public class DeviceDao extends AbstractDao<DeviceEntity> implements IDeviceDao {

	public DeviceDao() {
		super(EntityDetails.Device.ENTITY_NAME, DeviceEntity.class);
	}

	@Override
	public DeviceEntity getBydeviceIdAndPlatform(String deviceId, String platform) {
		String hql = "from " + EntityDetails.Device.ENTITY_NAME + " where " +
					EntityDetails.Device.DEVICE_ID + "='" + deviceId + "' and " +
					EntityDetails.Device.PLATFORM + " ='" + platform + "'";
		
		return super.getT(hql);
	}
	
}
