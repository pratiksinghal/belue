package api.belue.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.constants.OrderStatus;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.dao.IOrderDao;
import api.belue.entites.CustomerEntity;
import api.belue.entites.CustomerSessionEntity;
import api.belue.entites.OrderEntity;
import api.belue.pojo.RestaurantStat;

@Repository
public class OrderDao extends AbstractDao<OrderEntity> implements IOrderDao, EntityDetails.Order {

	public OrderDao() {
		super(EntityDetails.Order.ENTITY_NAME, OrderEntity.class);
	}

	@Override
	public List<OrderEntity> getByCheckinId(int checkinId) {
		String hql = "from " + EntityDetails.Order.ENTITY_NAME + " where "
				+ EntityDetails.Order.CHECKIN_ID + "=" + checkinId;
		return super.getAllT(hql);
	}

	@Override
	public List<OrderEntity> getByOrderStatusAndCheckinIds(List<Integer> checkinIds, int orderStatus) {
		List<Integer> orderStatuses = new ArrayList<>(1);
		orderStatuses.add(orderStatus);
		return getByOrderStatusAndCheckinIds(checkinIds, orderStatuses);
	}

	@Override
	public List<OrderEntity> getByOrderStatusAndCheckinIds(List<Integer> checkinIds, List<Integer> orderStatus) {
		String hql = "from " + EntityDetails.Order.ENTITY_NAME + " where "
				+ EntityDetails.Order.STATUS + " in(:orderStatuses) " + " and "
				+ EntityDetails.Order.CHECKIN_ID + " in(:checkinIds)";
		Query<OrderEntity> query = getQueryObject(hql);
		query.setParameterList("checkinIds", checkinIds);
		query.setParameterList("orderStatuses", orderStatus);
		return getAllT(query);
	}

	@Override
	public List<?> getWithCustomer(RoleType roleType, int userId, OrderStatus orderStatus) {

		String hql = "FROM %s order" +
				" JOIN %s session" +
				" ON order.%s=session.%s" +
				" JOIN %s cus" +
				" ON session.%s=cus.%s" +
				" WHERE ";
		hql = String.format(hql,
				EntityDetails.Order.ENTITY_NAME,
				EntityDetails.CustomerSession.ENTITY_NAME,
				EntityDetails.Order.CHECKIN_ID,
				EntityDetails.CustomerSession.ID,
				EntityDetails.Customer.ENTITY_NAME,
				EntityDetails.CustomerSession.CUSTOMER_ID,
				EntityDetails.Customer.ID);

		switch(roleType) {
		case RESTAURANT:
			hql += " session.%s=:userId";
			hql = String.format(hql, EntityDetails.CustomerSession.RESTAURANT_ID);
			break;
		case CUSTOMER:
			hql += "	 cus.%s=:userId";
			hql = String.format(hql, EntityDetails.Customer.ID);
			break;
		}

		hql += " AND order.%s=:orderStatus";
		hql = String.format(hql, EntityDetails.Order.STATUS);

		Query<?> q = getSession().createQuery(hql);
		q.setParameter("userId", userId);
		q.setParameter("orderStatus", orderStatus.getCode());
		return q.list();

	}

	@Override
	public List<?> getWithCustomer(RoleType roleType, int userId, SessionStatus sessionStatus) {
		String hql = "FROM %s order" +
				" JOIN %s session" +
				" ON order.%s=session.%s" +
				" JOIN %s cus" +
				" ON session.%s=cus.%s" +
				" WHERE session.%s=:sessionStatus ";
		hql = String.format(hql,
				EntityDetails.Order.ENTITY_NAME,
				EntityDetails.CustomerSession.ENTITY_NAME,
				EntityDetails.Order.CHECKIN_ID,
				EntityDetails.CustomerSession.ID,
				EntityDetails.Customer.ENTITY_NAME,
				EntityDetails.CustomerSession.CUSTOMER_ID,
				EntityDetails.Customer.ID,
				EntityDetails.CustomerSession.SESSION_STATUS_CODE);

		switch(roleType) {
		case RESTAURANT:
			hql += " AND session.%s=:userId";
			hql = String.format(hql, EntityDetails.CustomerSession.RESTAURANT_ID);
			break;
		case CUSTOMER:
			hql += " AND	 cus.%s=:userId";
			hql = String.format(hql, EntityDetails.Customer.ID);
			break;
		}
		System.out.println(hql);
		Query<?> q = getSession().createQuery(hql);
		q.setParameter("userId", userId);
		q.setParameter("sessionStatus", sessionStatus.getCode());
		return q.list();
	}

	@Override
	public List<OrderEntity> getByCheckinIds(List<Integer> checkinIds) {
		String hql = "from " + EntityDetails.Order.ENTITY_NAME + " where "
				+ EntityDetails.Order.CHECKIN_ID + " in(:checkinIds)";
		Query<OrderEntity> query = getQueryObject(hql);
		query.setParameterList("checkinIds", checkinIds);
		return getAllT(query);
	}

	@Override
	public boolean setOrderStatus(List<Integer> orderIds, int orderStatus) {
		String hql = "update " + EntityDetails.Order.ENTITY_NAME + " set " +
				EntityDetails.Order.STATUS + "=" + orderStatus + " where " +
				EntityDetails.Order.ID + " in(:orderIds)";
		@SuppressWarnings("rawtypes")
		Query query = getSession().createQuery(hql);
		query.setParameter("orderIds", orderIds);
		query.executeUpdate();
		return true;
	}

	@Override
	public RestaurantStat getStat(int id, Date startDate, Date endDate) {
		String hql = String.format("select count(%s), sum(%s) " + FROM_ENTITY +
				" where %s in(select id from %s where %s=:restaurantId) AND " +
				" %s BETWEEN DATE(:startDate) AND DATE(:endDate)",
				ID, AMOUNT, CHECKIN_ID,
				EntityDetails.CustomerSession.ENTITY_NAME,
				EntityDetails.CustomerSession.RESTAURANT_ID, CREATED);
		Query<?> query = getSession().createQuery(hql);
		query.setParameter("restaurantId", id);
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		List<?> list = query.list();
		RestaurantStat response = new RestaurantStat();
		Object[] row = (Object[]) list.get(0);
		try {
			response.setNumOfOrders(Long.parseLong(String.valueOf(row[0])));
			response.setAmount(Double.parseDouble(String.valueOf(row[1])));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public List<?> getWithCustomer(RoleType roleType, int userId, SessionStatus sessionStatus,
			OrderStatus orderStatus) {

		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery<Object> criteria = builder.createQuery();
		Root<OrderEntity> orderRoot = criteria.from(entityClass);
		Root<CustomerSessionEntity> sessionRoot = criteria.from(CustomerSessionEntity.class);
		Root<CustomerEntity> customerRoot = criteria.from(CustomerEntity.class);
		criteria.multiselect(orderRoot, sessionRoot, customerRoot);
		Predicate joins = builder.and(
				builder.equal(orderRoot.get(EntityDetails.Order.CHECKIN_ID),
						sessionRoot.get(EntityDetails.CustomerSession.ID)),
				builder.equal(sessionRoot.get(EntityDetails.CustomerSession.CUSTOMER_ID),
						customerRoot.get(EntityDetails.Customer.ID)));
		ParameterExpression<Integer> sessionStatusParam = builder.parameter(Integer.class);
		ParameterExpression<Integer> orderStatusParam = builder.parameter(Integer.class);
		ParameterExpression<Integer> userIdParam = builder.parameter(Integer.class);
		Predicate restrictions = builder.equal(sessionRoot.get(EntityDetails.CustomerSession.SESSION_STATUS_CODE),
				sessionStatusParam);
		if(orderStatus != OrderStatus.ALL) {
			restrictions = builder.and(restrictions,
					builder.equal(orderRoot.get(EntityDetails.Order.STATUS), orderStatusParam));
		}
		switch(roleType) {
		case RESTAURANT:
			restrictions = builder.and(restrictions,
					builder.equal(sessionRoot.get(EntityDetails.CustomerSession.RESTAURANT_ID), userIdParam));
			break;
		case CUSTOMER:
			restrictions = builder.and(restrictions,
					builder.equal(customerRoot.get(EntityDetails.Customer.ID), userIdParam));
			break;
		}
		criteria.where(builder.and(joins, restrictions));
		criteria.orderBy(builder.desc(orderRoot.get(EntityDetails.Order.UPDATED)));
		Query<?> q = getSession().createQuery(criteria);
		q.setParameter(userIdParam, userId);
		q.setParameter(sessionStatusParam, sessionStatus.getCode());
		if(orderStatus != OrderStatus.ALL) {
			q.setParameter(orderStatusParam, orderStatus.getCode());
		}
		return q.list();
	}

}
