package api.belue.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import api.belue.dao.IAbstractDao;

@Repository
@Transactional
public abstract class AbstractDao<T> implements IAbstractDao<T> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected String entityName;
	protected Class<T> entityClass;
	protected String FROM_ENTITY;
	protected String UPDATE_ENTITY;
	protected final String ORDER_BY = " ORDER BY %s ";
	
	public AbstractDao(String entityName, Class<T> entityClass) {
		this.entityName = entityClass.getSimpleName();
		this.entityClass = entityClass;
		FROM_ENTITY = "from " + entityName;
		UPDATE_ENTITY = "update " + entityName;
	}
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public T getTById(int id) {
		return (T) getSession().get(entityClass, id);
	}

	@Override
	public void saveT(T itemToSave) {
		getSession().save(itemToSave);
	}

	@Override
	public void updateT(T itemToUpdate) {
		getSession().update(itemToUpdate);
	}

	@Override
	public void deleteT(int id) {
		T itemToDelete = getTById(id);
		getSession().delete(itemToDelete);
	}

	@Override
	public T getT(String query) {
		return getSession().createQuery(query, entityClass).uniqueResult();
	}

	@Override
	public List<T> getAllT() {
		return getSession().createQuery(FROM_ENTITY, entityClass).list();
	}

	@Override
	public List<T> getAllT(String query) {
		return getSession().createQuery(query, entityClass).list();
	}

	@Override
	public boolean isExist(int id) {
		return (getTById(id) == null) ? false : true;
	}
	
	@Override
	public int executeUpdate(String hql) {
		Query<?> query = getSession().createQuery(hql);
		return query.executeUpdate();
	}
	
	@Override
	public List<T> getAllT(Query<T> query) {
		return query.list();
	}
	
	@Override
	public Query<T> getQueryObject(String hql) {
		return getSession().createQuery(hql, entityClass);
	}

}
