package api.belue.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IRestaurantTableDao;
import api.belue.entites.RestaurantTableEntity;

@Repository
public class RestaurantTableDao extends AbstractDao<RestaurantTableEntity> implements IRestaurantTableDao {

	public RestaurantTableDao() {
		super(EntityDetails.RestaurantTable.ENTITY_NAME, RestaurantTableEntity.class);
	}

	@Override
	public List<RestaurantTableEntity> getAllTablesByRestaurantId(int restaurantId) {
		String hql = "from " + EntityDetails.RestaurantTable.ENTITY_NAME + " where "
				+ EntityDetails.RestaurantTable.RESTAURANT_ID + "=" + restaurantId;
		return super.getAllT(hql);
	}

	@Override
	public RestaurantTableEntity getByRestaurantIdAndTableNumber(int restaurantId, int tableNumber) {
		String hql = "from " + EntityDetails.RestaurantTable.ENTITY_NAME + " where " +
					EntityDetails.RestaurantTable.RESTAURANT_ID + "=" + restaurantId + " and " +
					EntityDetails.RestaurantTable.TABLE_NUMBER + "=" + tableNumber;
		return super.getT(hql);
	}

}
