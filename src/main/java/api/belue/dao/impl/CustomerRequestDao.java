package api.belue.dao.impl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.ICustomerRequestDao;
import api.belue.entites.CustomerRequestEntity;

@Repository
public class CustomerRequestDao extends AbstractDao<CustomerRequestEntity> implements ICustomerRequestDao, EntityDetails.CustomerRequest {

	public CustomerRequestDao() {
		super(ENTITY_NAME, CustomerRequestEntity.class);
	}

	@Override
	public List<CustomerRequestEntity> getByCheckinIdsAndStatus(List<Integer> checkinIds, int statusCode) {
		String hql = String.format( FROM_ENTITY + " where %s = :statusCode AND %s in (:checkinIds)", STATUS, CHECKIN_ID);
		Query<CustomerRequestEntity> query = getQueryObject(hql);
		query.setParameter("statusCode", statusCode);
		query.setParameter("checkinIds", checkinIds);
		return getAllT(query);
	}

	@Override
	public void setStatusByIds(List<Integer> requestIds, int statusCode) {
		String hql = String.format( UPDATE_ENTITY + " set %s = :statusCode where %s in (:requestIds)", STATUS, ID);
		Query<CustomerRequestEntity> query = getSession().createQuery(hql, entityClass);
		query.setParameter("statusCode", statusCode);
		query.setParameter("requestIds", requestIds);
		query.executeUpdate();
	}

}
