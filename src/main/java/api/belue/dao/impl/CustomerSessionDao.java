package api.belue.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.ICustomerSessionDao;
import api.belue.entites.CustomerSessionEntity;

@Repository
public class CustomerSessionDao extends AbstractDao<CustomerSessionEntity> implements ICustomerSessionDao, EntityDetails.CustomerSession {

	public CustomerSessionDao() {
		super(EntityDetails.CustomerSession.ENTITY_NAME, CustomerSessionEntity.class);
	}

	@Override
	public List<CustomerSessionEntity> getByCustomerId(int customerId) {
		String hql = " from " + EntityDetails.CustomerSession.ENTITY_NAME + " where "
				+ EntityDetails.CustomerSession.CUSTOMER_ID + "=" + customerId;
		return super.getAllT(hql);
	}

	@Override
	public List<CustomerSessionEntity> getByCustomerIdAndSessionStatus(int customerId, int sessionStatusCode) {
		String hql = " from " + EntityDetails.CustomerSession.ENTITY_NAME + " where "
				+ EntityDetails.CustomerSession.CUSTOMER_ID + "=" + customerId + " and "
				+ EntityDetails.CustomerSession.SESSION_STATUS_CODE + "=" + sessionStatusCode;
		return super.getAllT(hql);
	}

	@Override
	public List<CustomerSessionEntity> getByRestaurantId(int restaurantId) {
		String hql = " from " + EntityDetails.CustomerSession.ENTITY_NAME + " where "
				+ EntityDetails.CustomerSession.RESTAURANT_ID + "=" + restaurantId;
		return super.getAllT(hql);
	}
	
	@Override
	public List<CustomerSessionEntity> getByRestaurantIdAndSessionStatus(int restaurantId, int sessionStatusCode) {
		String hql = " from " + EntityDetails.CustomerSession.ENTITY_NAME + " where "
				+ EntityDetails.CustomerSession.RESTAURANT_ID + "=" + restaurantId + " and "
				+ EntityDetails.CustomerSession.SESSION_STATUS_CODE + "=" + sessionStatusCode;
		return super.getAllT(hql);
	}

	@Override
	public List<CustomerSessionEntity> getByTableIdAndSessionStatus(int tableId, int sessionstatusCode) {
		String hql = String.format(FROM_ENTITY + " where %s=%d AND %s=%d ", TABLE_ID, tableId, SESSION_STATUS_CODE, sessionstatusCode);
		return getAllT(hql);
	}

}
