package api.belue.dao.impl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.ICustomerDao;
import api.belue.entites.CustomerEntity;

@Repository
public class CustomerDao extends AbstractDao<CustomerEntity> implements ICustomerDao {

	public CustomerDao() {
		super(EntityDetails.Customer.ENTITY_NAME, CustomerEntity.class);
	}

	@Override
	public CustomerEntity getByPhoneNumber(String phoneNumber) {
		String hql = " from " + EntityDetails.Customer.ENTITY_NAME + " where " +
					EntityDetails.Customer.PHONE_NUMBER + " ='" + phoneNumber + "'";
		return super.getT(hql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllNotificationTokens() {
		String hql = "Select " + EntityDetails.Customer.NOTIFICATION_TOKEN + " " + FROM_ENTITY;
		return getSession().createQuery(hql).list();
	}

	@Override
	public List<String> getNotificationTokensByCustomerIds(List<Integer> customerIds) {
		String hql = "Select " + EntityDetails.Customer.NOTIFICATION_TOKEN + " " + FROM_ENTITY + " where " + 
				EntityDetails.Customer.ID + " in(:customerIds)";
		Query<String> query = getSession().createQuery(hql, String.class);
		query.setParameter("customerIds", customerIds);
		return query.list();
	}

}
