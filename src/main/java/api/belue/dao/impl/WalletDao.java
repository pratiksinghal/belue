package api.belue.dao.impl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails.Wallet;
import api.belue.constants.TransactionType;
import api.belue.dao.IWalletDao;
import api.belue.entites.WalletTransactionEntity;

@Repository
public class WalletDao extends AbstractDao<WalletTransactionEntity> implements IWalletDao, Wallet {

	public WalletDao() {
		super(ENTITY_NAME, WalletTransactionEntity.class);
	}

	@Override
	public List<WalletTransactionEntity> getByCustomerIdAndType(int customerId, TransactionType type) {
		String hql = String.format(FROM_ENTITY + " where %s = :customerId and %s = :type", 
				CUSTOMER_ID, TRANSACTION_TYPE);
		Query<WalletTransactionEntity> query = getQueryObject(hql);
		query.setParameter("customerId", customerId);
		query.setParameter("type", type);
		return getAllT(query);
	}
	
	@Override
	public WalletTransactionEntity getByUniqueId(String uniqueId) {
		String hql = String.format(FROM_ENTITY + " where %s='%s'", UNIQUE_ID, uniqueId);
		return getT(hql);
	}

}
