package api.belue.dao.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IMenuDao;
import api.belue.entites.MenuEntity;

@Repository
public class MenuDao extends AbstractDao<MenuEntity> implements IMenuDao {

	public MenuDao() {
		super(EntityDetails.Menu.ENTITY_NAME, MenuEntity.class);
	}

	@Override
	public List<MenuEntity> getAllByRestaurantId(int restaurantId) {
		String hql = "from " + EntityDetails.Menu.ENTITY_NAME + " where " +
					EntityDetails.Menu.RESTAURANT_ID + " = " + restaurantId;
		return super.getAllT(hql);
	}
	
}
