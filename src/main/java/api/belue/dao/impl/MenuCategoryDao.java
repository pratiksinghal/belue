package api.belue.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import api.belue.constants.EntityDetails;
import api.belue.dao.IMenuCategoryDao;
import api.belue.entites.MenuCategoryEntity;

@Repository
public class MenuCategoryDao extends AbstractDao<MenuCategoryEntity> implements IMenuCategoryDao {

	public MenuCategoryDao() {
		super(EntityDetails.MenuCategory.ENTITY_NAME, MenuCategoryEntity.class);
	}

	@Override
	public List<MenuCategoryEntity> getAllByMenuId(int menuId) {
		String hql = " from " + EntityDetails.MenuCategory.ENTITY_NAME + " where " +
					EntityDetails.MenuCategory.MENU_ID + "=" + menuId;
		return super.getAllT(hql);
	}
	
}
