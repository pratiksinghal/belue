package api.belue.dao;

import java.util.List;

import api.belue.entites.Property;

public interface IPropertyDao {

	public void save(Property property);
	public void save(String propertyName, String propertyValue);
	public void update(Property property);
	public List<Property> getAll();
	public Property get(String propertyName);
	public Property get(int id);
	public void delete(int id);
	public void delete(String propertyName);
	public void addOrModify(String propertyName, String proprtyValue);
	
}
