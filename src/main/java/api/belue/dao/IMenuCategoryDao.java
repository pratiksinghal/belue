package api.belue.dao;

import java.util.List;

import api.belue.entites.MenuCategoryEntity;

public interface IMenuCategoryDao extends IAbstractDao<MenuCategoryEntity> {
	public List<MenuCategoryEntity> getAllByMenuId(int menuId);
}
