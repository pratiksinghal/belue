 package api.belue.dao;

import java.util.List;

import org.hibernate.query.Query;


public interface IAbstractDao<T> {
	public T getTById(int id);
	public void saveT(T itemToSave);
	public void updateT(T itemToUpdate);
	public void deleteT(int id);
	public T getT(String query);
	public List<T> getAllT();
	public List<T> getAllT(String query);
	public boolean isExist(int id);
	public int executeUpdate(String hql);
	List<T> getAllT(Query<T> query);
	Query<T> getQueryObject(String hql);
}
