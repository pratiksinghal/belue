package api.belue.dao;

import java.util.List;

import api.belue.constants.TransactionType;
import api.belue.entites.WalletTransactionEntity;

public interface IWalletDao extends IAbstractDao<WalletTransactionEntity> {

	public List<WalletTransactionEntity> getByCustomerIdAndType(int customerId, TransactionType type);

	WalletTransactionEntity getByUniqueId(String uniqueId);
}
