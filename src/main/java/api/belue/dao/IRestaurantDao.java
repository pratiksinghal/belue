package api.belue.dao;

import java.util.List;

import api.belue.entites.RestaurantEntity;

public interface IRestaurantDao extends IAbstractDao<RestaurantEntity> {
	public RestaurantEntity getByUsername(String username);

	public List<String> getAllNotificationTokens();

	public List<String> getNotificationTokensByRestaurantIds(List<Integer> customerIds);

}
