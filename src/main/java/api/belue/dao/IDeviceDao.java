package api.belue.dao;

import api.belue.entites.DeviceEntity;

public interface IDeviceDao extends IAbstractDao<DeviceEntity> {
	public DeviceEntity getBydeviceIdAndPlatform(String deviceId, String platform);
}
