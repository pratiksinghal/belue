package api.belue.dao;

import java.util.List;

import api.belue.entites.RestaurantNotificationsEntity;

public interface IRestaurantNotificationDao extends IAbstractDao<RestaurantNotificationsEntity> {

	List<RestaurantNotificationsEntity> getByRestaurantId(int restaurantId);

	List<RestaurantNotificationsEntity> getByRestaurantIdAndReadStatus(int restaurantId, boolean readStatus);
	
}
