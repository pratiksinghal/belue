package api.belue.dao;

import api.belue.entites.TokenEntity;

public interface ITokenDao extends IAbstractDao<TokenEntity> {
	public TokenEntity getByTokenValue(String token);
	public TokenEntity getByUserIdAndRoleType(int userId, int roleType);
}
