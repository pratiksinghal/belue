package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddMenuItemRequestDto;
import api.belue.request.dto.UpdateMenuItemRequestDto;

public class MenuItemValidator {
	public static void validateAddRequest(AddMenuItemRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(request.getUnitPrice() < 0) {
			throw new DtoValidationException(ResponseCode.INVALID_PRICE);
		}
		if(request.getUnitPrice() < 0) {
			throw new DtoValidationException(ResponseCode.INVALID_QUANTITY);
		}
		if(StringUtils.isEmpty(request.getDescription())) {
			throw new DtoValidationException(ResponseCode.DESCRIPTION_NOT_PRESENT);
		}
		if(request.getCategoryId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
	}

	public static void validateUpdateRequest(UpdateMenuItemRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_ITEM_ID);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(request.getUnitPrice() < 0) {
			throw new DtoValidationException(ResponseCode.INVALID_PRICE);
		}
		if(request.getUnitPrice() < 0) {
			throw new DtoValidationException(ResponseCode.INVALID_QUANTITY);
		}
		if(StringUtils.isEmpty(request.getDescription())) {
			throw new DtoValidationException(ResponseCode.DESCRIPTION_NOT_PRESENT);
		}
		if(request.getCategoryId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
	}
}
