package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddMenuCategoryRequestDto;
import api.belue.request.dto.UpdateMenuCategoryRequestDto;

public class MenuCategoryValidator {
	public static void validateAddRequest(AddMenuCategoryRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getDescription())) {
			throw new DtoValidationException(ResponseCode.DESCRIPTION_NOT_PRESENT);
		}
		if(request.getMenuId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_ID);
		}
	}

	public static void validateUpdateRequest(UpdateMenuCategoryRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_CATEGORY_ID);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getDescription())) {
			throw new DtoValidationException(ResponseCode.DESCRIPTION_NOT_PRESENT);
		}
		if(request.getMenuId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_ID);
		}
	}
}
