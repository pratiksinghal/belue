package api.belue.validators;

import org.springframework.web.multipart.MultipartFile;

import api.belue.constants.FileType;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;

public class UploadFileValidator {
	public static void validateFile(MultipartFile file, int fileTypeCode) throws DtoValidationException {
		if(file == null || file.isEmpty()) {
			throw new DtoValidationException(ResponseCode.FILE_NOT_PRESENT);
		}
		if(FileType.getByCode(fileTypeCode) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_FILE_TYPE);
		}
	}
}
