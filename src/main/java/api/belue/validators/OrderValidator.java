package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.OrderStatus;
import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddOrderRequestDto;
import api.belue.request.dto.OrderStatusRequestDto;
import api.belue.request.dto.UpdateOrderRequestDto;

public class OrderValidator {
	public static void validateAddRequest(AddOrderRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getData())) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getCheckinId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_CHECKIN_ID);
		}
	}

	public static void validateUpdateRequest(UpdateOrderRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_ORDER_ID);
		}
		if(StringUtils.isEmpty(request.getData())) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
	}

	public static void validateOrderSetStatusRequest(OrderStatusRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.ERROR);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_ORDER_ID);
		}
	}
	
	public static void validateOrderGetAllRequest(int orderStatus, int roleType, int id, boolean history) throws DtoValidationException {
		if(!history && OrderStatus.getByCode(orderStatus) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_ORDER_STATUS);
		}
		if(RoleType.getByCode(roleType) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_USER_TYPE);
		}
		if(id <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_ID);
		}
	}
}
