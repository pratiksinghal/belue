package api.belue.validators;

import api.belue.constants.CustomerRequestStatus;
import api.belue.constants.CustomerRequestType;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddCustomerRequestRequestDto;
import api.belue.request.dto.UpdateCustomerRequestStatusRequestDto;

public class CustomerRequestValidator {
	
	public static void validateAddRequest(AddCustomerRequestRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(CustomerRequestType.getByCode(request.getType()) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_REQUEST_TYPE);
		}
		if(CustomerRequestStatus.getByCode(request.getStatus()) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_REQUEST_STATUS);
		}
	}

	public static void validateUpdateRequest(UpdateCustomerRequestStatusRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(CustomerRequestStatus.getByCode(request.getStatus()) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_REQUEST_STATUS);
		}
	}
	
	public static void validateGetByRestaurantRequest(int requestStatusCode) throws DtoValidationException {
		if(CustomerRequestStatus.getByCode(requestStatusCode) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_REQUEST_STATUS);
		}
	}

}
