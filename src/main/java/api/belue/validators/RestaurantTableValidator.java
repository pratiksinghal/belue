package api.belue.validators;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddRestaurantTableRequestDto;
import api.belue.request.dto.UpdateRestaurantTableRequestDto;

public class RestaurantTableValidator {
	public static void validateAddRequest(AddRestaurantTableRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getRestaurantId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		if(request.getTableNumber() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_TABLE_NUMBER);
		}
		if(request.getSeatingCapacity() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_SEATING_CAPACITY);
		}
	}

	public static void validateUpdateRequest(UpdateRestaurantTableRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_TABLE_ID);
		}
		if(request.getRestaurantId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		if(request.getTableNumber() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_TABLE_NUMBER);
		}
		if(request.getSeatingCapacity() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_SEATING_CAPACITY);
		}
	}
}
