package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.constants.RoleType;
import api.belue.constants.SessionStatus;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddCustomerSessionRequestDto;

public class CustomerSessionValidator {
	
	public static void validateAddRequest(AddCustomerSessionRequestDto request)  throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getQrCodeId())) {
			throw new DtoValidationException(ResponseCode.QRCODE_NOT_PRESENT);
		}
	}

	public static void validateGetRequest(int sessionStatusCode, int user, int userId) throws DtoValidationException {
		if(SessionStatus.getByCode(sessionStatusCode) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_SESSION_STATUS_CODE);
		}
		if(RoleType.getByCode(user) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_USER_TYPE);
		}
		if(userId <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_ID);
		}
	}

}
