package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddMenuRequestDto;
import api.belue.request.dto.UpdateMenuRequestDto;

public class MenuValidator {
	public static void validateAddRequest(AddMenuRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(request.getRestaurantId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_ID);
		}
	}

	public static void validateUpdateRequest(UpdateMenuRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_MENU_ID);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(request.getRestaurantId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_ID);
		}
	}
}
