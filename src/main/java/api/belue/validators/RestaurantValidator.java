package api.belue.validators;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import api.belue.constants.PaymentType;
import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddRestaurantRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.ChangePasswordRequestDto;
import api.belue.request.dto.RestaurantLoginRequestDto;
import api.belue.request.dto.UpdateRestaurantRequestDto;
import api.belue.utils.DateUtil;

public class RestaurantValidator {

	public static void validateAddRequest(AddRestaurantRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getAddress())) {
			throw new DtoValidationException(ResponseCode.ADDRESS_NOT_PRESENT);
		}
		if(CollectionUtils.isEmpty(request.getMobileNumbers())) {
			throw new DtoValidationException(ResponseCode.MOBILE_NUMBER_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getUsername())) {
			throw new DtoValidationException(ResponseCode.USERNAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getPassword())) {
			throw new DtoValidationException(ResponseCode.PASSWORD_NOT_PRESENT);
		}
		if(PaymentType.getByCode(request.getPaymentTypeCode()) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_PAYMENT_TYPE);
		}
	}

	public static void validateUpdateRequest(UpdateRestaurantRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_RESTAURANT_ID);
		}
		if(StringUtils.isEmpty(request.getName())) {
			throw new DtoValidationException(ResponseCode.NAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getAddress())) {
			throw new DtoValidationException(ResponseCode.ADDRESS_NOT_PRESENT);
		}
		if(CollectionUtils.isEmpty(request.getMobileNumbers())) {
			throw new DtoValidationException(ResponseCode.MOBILE_NUMBER_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getUsername())) {
			throw new DtoValidationException(ResponseCode.USERNAME_NOT_PRESENT);
		}
		if(PaymentType.getByCode(request.getPaymentTypeCode()) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_PAYMENT_TYPE);
		}
	}

	public static void validateLoginRequest(RestaurantLoginRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getUsername())) {
			throw new DtoValidationException(ResponseCode.USERNAME_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getPassword())) {
			throw new DtoValidationException(ResponseCode.PASSWORD_NOT_PRESENT);
		}
	}

	public static void validateChangePasswordRequest(ChangePasswordRequestDto request) throws DtoValidationException {
		if(StringUtils.isEmpty(request.getOldPassword())) {
			throw new DtoValidationException(ResponseCode.PASSWORD_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getNewPassword())) {
			throw new DtoValidationException(ResponseCode.NEW_PASSWORD_NOT_PRESENT);
		}
	}

	public static void validateStatRequest(String startDate, String endDate) throws DtoValidationException {
		if(DateUtil.parseDate(startDate, DateUtil.DATE_PATTERN) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_START_DATE);
		}
		if(DateUtil.parseDate(endDate, DateUtil.DATE_PATTERN) == null) {
			throw new DtoValidationException(ResponseCode.INVALID_END_DATE);
		}
	}
	
	public static void validateNotificationTokenRequest(AddUpdateNotificationTokenRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getNotificationToken())) {
			throw new DtoValidationException(ResponseCode.NOTIFICATION_TOKEN_NOT_PRESENT);
		}
	}
}
