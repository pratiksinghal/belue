package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddCustomerRequestDto;
import api.belue.request.dto.AddUpdateNotificationTokenRequestDto;
import api.belue.request.dto.CustomerLoginRequestDto;
import api.belue.request.dto.UpdateCustomerRequestDto;

public class CustomerValidator {
	
	public static void validateAddRequest(AddCustomerRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getPhoneNumber())) {
			throw new DtoValidationException(ResponseCode.MOBILE_NUMBER_NOT_PRESENT);
		}
	}
	
	public static void validateUpdateRequest(UpdateCustomerRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(request.getId() <= 0) {
			throw new DtoValidationException(ResponseCode.INVALID_CUSTOMER_ID);
		}
		
	}
	public static void validateLoginRequest(CustomerLoginRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getPhoneNumber())) {
			throw new DtoValidationException(ResponseCode.MOBILE_NUMBER_NOT_PRESENT);
		}
	}
	
	public static void validateNotificationTokenRequest(AddUpdateNotificationTokenRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
		if(StringUtils.isEmpty(request.getNotificationToken())) {
			throw new DtoValidationException(ResponseCode.NOTIFICATION_TOKEN_NOT_PRESENT);
		}
		if(StringUtils.isEmpty(request.getPlatform())) {
			throw new DtoValidationException(ResponseCode.PLATFORM_NOT_PRESENT);
		}
	}
}
