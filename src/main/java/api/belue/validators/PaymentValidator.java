package api.belue.validators;

import org.springframework.util.StringUtils;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.DtoValidationException;
import api.belue.request.dto.AddPaymentRequestDto;

public abstract class PaymentValidator {

	public static void validateAddPayment(AddPaymentRequestDto request) throws DtoValidationException {
		if(request == null) {
			throw new DtoValidationException(ResponseCode.NO_DATA_PRESENT);
		}
//		if(StringUtils.isEmpty(request.getPaymentGatewayName())) {
//			throw new DtoValidationException(ResponseCode.PAYMENT_GATEWAY_NAME_NOT_PRESENT);
//		}
		if(StringUtils.isEmpty(request.getTransactionMetaData())) {
			throw new DtoValidationException(ResponseCode.TRANSACTION_METADATA_NOT_PRESENT);
		}
	}
}
