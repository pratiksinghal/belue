package api.belue.entites;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import api.belue.constants.RoleType;

@Entity
@Table()
public class TokenEntity {
	@Id
	private String tokenValue;
	private int userId;
	private int roleTypeCode;
	
	public String getTokenValue() {
		return tokenValue;
	}
	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRoleTypeCode() {
		return roleTypeCode;
	}
	public void setRoleTypeCode(int roleTypeCode) {
		this.roleTypeCode = roleTypeCode;
	}
	public RoleType getRoleType() {
		return RoleType.getByCode(roleTypeCode);
	}
}
