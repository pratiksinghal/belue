package api.belue.entites;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import api.belue.constants.EntityDetails;
import api.belue.constants.TransactionStatus;
import api.belue.constants.TransactionType;

@Entity
@Table(name = EntityDetails.Wallet.TABLE_NAME)
public class WalletTransactionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int customerId;
	private int customerSessionId;
	private int orderId;
	private double amount;
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = TransactionType.COL_DEF, nullable = false)
	private TransactionType transactionType;
	private int referenceId;
	private int orderOrSessionType;
	private String paymentGatewayName;
	private String paymentGatewayTransactionId;
	private String paymentGatewayMetaData;
	private String metaData;
	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = TransactionStatus.COL_DEF, nullable = false)
	private TransactionStatus status;
	private String uniqueId;
	@CreationTimestamp
	@Column(columnDefinition = "timestamp DEFAULT CURRENT_TIMESTAMP")
	private Date created;
	@UpdateTimestamp
	@Column(columnDefinition = "timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private Date updated;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getCustomerSessionId() {
		return customerSessionId;
	}

	public void setCustomerSessionId(int customerSessionId) {
		this.customerSessionId = customerSessionId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public int getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

	public int getOrderOrSessionType() {
		return orderOrSessionType;
	}

	public void setOrderOrSessionType(int orderOrSessionType) {
		this.orderOrSessionType = orderOrSessionType;
	}

	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	public String getPaymentGatewayTransactionId() {
		return paymentGatewayTransactionId;
	}

	public void setPaymentGatewayTransactionId(String paymentGatewayTransactionId) {
		this.paymentGatewayTransactionId = paymentGatewayTransactionId;
	}

	public String getPaymentGatewayMetaData() {
		return paymentGatewayMetaData;
	}

	public void setPaymentGatewayMetaData(String paymentGatewayMetaData) {
		this.paymentGatewayMetaData = paymentGatewayMetaData;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
}
