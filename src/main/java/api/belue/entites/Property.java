package api.belue.entites;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import api.belue.constants.EntityDetails;

@Entity
@Table(name=EntityDetails.Property.TABLE_NAME)
public class Property {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer Id;
	@Column(name=EntityDetails.Property.PROPERTY_NAME, nullable=false)
	private String propertyName;
	@Column(name=EntityDetails.Property.PROPERTY_VALUE, nullable=false, columnDefinition="varchar(5000) not null")
	private String propertyValue;
	@Column(name=EntityDetails.Property.UPDATED, columnDefinition="timestamp default now() ON UPDATE now()")
	private Date updated;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
