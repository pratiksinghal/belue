package api.belue.entites;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import api.belue.constants.EntityDetails;

@Entity
@Table(name=EntityDetails.CustomerSession.TABLE_NAME)
public class CustomerSessionEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int customerId;
	private int tableId;
	private int restaurantId;
	private int sessionStatusCode;
	private String taxes;
	private String discounts;
	private String paidInfo;
	
	@CreationTimestamp
	private Date checkInTimeStamp;
	
	private Date checkOutTimeStamp;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public int getSessionStatusCode() {
		return sessionStatusCode;
	}
	public void setSessionStatusCode(int sessionStatusCode) {
		this.sessionStatusCode = sessionStatusCode;
	}
	public String getTaxes() {
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}
	public String getDiscounts() {
		return discounts;
	}
	public void setDiscounts(String discounts) {
		this.discounts = discounts;
	}
	public String getPaidInfo() {
		return paidInfo;
	}
	public void setPaidInfo(String paidInfo) {
		this.paidInfo = paidInfo;
	}
	public Date getCheckInTimeStamp() {
		return checkInTimeStamp;
	}
	public void setCheckInTimeStamp(Date checkInTimeStamp) {
		this.checkInTimeStamp = checkInTimeStamp;
	}
	public Date getCheckOutTimeStamp() {
		return checkOutTimeStamp;
	}
	public void setCheckOutTimeStamp(Date checkOutTimeStamp) {
		this.checkOutTimeStamp = checkOutTimeStamp;
	}
}
