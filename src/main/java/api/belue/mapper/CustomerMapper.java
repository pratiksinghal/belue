package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.CustomerEntity;
import api.belue.request.dto.AddCustomerRequestDto;
import api.belue.request.dto.UpdateCustomerRequestDto;
import api.belue.response.dto.CustomerResponseDto;

public class CustomerMapper {
	public static CustomerResponseDto mapEntityToResponse(CustomerEntity request) {
		if(request == null) {
			return null;
		}
		CustomerResponseDto response = new CustomerResponseDto();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setPhoneNumber(request.getPhoneNumber());
		response.setEmail(request.getEmail());
		response.setVerified(request.isVerified());
		response.setNotificationToken(request.getNotificationToken());
		response.setPlatform(request.getPlatform());
		return response;
	}
	
	public static CustomerEntity mapAddRequestToEntity(AddCustomerRequestDto request) {
		if(request == null) {
			return null;
		}
		CustomerEntity response = new CustomerEntity();
		response.setName(request.getName());
		response.setPhoneNumber(request.getPhoneNumber());
		response.setEmail(request.getEmail());
		response.setVerified(request.isVerified());
		return response;
	}

	public static CustomerEntity mapUpdateRequestToEntity(UpdateCustomerRequestDto request) {
		if(request == null) {
			return null;
		}
		CustomerEntity response = new CustomerEntity();
		response.setName(request.getName());
		response.setId(request.getId());
		response.setVerified(request.isVerified());
		response.setEmail(request.getEmail());
		return response;
	}

	public static List<CustomerResponseDto> mapEntityListToResponseList(List<CustomerEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<CustomerResponseDto> responseList = new ArrayList<>(requestList.size());
		requestList.forEach(entity -> responseList.add(mapEntityToResponse(entity)));
		return responseList;
	}
	
}
