package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.MenuCategoryEntity;
import api.belue.entites.MenuItemEntity;
import api.belue.pojo.MenuItemInOrderPojo;
import api.belue.request.dto.AddMenuItemRequestDto;
import api.belue.request.dto.UpdateMenuItemRequestDto;
import api.belue.response.dto.MenuItemResponseDto;
import api.belue.utils.NumberUtils;

public class MenuItemMapper {
	public static MenuItemResponseDto mapEntityToResponse(MenuItemEntity request) {
		if(request == null) {
			return null;
		}
		MenuItemResponseDto response = new MenuItemResponseDto();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setUnitPrice(request.getUnitPrice());
		response.setUnitQty(request.getUnitQty());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setInStock(request.isInStock());
		response.setQtyInStock(request.getQtyInStock());
		response.setCategoryId(request.getCategoryId());
		response.setMenuId(request.getMenuId());
		response.setQuantity(request.getQuantity());
		response.setCgst(request.getCgst());
		response.setSgst(request.getSgst());
		response.setIgst(request.getIgst());
		response.setGst(request.getGst());
		response.setTotalTax(request.getTotalTax());
		response.setTaxAmount(request.getTaxAmount());
		response.setTotalAmount(request.getTotalAmount());
		return response;
	}

	public static MenuItemEntity mapAddRequestToEntity(AddMenuItemRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuItemEntity response = new MenuItemEntity();
		response.setName(request.getName());
		response.setUnitPrice(request.getUnitPrice());
		response.setUnitQty(request.getUnitQty());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setInStock(request.isInStock());
		response.setQtyInStock(request.getQtyInStock());
		response.setCategoryId(request.getCategoryId());
		response.setQuantity(request.getQuantity());
		return response;
	}

	public static MenuItemEntity mapUpdateRequestToEntity(UpdateMenuItemRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuItemEntity response = new MenuItemEntity();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setUnitPrice(request.getUnitPrice());
		response.setUnitQty(request.getUnitQty());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setInStock(request.isInStock());
		response.setQtyInStock(request.getQtyInStock());
		response.setCategoryId(request.getCategoryId());
		response.setQuantity(request.getQuantity());
		return response;
	}
	
	public static void setCategoryDetails(MenuItemEntity menuEntity, MenuCategoryEntity menuCategoryEntity) {
		if(menuEntity == null || menuCategoryEntity == null) {
			return;
		}
		menuEntity.setMenuId(menuCategoryEntity.getMenuId());
		menuEntity.setCgst(menuCategoryEntity.getCgst());
		menuEntity.setSgst(menuCategoryEntity.getSgst());
		menuEntity.setIgst(menuCategoryEntity.getIgst());
		menuEntity.setGst(menuCategoryEntity.getGst());
		menuEntity.setTotalTax(menuCategoryEntity.getTotalTax());
		menuEntity.setTaxAmount(NumberUtils.getPercentageValue(menuEntity.getUnitPrice(), menuEntity.getTotalTax()));
		menuEntity.setTotalAmount(menuEntity.getTaxAmount() + menuEntity.getUnitPrice());
	}
	
	public static void setCategoryDetails(MenuItemEntity newEntity, MenuItemEntity oldEntity) {
		if(newEntity == null || oldEntity == null) {
			return;
		}
		newEntity.setMenuId(oldEntity.getMenuId());
		newEntity.setCgst(oldEntity.getCgst());
		newEntity.setSgst(oldEntity.getSgst());
		newEntity.setIgst(oldEntity.getIgst());
		newEntity.setGst(oldEntity.getGst());
		newEntity.setTotalTax(oldEntity.getTotalTax());
		newEntity.setTaxAmount(NumberUtils.getPercentageValue(newEntity.getUnitPrice(), newEntity.getTotalTax()));
		newEntity.setTotalAmount(newEntity.getTaxAmount() + newEntity.getUnitPrice());
	}

	public static List<MenuItemResponseDto> mapEntityListToResponseList(List<MenuItemEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<MenuItemResponseDto> response = new ArrayList<>(requestList.size());
		for(MenuItemEntity menuItemEntity : requestList) {
			response.add(mapEntityToResponse(menuItemEntity));
		}
		return response;
	}

	public static void setEntityDetailsToOrderItem(MenuItemInOrderPojo orderItem, MenuItemEntity menuItemEntity) {
		if(orderItem == null || menuItemEntity == null) {
			return;
		}
		orderItem.setUnitPrice(menuItemEntity.getUnitPrice());
		orderItem.setCgst(menuItemEntity.getCgst());
		orderItem.setSgst(menuItemEntity.getSgst());
		orderItem.setIgst(menuItemEntity.getIgst());
		orderItem.setGst(menuItemEntity.getGst());
		orderItem.setTotalTax(menuItemEntity.getTotalTax());
	}
}
