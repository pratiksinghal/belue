package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.constants.CustomerRequestStatus;
import api.belue.constants.CustomerRequestType;
import api.belue.entites.CustomerRequestEntity;
import api.belue.request.dto.AddCustomerRequestRequestDto;
import api.belue.request.dto.UpdateCustomerRequestStatusRequestDto;
import api.belue.response.dto.CustomerRequestReponseDto;
import api.belue.utils.DateUtil;

public class CustomerRequestMapper {
	
	public static CustomerRequestReponseDto mapEntityToResponse(CustomerRequestEntity request) {
		if(request == null) {
			return null;
		}
		CustomerRequestReponseDto response = new CustomerRequestReponseDto();
		response.setId(request.getId());
		response.setCheckinId(request.getCheckinId());
		response.setStatus(request.getStatus());
		response.setType(request.getType());
		response.setRequestMessage(CustomerRequestType.getByCode(request.getType()).getMessage());
		response.setCreated(DateUtil.formateDate(request.getCreated(), DateUtil.TIMESTAMP_PATTERN));
		response.setUpdated(DateUtil.formateDate(request.getUpdated(), DateUtil.TIMESTAMP_PATTERN));
		return response;
	}
	
	public static CustomerRequestEntity mapAddRequestToEntity(AddCustomerRequestRequestDto request) {
		if(request == null) {
			return null;
		}
		CustomerRequestEntity response = new CustomerRequestEntity();
		response.setCheckinId(request.getCheckinId());
		response.setStatus(CustomerRequestStatus.INITIATED.getCode());
		response.setType(request.getType());
		return response;
	}

	public static CustomerRequestEntity mapUpdateRequestToEntity(UpdateCustomerRequestStatusRequestDto request) {
		if(request == null) {
			return null;
		}
		CustomerRequestEntity response = new CustomerRequestEntity();
		response.setId(request.getId());
		response.setStatus(request.getStatus());
		return response;
	}

	public static List<CustomerRequestReponseDto> mapEntityListToResponseList(List<CustomerRequestEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<CustomerRequestReponseDto> responseList = new ArrayList<>(requestList.size());
		requestList.forEach(entity -> responseList.add(mapEntityToResponse(entity)));
		return responseList;
	}
}
