package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.RestaurantTableEntity;
import api.belue.exceptions.BaseException;
import api.belue.request.dto.AddRestaurantTableRequestDto;
import api.belue.request.dto.UpdateRestaurantTableRequestDto;
import api.belue.response.dto.RestaurantTableResponseDto;

public class RestaurantTableMapper {
	public static RestaurantTableResponseDto mapEntityToResponse(RestaurantTableEntity request) {
		if(request == null) {
			return null;
		}
		RestaurantTableResponseDto response = new RestaurantTableResponseDto();
		response.setId(request.getId());
		response.setRestaurantId(request.getRestaurantId());
		response.setTableNumber(request.getTableNumber());
		response.setSeatingCapacity(request.getSeatingCapacity());
		response.setQrCodeUrl(request.getQrCodeUrl());
		response.setOccoupied(request.isOccupied());
		return response;
	}

	public static RestaurantTableEntity mapAddRequestToEntity(AddRestaurantTableRequestDto request)
			throws BaseException {
		if(request == null) {
			return null;
		}
		RestaurantTableEntity response = new RestaurantTableEntity();
		response.setRestaurantId(request.getRestaurantId());
		response.setTableNumber(request.getTableNumber());
		response.setSeatingCapacity(request.getSeatingCapacity());
		return response;
	}

	public static RestaurantTableEntity mapUpdateRequestToEntity(UpdateRestaurantTableRequestDto request)
			throws BaseException {
		if(request == null) {
			return null;
		}
		RestaurantTableEntity response = new RestaurantTableEntity();
		response.setId(request.getId());
		response.setRestaurantId(request.getRestaurantId());
		response.setTableNumber(request.getTableNumber());
		response.setSeatingCapacity(request.getSeatingCapacity());
		return response;
	}

	public static List<RestaurantTableResponseDto>
			mapEntityListToResponseList(List<RestaurantTableEntity> requestList) {
		if(requestList == null) {
			return null;
		}

		List<RestaurantTableResponseDto> responseList = new ArrayList<>(requestList.size());
		for(RestaurantTableEntity restaurantTableEntity : requestList) {
			responseList.add(mapEntityToResponse(restaurantTableEntity));
		}
		return responseList;
	}
}
