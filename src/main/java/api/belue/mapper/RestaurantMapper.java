package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.RestaurantEntity;
import api.belue.exceptions.BaseException;
import api.belue.pojo.Discount;
import api.belue.request.dto.AddRestaurantRequestDto;
import api.belue.request.dto.UpdateRestaurantRequestDto;
import api.belue.response.dto.RestuarantResponseDto;
import api.belue.utils.JsonUtils;

public class RestaurantMapper {
	public static RestuarantResponseDto mapEntityToResponse(RestaurantEntity request) {
		if(request == null) {
			return null;
		}
		RestuarantResponseDto response = new RestuarantResponseDto();
		response.setId(request.getId());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setAddress(request.getAddress());
		response.setMobileNumbers(JsonUtils.convertJsonToList(request.getMobileNumbers(), String.class));
		response.setUsername(request.getUsername());
		response.setImages(JsonUtils.convertJsonToList(request.getImages(), String.class));
		response.setPaymentTypeCode(request.getPaymentTypeCode());
		response.setCuisine(request.getCuisine());
		response.setMetaInfo(JsonUtils.fromJson(request.getMetaInfo(), Object.class));
		response.setDiscounts(JsonUtils.convertJsonToList(request.getDiscounts(), Discount.class));
		return response;
	}

	public static RestaurantEntity mapAddRequestToEntity(AddRestaurantRequestDto request) throws BaseException {
		if(request == null) {
			return null;
		}
		RestaurantEntity response = new RestaurantEntity();
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setAddress(request.getAddress());
		response.setMobileNumbers(JsonUtils.convertListToJSON(request.getMobileNumbers()));
		response.setUsername(request.getUsername());
		response.setPassword(request.getPassword());
		response.setImages(JsonUtils.convertListToJSON(request.getImages()));
		response.setPaymentTypeCode(request.getPaymentTypeCode());
		response.setCuisine(request.getCuisine());
		response.setMetaInfo(JsonUtils.getJson(request.getMetaInfo()));
		response.setDiscounts(JsonUtils.getJson(request.getDiscounts()));
		return response;
	}

	public static RestaurantEntity mapUpdateRequestToEntity(UpdateRestaurantRequestDto request) throws BaseException {
		if(request == null) {
			return null;
		}
		RestaurantEntity response = new RestaurantEntity();
		response.setId(request.getId());
		response.setLogo(request.getLogo());
		response.setName(request.getName());
		response.setAddress(request.getAddress());
		response.setMobileNumbers(JsonUtils.convertListToJSON(request.getMobileNumbers()));
		response.setUsername(request.getUsername());
		response.setImages(JsonUtils.convertListToJSON(request.getImages()));
		response.setPaymentTypeCode(request.getPaymentTypeCode());
		response.setCuisine(request.getCuisine());
		response.setMetaInfo(JsonUtils.getJson(request.getMetaInfo()));
		response.setDiscounts(JsonUtils.getJson(request.getDiscounts()));
		return response;
	}

	public static List<RestuarantResponseDto> mapEntityListToResponseList(List<RestaurantEntity> requestList) {
		if(requestList == null) {
			return null;
		}

		List<RestuarantResponseDto> responseList = new ArrayList<>(requestList.size());
		for(RestaurantEntity restaurantEntity : requestList) {
			responseList.add(mapEntityToResponse(restaurantEntity));
		}
		return responseList;
	}
}
