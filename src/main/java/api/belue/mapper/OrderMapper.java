package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.OrderEntity;
import api.belue.request.dto.AddOrderRequestDto;
import api.belue.request.dto.UpdateOrderRequestDto;
import api.belue.response.dto.OrderResponseDto;
import api.belue.utils.DateUtil;

public class OrderMapper {
	public static OrderResponseDto mapEntityToResponse(OrderEntity request) {
		if(request == null) {
			return null;
		}
		OrderResponseDto response = new OrderResponseDto();
		response.setId(request.getId());
		response.setData(request.getData());
		response.setCheckinId(request.getCheckinId());
		response.setCreated(DateUtil.formateDate(request.getCreated(), DateUtil.TIMESTAMP_PATTERN));
		response.setUpdated(DateUtil.formateDate(request.getUpdated(), DateUtil.TIMESTAMP_PATTERN));
		response.setOrderStatus(request.getStatusCode());
		response.setAmount(request.getAmount());
		response.setRemainingAmount(request.getRemainingAmount());
		response.setPaymentStatusCode(request.getPaymentStatusCode());
		response.setOrderName(request.getOrderName());
		response.setTaxAmount(request.getTaxAmount());
		response.setDiscountAmount(request.getDiscountAmount());
		response.setAmountWithoutDiscount(response.getAmount() + response.getDiscountAmount());
		return response;
	}

	public static OrderEntity mapAddRequestToEntity(AddOrderRequestDto request) {
		if(request == null) {
			return null;
		}
		OrderEntity response = new OrderEntity();
		response.setData(request.getData());
		response.setCheckinId(request.getCheckinId());
		response.setPaymentStatusCode(request.getPaymentStatusCode());
		return response;
	}

	public static OrderEntity mapUpdateRequestToEntity(UpdateOrderRequestDto request) {
		if(request == null) {
			return null;
		}
		OrderEntity response = new OrderEntity();
		response.setId(request.getId());
		response.setData(request.getData());
		response.setPaymentStatusCode(request.getPaymentStatusCode());
		return response;
	}

	public static List<OrderResponseDto> mapEntityListToResponseList(List<OrderEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<OrderResponseDto> responseList = new ArrayList<>(requestList.size());
		for(OrderEntity orderEntity : requestList) {
			responseList.add(mapEntityToResponse(orderEntity));
		}
		return responseList;
	}
}
