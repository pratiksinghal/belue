package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.CustomerSessionEntity;
import api.belue.request.dto.AddCustomerSessionRequestDto;
import api.belue.request.dto.UpdateCustomerSessionRequestDto;
import api.belue.response.dto.CustomerSessionResponseDto;
import api.belue.utils.DateUtil;
import api.belue.utils.JsonUtils;

public class CustomerSessionMapper {
	public static CustomerSessionResponseDto mapEntityToResponse(CustomerSessionEntity request) {
		if(request == null) {
			return null;
		}
		CustomerSessionResponseDto response = new CustomerSessionResponseDto();
		response.setCheckinId(request.getId());
		response.setCheckinTime(DateUtil.formateDate(request.getCheckInTimeStamp(), DateUtil.TIMESTAMP_PATTERN));
		response.setCheckoutTime(DateUtil.formateDate(request.getCheckOutTimeStamp(), DateUtil.TIMESTAMP_PATTERN));
		response.setUserId(request.getCustomerId());
		response.setRestaurantId(request.getRestaurantId());
		response.setSessionStatusCode(request.getSessionStatusCode());
		response.setTableId(request.getTableId());
		response.setTaxes(JsonUtils.fromJson(request.getTaxes(), Object.class));
		response.setDiscounts(JsonUtils.fromJson(request.getDiscounts(), Object.class));
		response.setPaidInfo(request.getPaidInfo());
		return response;
	}

	public static CustomerSessionEntity mapAddRequestToEntity(AddCustomerSessionRequestDto request)
			throws NumberFormatException {
		if(request == null) {
			return null;
		}
		CustomerSessionEntity response = new CustomerSessionEntity();
		response.setCustomerId(request.getCustomerId());
		response.setTableId(Integer.parseInt(request.getQrCodeId()));
		return response;
	}
	
	public static List<CustomerSessionResponseDto> mapEntityListToResponseList(List<CustomerSessionEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<CustomerSessionResponseDto> responseList = new ArrayList<>(requestList.size());
		for(CustomerSessionEntity customerSessionEntity : requestList) {
			responseList.add(mapEntityToResponse(customerSessionEntity));
		}
		return responseList;
	}
	
	public static CustomerSessionEntity mapUpdateRequestToEntity(UpdateCustomerSessionRequestDto request) {
		if(request == null) {
			return null;
		}
		CustomerSessionEntity response = new CustomerSessionEntity();
		response.setId(request.getId());
		response.setTaxes(JsonUtils.getJson(request.getTaxes()));
		response.setDiscounts(JsonUtils.getJson(request.getDiscounts()));
		return response;
	}
}
