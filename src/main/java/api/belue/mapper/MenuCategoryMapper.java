package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.MenuCategoryEntity;
import api.belue.request.dto.AddMenuCategoryRequestDto;
import api.belue.request.dto.UpdateMenuCategoryRequestDto;
import api.belue.response.dto.MenuCategoryResponseDto;

public class MenuCategoryMapper {
	public static MenuCategoryResponseDto mapEntityToResponse(MenuCategoryEntity request) {
		if(request == null) {
			return null;
		}
		MenuCategoryResponseDto response = new MenuCategoryResponseDto();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setMenuId(request.getMenuId());
		response.setCgst(request.getCgst());
		response.setSgst(request.getSgst());
		response.setIgst(request.getIgst());
		response.setGst(request.getGst());
		response.setTotalTax(request.getTotalTax());
		return response;
	}

	public static MenuCategoryEntity mapAddRequestToEntity(AddMenuCategoryRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuCategoryEntity response = new MenuCategoryEntity();
		response.setName(request.getName());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setMenuId(request.getMenuId());
		response.setCgst(request.getCgst());
		response.setSgst(request.getSgst());
		response.setIgst(request.getIgst());
		response.setGst(request.getGst());
		response.setTotalTax(request.getTotalTax());
		return response;
	}

	public static MenuCategoryEntity mapUpdateRequestToEntity(UpdateMenuCategoryRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuCategoryEntity response = new MenuCategoryEntity();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setDescription(request.getDescription());
		response.setIcon(request.getIcon());
		response.setMenuId(request.getMenuId());
		response.setCgst(request.getCgst());
		response.setSgst(request.getSgst());
		response.setIgst(request.getIgst());
		response.setGst(request.getGst());
		response.setTotalTax(request.getTotalTax());
		return response;
	}

	public static List<MenuCategoryResponseDto> mapEntityListToResponseList(List<MenuCategoryEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<MenuCategoryResponseDto> response = new ArrayList<>(requestList.size());
		for(MenuCategoryEntity menuCategoryEntity : requestList) {
			response.add(mapEntityToResponse(menuCategoryEntity));
		}
		return response;
	}
}
