package api.belue.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import api.belue.constants.FCMNotifTemplate;
import api.belue.entites.RestaurantNotificationsEntity;
import api.belue.response.dto.RestaurantNotificationResposeDto;
import api.belue.utils.DateUtil;
import api.belue.utils.JsonUtils;

public abstract class RestaurantNotificationsMapper {
	public static RestaurantNotificationResposeDto mapEntityToResponse(RestaurantNotificationsEntity request) {
		if(request == null) {
			return null;
		}
		RestaurantNotificationResposeDto response = new RestaurantNotificationResposeDto();
		response.setId(request.getId());
		FCMNotifTemplate notifTemplate = JsonUtils.fromJson(request.getNotifData(), FCMNotifTemplate.class);
		notifTemplate.setNotifId(request.getId());
		response.setNotification(notifTemplate);
		response.setRestaurantId(request.getRestaurantId());
		response.setRead(request.isIsRead());
		response.setCreated(DateUtil.formateDate(request.getCreated(), DateUtil.TIMESTAMP_PATTERN));
		response.setUpdated(DateUtil.formateDate(request.getUpdated(), DateUtil.TIMESTAMP_PATTERN));
		return response;
	}

	public static List<RestaurantNotificationResposeDto>
			mapEntityListToResponseList(List<RestaurantNotificationsEntity> requestList) {
		if(CollectionUtils.isEmpty(requestList)) {
			return null;
		}
		return requestList.parallelStream()
				.map(request -> mapEntityToResponse(request))
				.filter(response -> response != null)
				.collect(Collectors.toList());
	}

	public static RestaurantNotificationsEntity mapNotifTemplateToEntity(FCMNotifTemplate template, int restaurantId) {
		if(template == null) {
			return null;
		}
		RestaurantNotificationsEntity response = new RestaurantNotificationsEntity();
		response.setNotifData(JsonUtils.getJson(template));
		response.setRestaurantId(restaurantId);
		return response;
	}
}
