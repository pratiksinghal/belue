package api.belue.mapper;

import java.util.ArrayList;
import java.util.List;

import api.belue.entites.MenuEntity;
import api.belue.request.dto.AddMenuRequestDto;
import api.belue.request.dto.UpdateMenuRequestDto;
import api.belue.response.dto.MenuResponseDto;

public class MenuMapper {
	public static MenuResponseDto mapEntityToResponse(MenuEntity request) {
		if(request == null) {
			return null;
		}
		MenuResponseDto response = new MenuResponseDto();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setRestaurantId(request.getRestaurantId());
		return response;
	}
	public static MenuEntity mapAddRequestToEntity(AddMenuRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuEntity response = new MenuEntity();
		response.setName(request.getName());
		response.setRestaurantId(request.getRestaurantId());
		return response;
	}

	public static MenuEntity mapUpdateRequestToEntity(UpdateMenuRequestDto request) {
		if(request == null) {
			return null;
		}
		MenuEntity response = new MenuEntity();
		response.setId(request.getId());
		response.setName(request.getName());
		response.setRestaurantId(request.getRestaurantId());
		return response;
	}
	public static List<MenuResponseDto> mapEntityListToResponseList(List<MenuEntity> requestList) {
		if(requestList == null) {
			return null;
		}
		List<MenuResponseDto> response = new ArrayList<>(requestList.size());
		for(MenuEntity menuEntity : requestList) {
			response.add(mapEntityToResponse(menuEntity));
		}
		return response;
	}
}
