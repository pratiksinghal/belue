package api.belue.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import api.belue.entites.WalletTransactionEntity;
import api.belue.request.dto.AddPaymentRequestDto;
import api.belue.response.dto.TransactionResponseDto;
import api.belue.utils.DateUtil;
import api.belue.utils.JsonUtils;

public abstract class WalletTransactionMapper {
	
	public static TransactionResponseDto mapEntityToResponse(WalletTransactionEntity request) {
		if(request == null) {
			return null;
		}
		TransactionResponseDto response = new TransactionResponseDto();
		response.setId(request.getId());
		response.setCustomerId(request.getCustomerId());
		response.setCustomerSessionId(request.getCustomerSessionId());
		response.setOrderId(request.getOrderId());
		response.setIdType(request.getOrderOrSessionType());
		response.setPaymentGatewayName(request.getPaymentGatewayName());
		response.setAmount(request.getAmount());
		response.setTransactionTypeCode(request.getTransactionType().toString());
		response.setMetaData(JsonUtils.fromJson(request.getMetaData(), Object.class));
		response.setCreated(DateUtil.formateDate(request.getCreated(), DateUtil.TIMESTAMP_PATTERN));
		response.setUpdated(DateUtil.formateDate(request.getUpdated(), DateUtil.TIMESTAMP_PATTERN));
		return response;
	}
	
	public static WalletTransactionEntity mapAddRequestToEntity(AddPaymentRequestDto request) {
		if(request == null) {
			return null;
		}
		WalletTransactionEntity response = new WalletTransactionEntity();
		response.setCustomerId(request.getCustomerId());
		response.setCustomerSessionId(request.getCustomerSessionId());
		response.setOrderId(request.getOrderId());
		response.setOrderOrSessionType(request.getIdType());
		response.setPaymentGatewayName(request.getPaymentGatewayName());
		response.setPaymentGatewayMetaData(JsonUtils.getJson(request.getTransactionMetaData()));
		response.setMetaData(JsonUtils.getJson(request.getMetaData()));
		return response;
	}
	
	public static WalletTransactionEntity cloneEntity(WalletTransactionEntity request) {
		if(request == null) {
			return null;
		}
		WalletTransactionEntity response = new WalletTransactionEntity();
		response.setId(request.getId());
		response.setCustomerId(request.getCustomerId());
		response.setCustomerSessionId(request.getCustomerSessionId());
		response.setOrderId(request.getOrderId());
		response.setAmount(request.getAmount());
		response.setTransactionType(request.getTransactionType());
		response.setReferenceId(request.getId());
		response.setOrderOrSessionType(request.getOrderOrSessionType());
		response.setPaymentGatewayName(request.getPaymentGatewayName());
		response.setPaymentGatewayTransactionId(request.getPaymentGatewayTransactionId());
		response.setPaymentGatewayMetaData(request.getPaymentGatewayMetaData());
		response.setMetaData(request.getMetaData());
		response.setStatus(request.getStatus());
		response.setUniqueId(request.getUniqueId());
		return response;
	}
	
	public static List<TransactionResponseDto> mapEntityListToResponseList(List<WalletTransactionEntity>  requestList) {
		if(CollectionUtils.isEmpty(requestList)) {
			return null;
		}
		return requestList.parallelStream()
		.map(entity -> mapEntityToResponse(entity))
		.collect(Collectors.toList());
	}
}
