package api.belue.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import api.belue.constants.ResponseCode;
import api.belue.exceptions.BaseException;

@JsonInclude(value=Include.NON_NULL)
public class BaseResponse<T> {
	public boolean error;
	public String statusCode;
	public String message;
	public T data;
	public String serverMsg;
	
	public BaseResponse(boolean error, String statusCode, String message, T data) {
		super();
		this.error = error;
		this.statusCode = statusCode;
		this.message = message;
		this.data = data;
	}

	public BaseResponse(BaseException baseException) {
		this.error = true;
		this.data = null;
		this.statusCode = baseException.getResponseCode().getCode();
		this.message = baseException.getResponseCode().getMessage();
	}

	public BaseResponse(boolean isError, T data, ResponseCode responseCode) {
		this.error = isError;
		this.data = data;
		this.statusCode = responseCode.getCode();
		this.message = responseCode.getMessage();
	}
	
	public BaseResponse(boolean isError, T data, ResponseCode responseCode, String serverMsg) {
		this.error = isError;
		this.data = data;
		this.statusCode = responseCode.getCode();
		this.message = responseCode.getMessage();
		this.serverMsg = serverMsg;
	}
	
	public static BaseResponse<?> getOkNullResponse() {
		return new BaseResponse<>(false, null, ResponseCode.OK);
	}
	
}
