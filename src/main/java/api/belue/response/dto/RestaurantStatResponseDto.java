package api.belue.response.dto;

public class RestaurantStatResponseDto {
	private double amount;
	private int ordersCount;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getOrdersCount() {
		return ordersCount;
	}
	public void setOrdersCount(int ordersCount) {
		this.ordersCount = ordersCount;
	}
}
