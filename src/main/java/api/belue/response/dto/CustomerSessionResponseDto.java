package api.belue.response.dto;

import java.util.List;

import api.belue.pojo.CustomerSessionPojo;

public class CustomerSessionResponseDto extends CustomerSessionPojo {
	private int checkinId;
	private int userId;
	private int restaurantId;
	private int sessionStatusCode;
	private int tableId;
	private Object taxes;
	private Object discounts;
	private String paidInfo;
	private String checkinTime;
	private String checkoutTime;
	private List<OrderResponseDto> orders;
	private CustomerResponseDto customer;
	private RestaurantTableResponseDto table;
	private RestuarantResponseDto restaurant;
	
	public int getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(int checkinId) {
		this.checkinId = checkinId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public int getSessionStatusCode() {
		return sessionStatusCode;
	}
	public void setSessionStatusCode(int sessionStatusCode) {
		this.sessionStatusCode = sessionStatusCode;
	}
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	public Object getTaxes() {
		return taxes;
	}
	public void setTaxes(Object taxes) {
		this.taxes = taxes;
	}
	public Object getDiscounts() {
		return discounts;
	}
	public void setDiscounts(Object discounts) {
		this.discounts = discounts;
	}
	public String getPaidInfo() {
		return paidInfo;
	}
	public void setPaidInfo(String paidInfo) {
		this.paidInfo = paidInfo;
	}
	public String getCheckinTime() {
		return checkinTime;
	}
	public void setCheckinTime(String checkinTime) {
		this.checkinTime = checkinTime;
	}
	public String getCheckoutTime() {
		return checkoutTime;
	}
	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}
	public List<OrderResponseDto> getOrders() {
		return orders;
	}
	public void setOrders(List<OrderResponseDto> orders) {
		this.orders = orders;
	}
	public CustomerResponseDto getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerResponseDto customer) {
		this.customer = customer;
	}
	public RestaurantTableResponseDto getTable() {
		return table;
	}
	public void setTable(RestaurantTableResponseDto table) {
		this.table = table;
	}
	public RestuarantResponseDto getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(RestuarantResponseDto restaurant) {
		this.restaurant = restaurant;
	}
}
