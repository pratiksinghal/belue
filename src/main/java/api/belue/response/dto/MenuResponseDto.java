package api.belue.response.dto;

import api.belue.pojo.MenuPojo;

public class MenuResponseDto extends MenuPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
