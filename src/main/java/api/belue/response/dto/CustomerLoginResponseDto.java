package api.belue.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CustomerLoginResponseDto {
	
	private String token;
	private CustomerResponseDto customerData;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public CustomerResponseDto getCustomerData() {
		return customerData;
	}
	public void setCustomerData(CustomerResponseDto customerData) {
		this.customerData = customerData;
	}
}
