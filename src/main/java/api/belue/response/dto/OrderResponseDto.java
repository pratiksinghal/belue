package api.belue.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import api.belue.pojo.OrderPojo;

@JsonInclude(Include.NON_NULL)
public class OrderResponseDto extends OrderPojo {
	private int id;
	private int checkinId;
	private int orderStatus;
	private String created;
	private String updated;
	private double amount;
	private double remainingAmount;
	private double taxAmount;
	private String orderName;
	private double discountAmount;
	private double amountWithoutDiscount;
	private CustomerResponseDto customer;
	private RestaurantTableResponseDto table;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(int checkinId) {
		this.checkinId = checkinId;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public CustomerResponseDto getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerResponseDto customer) {
		this.customer = customer;
	}

	public RestaurantTableResponseDto getTable() {
		return table;
	}

	public void setTable(RestaurantTableResponseDto table) {
		this.table = table;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getAmountWithoutDiscount() {
		return amountWithoutDiscount;
	}

	public void setAmountWithoutDiscount(double amountWithoutDiscount) {
		this.amountWithoutDiscount = amountWithoutDiscount;
	}
}
