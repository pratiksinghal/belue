package api.belue.response.dto;

import api.belue.pojo.CustomerRequestPojo;

public class CustomerRequestReponseDto extends CustomerRequestPojo {
	private int id;
	private int type;
	private int checkinId;
	private String requestMessage;
	private CustomerResponseDto customer;
	private RestaurantTableResponseDto table;
	private String created;
	private String updated;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(int checkinId) {
		this.checkinId = checkinId;
	}
	public String getRequestMessage() {
		return requestMessage;
	}
	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}
	public CustomerResponseDto getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerResponseDto customer) {
		this.customer = customer;
	}
	public RestaurantTableResponseDto getTable() {
		return table;
	}
	public void setTable(RestaurantTableResponseDto table) {
		this.table = table;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
}
