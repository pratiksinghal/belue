package api.belue.response.dto;

import api.belue.pojo.WalletTransactionPojo;

public class TransactionResponseDto extends WalletTransactionPojo {
	private int id;
	private double amount;
	private String transactionType;
	private String created;
	private String updated;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTransactionTypeCode() {
		return transactionType;
	}
	public void setTransactionTypeCode(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
}
