package api.belue.response.dto;

import api.belue.pojo.MenuCategoryPojo;

public class MenuCategoryResponseDto extends MenuCategoryPojo {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
