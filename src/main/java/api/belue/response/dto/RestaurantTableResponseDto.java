package api.belue.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import api.belue.pojo.RestaurantTablePojo;

public class RestaurantTableResponseDto extends RestaurantTablePojo {
	private int id;
	private String qrCodeUrl;
	private boolean isOccoupied;
	@JsonInclude(Include.NON_DEFAULT)
	private int checkInId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQrCodeUrl() {
		return qrCodeUrl;
	}

	public void setQrCodeUrl(String qrCodeUrl) {
		this.qrCodeUrl = qrCodeUrl;
	}

	public boolean isOccoupied() {
		return isOccoupied;
	}

	public void setOccoupied(boolean isOccoupied) {
		this.isOccoupied = isOccoupied;
	}

	public int getCheckInId() {
		return checkInId;
	}

	public void setCheckInId(int checkInId) {
		this.checkInId = checkInId;
	}
	
}
