package api.belue.response.dto;

public class RestaurantLoginResponseDto {
	private String token;
	private RestuarantResponseDto restaurantDetail;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public RestuarantResponseDto getRestaurantDetail() {
		return restaurantDetail;
	}
	public void setRestaurantDetail(RestuarantResponseDto restaurantDetail) {
		this.restaurantDetail = restaurantDetail;
	}
}
