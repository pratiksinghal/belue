package api.belue.response.dto;

import api.belue.pojo.CustomerPojo;

public class CustomerResponseDto extends CustomerPojo {
	
	private int id;
	private String phoneNumber;
	private String platform;
	private String notificationToken;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getNotificationToken() {
		return notificationToken;
	}
	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}
}
