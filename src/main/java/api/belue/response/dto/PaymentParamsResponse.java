package api.belue.response.dto;

import java.util.Map;

public class PaymentParamsResponse {

private Map<String, String> paramsMap;
	
	public Map<String, String> getParamsMap() {
		return paramsMap;
	}
	
	public void setParamsMap(Map<String, String> paramsMap) {
		this.paramsMap = paramsMap;
	}
}
