package api.belue.response.dto;

import api.belue.pojo.RestaurantNotificationPojo;

public class RestaurantNotificationResposeDto extends RestaurantNotificationPojo {
	private String created;
	private String updated;

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}
}
