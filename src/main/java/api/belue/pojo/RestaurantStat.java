package api.belue.pojo;

public class RestaurantStat {
	private double amount;
	private Long numOfOrders;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Long getNumOfOrders() {
		return numOfOrders;
	}

	public void setNumOfOrders(Long numOfOrders) {
		this.numOfOrders = numOfOrders;
	}
}
