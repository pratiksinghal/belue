package api.belue.pojo;

public class WalletTransactionPojo {
	private int customerId;
	private int customerSessionId;
	private int orderId;
	private int idType;
	private String paymentGatewayName;
	private Object metaData;

	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getCustomerSessionId() {
		return customerSessionId;
	}

	public void setCustomerSessionId(int customerSessionId) {
		this.customerSessionId = customerSessionId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getIdType() {
		return idType;
	}

	public void setIdType(int idType) {
		this.idType = idType;
	}

	public Object getMetaData() {
		return metaData;
	}

	public void setMetaData(Object metaData) {
		this.metaData = metaData;
	}
}
