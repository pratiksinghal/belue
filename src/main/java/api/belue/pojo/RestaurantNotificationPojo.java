package api.belue.pojo;

import api.belue.constants.FCMNotifTemplate;

public class RestaurantNotificationPojo {
	private int id;
	private FCMNotifTemplate notification;
	private int restaurantId;
	private boolean isRead;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FCMNotifTemplate getNotification() {
		return notification;
	}

	public void setNotification(FCMNotifTemplate notification) {
		this.notification = notification;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

}
