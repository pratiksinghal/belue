package api.belue.pojo;

import java.util.List;

public class RestaurantPojo {
	private String logo;
	private String name;
	private String address;
	private String username;
	private List<String> mobileNumbers;
	private List<String> images;
	private int paymentTypeCode;
	private String cuisine;
	private Object metaInfo;
	private List<Discount> discounts;
	
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<String> getMobileNumbers() {
		return mobileNumbers;
	}
	public void setMobileNumbers(List<String> mobileNumbers) {
		this.mobileNumbers = mobileNumbers;
	}
	public List<String> getImages() {
		return images;
	}
	public void setImages(List<String> images) {
		this.images = images;
	}
	public int getPaymentTypeCode() {
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(int paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	public Object getMetaInfo() {
		return metaInfo;
	}
	public void setMetaInfo(Object metaInfo) {
		this.metaInfo = metaInfo;
	}
	public List<Discount> getDiscounts() {
		return discounts;
	}
	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}
	
}
