package api.belue.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import api.belue.constants.DiscountType;

public class Discount {
	private double value;
	@JsonProperty("isPercentage")
	private boolean isPercentage;
	private DiscountType type;
	
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public boolean isPercentage() {
		return isPercentage;
	}
	public void setPercentage(boolean isPercentage) {
		this.isPercentage = isPercentage;
	}
	public DiscountType getType() {
		return type;
	}
	public void setType(DiscountType type) {
		this.type = type;
	}
}
