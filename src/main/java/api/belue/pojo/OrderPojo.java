package api.belue.pojo;

public class OrderPojo {
	
	private String data;
	private int paymentStatusCode;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getPaymentStatusCode() {
		return paymentStatusCode;
	}
	public void setPaymentStatusCode(int paymentStatusCode) {
		this.paymentStatusCode = paymentStatusCode;
	}
}
