package api.belue.constants;

public enum FileType {
	IMAGE(1),
	OTHER(10);
	
	private int code;

	public int getCode() {
		return code;
	}

	private FileType(int code) {
		this.code = code;
	}
	
	public static FileType getByCode(int code) {
		for(FileType fileType : FileType.values()) {
			if(fileType.getCode() == code) {
				return fileType;
			}
		}
		return null;
	}

}
