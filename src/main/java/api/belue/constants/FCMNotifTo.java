package api.belue.constants;

public enum FCMNotifTo {
	TO_MOB,
	TO_WEB;
}
