package api.belue.constants;

public interface PaymentParameters {

	String MOBILE_NO = "MOBILE_NO";
	String EMAIL = "EMAIL";
	String KEY_MERCHANT_ID = "MID";
	String KEY_ORDER_ID = "ORDER_ID";
	String KEY_CUSTOMER_ID = "CUST_ID";
	String KEY_INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
	String KEY_CHANNEL_ID = "CHANNEL_ID";
	String KEY_TXN_AMOUNT = "TXN_AMOUNT";
	String KEY_WEBSITE = "WEBSITE";
	String KEY_CALLBACK_URL = "CALLBACK_URL";
	String KEY_CHECKSUMHASH = "CHECKSUMHASH";

}
