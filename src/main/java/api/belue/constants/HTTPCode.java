package api.belue.constants;

public interface HTTPCode {
	public String OK               = "200";
	public String NO_CONTENT       = "204";
	public String BAD_REQUEST      = "400";
	public String UNAUTHORIZED     = "401";
	public String FORBIDDEN        = "403";
	public String CONFLICT         = "409";
	public String UNSUPPORTED      = "415";
	public String INVALID          = "422";
	public String SERVER_ERROR     = "500";
}