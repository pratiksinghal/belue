package api.belue.constants;

public interface Hqls {
	
	public String FROM = " from ";
	public String UPDATE = " update ";
	public String WHERE = " where ";
	public String EQUALS = " = ";
	
	// TODO add and move hqls here
	public interface RestaurantTable {
		String byRestaurantId = FROM + EntityDetails.RestaurantTable.ENTITY_NAME + WHERE + 
			EntityDetails.RestaurantTable.RESTAURANT_ID + EQUALS + ":id"	;
	}

}
