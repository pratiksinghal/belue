package api.belue.constants;

public class FCMNotifTemplate {
	protected int notifId;
	protected String title;
	protected String body;
	protected String message;
	protected String notifType;
	protected int referenceId;

	public int getNotifId() {
		return notifId;
	}

	public void setNotifId(int notifId) {
		this.notifId = notifId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void formatTitle(Object... args) {
		this.title = String.format(title, args);
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void formatBody(Object... args) {
		this.body = String.format(body, args);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNotifType() {
		return notifType;
	}

	public void setNotifType(String notifType) {
		this.notifType = notifType;
	}

	public static FCMNotifTemplate getCheckoutTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("Checked Out");
		response.setBody("You have succesfully checked out");
		response.setNotifType(String.valueOf(FCMNotifType.CHECK_OUT.getCode()));
		response.setMessage(response.getBody());
		return response;
	}

	public static FCMNotifTemplate getMoneyPaidTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("Payment successful");
		response.setBody("Amount %s");
		response.setNotifType(String.valueOf(FCMNotifType.PAYMENT.getCode()));
		response.setMessage(response.getBody());
		return response;
	}

	public static FCMNotifTemplate CheckInTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("Table booked");
		response.setBody("Table number %d booked");
		response.setNotifType(String.valueOf(FCMNotifType.CHECK_IN.getCode()));
		response.setMessage(response.getBody());
		return response;
	}

	public static FCMNotifTemplate OrderStatusTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("Order status");
		response.setBody("Your order is %s");
		response.setNotifType(String.valueOf(FCMNotifType.ORDER_STATUS.getCode()));
		response.setMessage(response.getBody());
		return response;
	}
	
	public static FCMNotifTemplate OrderCreatedTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("New Order");
		response.setBody("New order");
		response.setNotifType(String.valueOf(FCMNotifType.NEW_ORDER.getCode()));
		response.setMessage(response.getBody());
		return response;
	}
	
	public static FCMNotifTemplate getCustomerRequestTemplate() {
		FCMNotifTemplate response = new FCMNotifTemplate();
		response.setTitle("New Customer Request");
		response.setBody("%s");
		response.setNotifType(String.valueOf(FCMNotifType.CUSTOMER_REQUEST.getCode()));
		response.setMessage(response.getBody());
		return response;		
	}

	public int getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

}
