package api.belue.constants;

public enum SortOrder {
	ASC("ASC"),
	DESC("DESC");
	
	private String queryOrderString;

	public String getQueryOrderString() {
		return queryOrderString;
	}

	private SortOrder(String queryOrderString) {
		this.queryOrderString = queryOrderString;
	}
}
