package api.belue.constants;

public enum TransactionType {
	DEPOSIT(1),
	WITHDRAWL(2);
	
	private int code;
	
	
	public int getCode() {
		return code;
	}
	
	private TransactionType(int code) {
		this.code = code;
	}

	public static TransactionType getByCode(int code) {
		for(TransactionType transactionType : TransactionType.values()) {
			if(transactionType.getCode() == code) {
				return transactionType;
			}
		}
		return null;
	}
	
	public static final String COL_DEF = "ENUM('DEPOSIT','WITHDRAWL')";
}
