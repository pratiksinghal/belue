package api.belue.constants;

public enum FCMNotifType {
	
	CUSTOMER_REQUEST(1),
	NEW_ORDER(2),
	ORDER_STATUS(3),
	CHECK_IN(4),
	CHECK_OUT(5),
	PAYMENT(6);
	
	private int code;

	public int getCode() {
		return code;
	}

	private FCMNotifType(int code) {
		this.code = code;
	}
}
