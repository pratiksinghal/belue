package api.belue.constants;

public enum RoleType {
	RESTAURANT(1),
	CUSTOMER(2);
	
	private int code;

	public int getCode() {
		return code;
	}

	private RoleType(int code) {
		this.code = code;
	}
	
	public static RoleType getByCode(int code) {
		for(RoleType roleType : RoleType.values()) {
			if(roleType.getCode() == code) {
				return roleType;
			}
		}
		return null;
	}
}
