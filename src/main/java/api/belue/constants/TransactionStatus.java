package api.belue.constants;

public enum TransactionStatus {
	INITIATED,
	COMPLETED,
	FAILED;
	
	public static final String COL_DEF = "ENUM('INITIATED','COMPLETED', 'FAILED') DEFAULT 'INITIATED'";
}
