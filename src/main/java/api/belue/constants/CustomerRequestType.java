package api.belue.constants;

public enum CustomerRequestType {
	WAITER(1, "Waiter"),
	WATER(2, "Water"),
	BILL(3, "Bill");
	
	private int code;
	private String message;
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	private CustomerRequestType(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public static CustomerRequestType getByCode(int code) {
		for(CustomerRequestType customerRequestType : CustomerRequestType.values()) {
			if(customerRequestType.getCode() == code) {
				return customerRequestType;
			}
		}
		return null;
	}
}
