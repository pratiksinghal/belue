package api.belue.constants;

public interface PropertyConstants {
	
	String PROPERTY_MERCHANT_ID = "paytm.merchantId";
	String PROPERTY_INDUSTRY_TYPE_ID = "paytm.industryTypeId";
	String PROPERTY_CHANNEL_ID = "paytm.channelId";
	String PROPERTY_WEBSITE = "paytm.website";
	String PROPERTY_MERCHANT_KEY = "paytm.merchantKey";
	String PROPERTY_MOBILE_KEY = "paytm.mobileNo";
	String PROPERTY_EMAIL_KEY = "paytm.email";
	String PROPERTY_CALLBACK_URL = "paytm.callbackUrl";
	
}
