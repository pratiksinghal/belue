package api.belue.constants;

public enum PaymentIdType {
	SESSION_ID(1),
	ORDER_ID(2);
	private int code;

	public int getCode() {
		return code;
	}

	private PaymentIdType(int code) {
		this.code = code;
	}
	
	public static PaymentIdType getByCode(int code) {
		for(PaymentIdType paymentIdType : PaymentIdType.values()) {
			if(paymentIdType.getCode() == code) {
				return paymentIdType;
			}
		}
		return null;
	}
}
