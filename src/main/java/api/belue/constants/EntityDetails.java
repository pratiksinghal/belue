package api.belue.constants;

public interface EntityDetails {

	public interface Restaurant {
		public String TABLE_NAME = "restaurant";
		public String ENTITY_NAME = "RestaurantEntity";
		public String USERNAME = "username";
		public String TABLE_NUMBER = "tableNumber";
		public String ID = "id";
		public String AMOUNT = "amount";
		public String NOTIFICATION_TOKEN = "notificationToken";
	}
	
	public interface RestaurantTable {
		public String TABLE_NAME = "restaurantTable";
		public String ENTITY_NAME = "RestaurantTableEntity";
		public String RESTAURANT_ID = "restaurantId";
		public String TABLE_NUMBER = "tableNumber";
	}
	
	public interface Token {
		public String TABLE_NAME = "token";
		public String ENTITY_NAME = "TokenEntity";
		public String TOKEN_VALUE = "tokenValue";
		public String USER_ID = "userId";
		public String ROLE_TYPE = "roleTypeCode";
	}
	
	public interface Menu {
		public String TABLE_NAME = "menu";
		public String ENTITY_NAME = "MenuEntity";
		public String RESTAURANT_ID = "restaurantId";
	}
	
	public interface MenuCategory {
		public String TABLE_NAME = "menuCategory";
		public String ENTITY_NAME = "MenuCategoryEntity";
		public String MENU_ID = "menuId";
	}
	
	public interface MenuItem {
		public String TABLE_NAME = "menuItem";
		public String ENTITY_NAME = "MenuItemEntity";
		public String CATEGORY_ID = "categoryId";
		public String MENU_ID = "menuId";
	}
	
	public interface Device {
		public String TABLE_NAME = "device";
		public String ENTITY_NAME = "DeviceEntity";
		public String DEVICE_ID = "deviceId";
		public String PLATFORM = "platform";
	}
	
	public interface Customer {
		public String TABLE_NAME = "customer";
		public String ENTITY_NAME = "CustomerEntity";
		public String PHONE_NUMBER = "phoneNumber";
		public String ID = "id";
		public String NOTIFICATION_TOKEN = "notificationToken";
	}
	
	public interface CustomerSession {
		public String TABLE_NAME = "customerSession";
		public String ENTITY_NAME = "CustomerSessionEntity";
		public String CUSTOMER_ID = "customerId";
		public String RESTAURANT_ID = "restaurantId";
		public String TABLE_ID = "tableId";
		public String SESSION_STATUS_CODE = "sessionStatusCode";
		public String ID = "id";
	}
	
	public interface Order {
		public String TABLE_NAME = "customerOrder";
		public String ENTITY_NAME = "OrderEntity";
		public String CHECKIN_ID = "checkinId";
		public String STATUS = "statusCode";
		public String ID = "id";
		public String AMOUNT = "amount";
		public String CREATED = "created";
		public String UPDATED = "updated";
	}
	
	public interface Property {
		public String TABLE_NAME = "properties";
		public String ENTITY_NAME = "Property";
		public String PROPERTY_NAME = "propertyName";
		public String PROPERTY_VALUE = "propertyValue";
		public String UPDATED = "updated";
	}
	
	public interface CustomerRequest {
		public String TABLE_NAME = "customerRequests";
		public String ENTITY_NAME = "CustomerRequestEntity";
		public String STATUS = "status";
		public String CHECKIN_ID = "checkinId";
		public String ID = "id";
	}
	
	public interface Wallet {
		public String TABLE_NAME = "walletTransaction";
		public String ENTITY_NAME = "WalletTransactionEntity";
		public String ID = "id";
		public String CUSTOMER_ID = "customerId";
		public String TRANSACTION_TYPE = "transactionType";
		public String UNIQUE_ID = "uniqueId";
	}
	
	public interface RestaurantNotifications {
		public String TABLE_NAME = "restaurant_notifications";
		public String ENTITY_NAME = "RestaurantNotificationsEntity";
		public String RESTAURANT_ID = "restaurantId";
		public String IS_READ = "isRead";
		public String CREATED = "created";
	}
}
