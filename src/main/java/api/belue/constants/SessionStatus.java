package api.belue.constants;

public enum SessionStatus {
	CHECKED_IN(1), 
	CHECKED_OUT(2);
	
	private int code;

	public int getCode() {
		return code;
	}

	private SessionStatus(int code) {
		this.code = code;
	}
	
	public static SessionStatus getByCode(int code) {
		for(SessionStatus sessionStatus : SessionStatus.values()) {
			if(sessionStatus.getCode() == code) {
				return sessionStatus;
			}
		}
		return null;
	}

}
