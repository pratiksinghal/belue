package api.belue.constants;

public enum ResponseCode {
	
	OK(HTTPCode.OK, "Success"), 
	
	NOT_FOUND                (HTTPCode.NO_CONTENT, "Not found"), 
	NO_RESTAURANT_FOUND      (HTTPCode.NO_CONTENT, "No Restaurant found"), 
	NO_RESTAURANT_TABLE_FOUND(HTTPCode.NO_CONTENT, "No Restaurant Table found"),
	NO_CUSTOMER_FOUND        (HTTPCode.NO_CONTENT, "No customer found"),
	NO_CUSTOMER_SESSION_FOUND(HTTPCode.NO_CONTENT, "No customer session found"),
	NO_MENU_FOUND            (HTTPCode.NO_CONTENT, "No menu found"),
	NO_MENU_CATEGORY_FOUND   (HTTPCode.NO_CONTENT, "No menu category found"),
	NO_MENU_ITEM_FOUND       (HTTPCode.NO_CONTENT, "No menu item found"),
	NO_ORDER_FOUND           (HTTPCode.NO_CONTENT, "No order found"),
	NO_REQUEST_FOUND         (HTTPCode.NO_CONTENT, "No request found"),
	NO_GLOBAL_DATA_FOUND     (HTTPCode.NO_CONTENT, "No global data is present"),
	NO_NOTIFICATION_TOKEN_FOUND(HTTPCode.NO_CONTENT, "No notification token found"),
	NO_TRANSACTION_FOUND     (HTTPCode.NO_CONTENT, "No transaction found"),
	NO_NOTIFICATION_FOUND    (HTTPCode.NO_CONTENT, "No notification found"),
	
	BAD_REQUEST              (HTTPCode.BAD_REQUEST, "Request made is not correct"), 
	MISSING_VALUE            (HTTPCode.BAD_REQUEST, "Some value missing"), 
	NO_DATA_PRESENT          (HTTPCode.BAD_REQUEST, "No data is present"), 
	ID_NOT_PRESENT           (HTTPCode.BAD_REQUEST, "Id is not present"),
	FILE_NOT_PRESENT         (HTTPCode.BAD_REQUEST, "No file is present"),
	NAME_NOT_PRESENT         (HTTPCode.BAD_REQUEST, "Name is not present"),
	ADDRESS_NOT_PRESENT      (HTTPCode.BAD_REQUEST, "Address is not present"),
	MOBILE_NUMBER_NOT_PRESENT(HTTPCode.BAD_REQUEST, "Mobile number not present"),
	DESCRIPTION_NOT_PRESENT  (HTTPCode.BAD_REQUEST, "Description is not present"),
	USERNAME_NOT_PRESENT     (HTTPCode.BAD_REQUEST, "Username is not present"),
	PASSWORD_NOT_PRESENT     (HTTPCode.BAD_REQUEST, "Password is not present"),
	NEW_PASSWORD_NOT_PRESENT (HTTPCode.BAD_REQUEST, "New password is not present"),
	QRCODE_NOT_PRESENT       (HTTPCode.BAD_REQUEST, "QRCode is not present"),
	PHONE_NOT_EXISTS         (HTTPCode.BAD_REQUEST, "Phone number does not exists"),
	NOTIFICATION_TOKEN_NOT_PRESENT(HTTPCode.BAD_REQUEST, "Notification token is not present"),
	PLATFORM_NOT_PRESENT     (HTTPCode.BAD_REQUEST, "Platform is not present"),
	INVALID_JSON_FORMAT      (HTTPCode.BAD_REQUEST, "Json is not in valid format"),
	INVALID_PAYMENT_JSON_FORMAT(HTTPCode.BAD_REQUEST, "payment json is not in valid format"),
	PAYMENT_GATEWAY_NAME_NOT_PRESENT(HTTPCode.BAD_REQUEST, "Payment gateway name is not present"),
	TRANSACTION_METADATA_NOT_PRESENT(HTTPCode.BAD_REQUEST, "Transaction metadata is not present"),

	INVALID_TOKEN        (HTTPCode.UNAUTHORIZED, "Token is not valid"),
	TOKEN_NOT_PRESENT    (HTTPCode.UNAUTHORIZED, "Token is not present"),
	
	CUSTOMER_NOT_VERIFIED(HTTPCode.FORBIDDEN, "Customer is not verified yet"), 
	NOT_ALLOWED          (HTTPCode.FORBIDDEN, "You are not allowed to perform this request"),
	INCORRECT_PASSWORD   (HTTPCode.FORBIDDEN, "Password is not correct"),
	
	USERNAME_EXISTS        (HTTPCode.CONFLICT, "Username already exists"), 
	TABLE_OCCUPIED         (HTTPCode.CONFLICT, "Table is already booked"),
	TABLE_NUMBER_PRESENT   (HTTPCode.CONFLICT, "Table number already present"),
	ALREADY_CHECKED_IN     (HTTPCode.CONFLICT, "Customer is already checked in"), 
	ALREADY_CHECKED_OUT    (HTTPCode.CONFLICT, "Customer has already checked out"), 
	ORDER_ALREADY_PRESENT  (HTTPCode.CONFLICT, "Order is already placed"),
	ORDER_ALREADY_COMPLETED(HTTPCode.CONFLICT, "Order is already completed"),
	PHONE_ALREADY_EXISTS   (HTTPCode.CONFLICT, "Phone number already exists"),

	FILE_TYPE_NOT_SUPPORTED(HTTPCode.UNSUPPORTED, "This file type is not supported"),
	
	INVALID_ID                 (HTTPCode.INVALID, "Invalid id"),
	INVALID_RESTAURANT_ID      (HTTPCode.INVALID, "Restaurant Id is not valid"),
	INVALID_RESTAURANT_TABLE_ID(HTTPCode.INVALID, "Restaurant table Id is not valid"),
	INVALID_MENU_ID            (HTTPCode.INVALID, "Menu Id is not valid"),
	INVALID_MENU_CATEGORY_ID   (HTTPCode.INVALID, "Menu category Id is not valid"),
	INVALID_MENU_ITEM_ID       (HTTPCode.INVALID, "Menu item Id is not valid"),
	INVALID_CUSTOMER_ID        (HTTPCode.INVALID, "Customer Id is not valid"),
	INVALID_CHECKIN_ID         (HTTPCode.INVALID, "Checkin Id is not valid"),
	INVALID_ORDER_ID           (HTTPCode.INVALID, "Order Id is not valid"),
	INVALID_ORDER_SESSION_ID   (HTTPCode.INVALID, "Order Session Id is not valid"),
	INVALID_QR_CODE            (HTTPCode.INVALID, "QR code is not valid"),
	INVALID_DETAILS            (HTTPCode.INVALID, "Details are not valid"), 
	INVALID_OTP                (HTTPCode.INVALID, "OTP is not correct"),
	INVALID_ORDER_STATUS       (HTTPCode.INVALID, "Order status is not valid"),
	INVALID_DATA               (HTTPCode.INVALID, "Data is not valid"),
	INVALID_USER_TYPE          (HTTPCode.INVALID, "User type is not valid"),
	INVALID_FILE_TYPE          (HTTPCode.INVALID, "File type is not valid"),
	INVALID_PRICE              (HTTPCode.INVALID, "Price is not valid"),
	INVALID_QUANTITY           (HTTPCode.INVALID, "Quantity is not valid"),
	INVALID_TABLE_NUMBER       (HTTPCode.INVALID, "Table number is not valid"),
	INVALID_SEATING_CAPACITY   (HTTPCode.INVALID, "Seating capacity is not valid"),
	INVALID_SESSION_STATUS_CODE(HTTPCode.INVALID, "Session status code is not valid"),
	INVALID_PAYMENT_TYPE       (HTTPCode.INVALID, "Payment type is not valid"),
	INVALID_PAYMENT_STATUS     (HTTPCode.INVALID, "Payment status is not valid"),
	INVALID_LOGIN_DETAILS      (HTTPCode.INVALID, "Login details are not valid"),
	INVALID_REQUEST_TYPE       (HTTPCode.INVALID, "Request type is not valid"),
	INVALID_REQUEST_STATUS     (HTTPCode.INVALID, "Request status is not valid"),
	INVALID_REQUEST_ID         (HTTPCode.INVALID, "Request id is not valid"),
	INVALID_ID_TYPE            (HTTPCode.INVALID, "Id type is not valid"),
	INVALID_TRANSACTION_TYPE   (HTTPCode.INVALID, "Transaction type is not valid"),
	INVALID_GROUPING_CODE      (HTTPCode.INVALID, "Groupingby code is not valid"),
	INVALID_START_DATE         (HTTPCode.INVALID, "Start date is not valid"),
	INVALID_END_DATE           (HTTPCode.INVALID, "End date is not valid"),
	INVALID_NOTIFICATION_ID    (HTTPCode.INVALID, "Notification id is not valid"),
	INVALID_PAYMENT            (HTTPCode.INVALID, "Payment is not valid"),
	
	ERROR             (HTTPCode.SERVER_ERROR, "Internal Server Error"), 
	ERROR_IN_UPLOADING(HTTPCode.SERVER_ERROR, "Error in uploading content"),
	ERROR_IN_PARSING  (HTTPCode.SERVER_ERROR, "Error in parsing");
	
	private String code;
	private String message;
	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	private ResponseCode(String code, String message) {
		this.code = code;
		this.message = message;
	}
}
