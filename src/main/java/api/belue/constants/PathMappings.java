package api.belue.constants;

public interface PathMappings {

	String BASE = "/";
	String ALL = BASE + "all";
	String SAVE = BASE + "save";
	String UPDATE = BASE + "update";
	String ID = "id";
	String ID_PARAM = BASE + "{" + ID + "}";
	String LOGIN = BASE + "login";

	public interface HeaderValues {
		public String TOKEN = "token";
	}

	public interface Restaurant {
		String BASE_MAPPING = BASE + "restaurant";
		String CHANGE_PASSWORD = BASE + "changePassword";
		String STATS = ID_PARAM +  BASE + "stats";
		public String NOTIFICATION = BASE + "notifications";
		public String NOTIFICATION_ID = NOTIFICATION + ID_PARAM;
		public String SEND_PUSH_NOTIFICATION = BASE + "sendNotofication";
		public String FCM_TOKEN = BASE + "notificationToken";
	}

	public interface RestaurantTable {
		String BASE_MAPPING = BASE + "restaurantTable";
		String RESTAURANT = BASE + "restaurant" + ID_PARAM;
	}

	public interface Menu {
		public String BASE_MAPPING = BASE + "menu";
		String RESTAURANT = BASE + "restaurant" + ID_PARAM;
	}

	public interface MenuCategory {
		public String BASE_MAPPING = BASE + "menucategory";
		public String MENU = BASE + "menu" + ID_PARAM;
	}

	public interface MenuItem {
		public String BASE_MAPPING = BASE + "menuitem";
		public String MENU_CATEGORY = BASE + "menucategory" + ID_PARAM;
		public String MENU = BASE + "menu" + ID_PARAM;
	}
	
	public interface Customer {
		public String BASE_MAPPING = BASE + "customer";
		public String LOGIN = BASE + "login";
		public String PHONE_NUM_CHECK = BASE + "checkPhoneNumber";
		public String SEND_PUSH_NOTIFICATION = BASE + "sendNotofication";
		public String FCM_TOKEN = BASE + "notificationToken";
	}
	
	public interface CustomerSession {
		public String BASE_MAPPING = Customer.BASE_MAPPING + BASE + "session";
		public String CHECKOUT = BASE + "checkout";
		public String ID_PARAM_VAR = "id";
		public String USER_PARAM_VAR = "user";
		public String STATUS_PARAM_VAR = "status";
	}
	
	public interface Order {
		public String BASE_MAPPING = BASE + "order";
		public String ORDER_STATUS = BASE + "orderStatus" + ID_PARAM;
		public String ID_PARAM_VAR = "id";
		public String USER_PARAM_VAR = "user";
		public String STATUS_PARAM_VAR = "status";
		public String HISTORY_PARAM_VAR = "history";
		public String CHECKIN_ID = BASE + "chekinId" + ID_PARAM;
	}
	
	public interface FileUpload {
		public String BASE_MAPPING = BASE + "uploadFile";
		public String FILE_PARAM_VAR = "file";
		public String FILE_TYPE_PARAM_VAR = "filetype";
	}
	
	public interface PaymentMappings {
		public String BASE_MAPPING = BASE + "payment";
		public String GET_PAYTM_PARAMS = BASE + "getPaytmParams";
		public String TRANSACTIONS = BASE + "transactions";
		public String CUSTOMER_ID_PARAM = "customerId";
		public String TRANSACTION_TYPE_PARAM = "transactionType";
	}
	
	public interface CustomerRequest {
		public String BASE_MAPPING = BASE + "customerRequest";
		public String RESTAURANT_ID = BASE + "restaurant" + ID_PARAM;
		public String STATUS_PARAM_VAR = "status";
	}
	
	public interface Global {
		String BASE = "/globaldata";
	}
	
	

}
