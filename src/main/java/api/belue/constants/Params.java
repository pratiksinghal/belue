package api.belue.constants;

public interface Params {
	
	public interface APP {
		public String APP_PROPS_FILE = "application.properties";
		public String APP_PROD_PROPS_FILE = "application-prod.properties";
		public String STORE_ON_CLOUD = "storeQROnCloud";
		public String IS_PRODUCTION_MODE = "isProductionMode";
	}
	

	public interface Cloudinary {
		public String CLOUDINARY_CLIENT_PROPS_FILE = "cloudinary.properties";
		public String CLOUDINARY_UPLOAD_PROPS_FILE = "cloudinaryUpload.properties";
		public interface Client {
			public String CLOUD_NAME = "cloud_name";
			public String API_KEY = "api_key";
			public String API_SECRET_KEY = "api_secret";
		}
		public interface Upload {
			
		}
		
		public interface Result {
			public String RESULT_URL = "url";
		}
	}
	
	public interface QR {
		public String UPLOAD_URL = "https://api.qrserver.com/v1/create-qr-code/?size=150*150&data=";
		public String FILE_NAME = "tempqr.png";
	}
	
	public interface Fcm {

		public String FCM_PROPS_FILE = "fcm.properties";
		public String AUTH_KEY = "authKey";
		public String URL = "fmcUrl";

	}
}
