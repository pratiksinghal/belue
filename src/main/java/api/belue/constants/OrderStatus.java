package api.belue.constants;

public enum OrderStatus {
	ALL(0, "All"),
	INITIATED(1, "Order initiated"),
	IN_PROGRESS(2, "Order in progress"),
	COMPLETED(3, "Order completed"),
	CANCELLED(4, "Order cancelled");
	
	private int code;
	private String message;
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	
	private OrderStatus(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public static OrderStatus getByCode(int code) {
		for(OrderStatus orderStatus : OrderStatus.values()) {
			if(orderStatus.getCode() == code) {
				return orderStatus;
			}
		}
		return null;
	}
}
