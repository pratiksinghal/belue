package api.belue.constants;

public enum CustomerRequestStatus {
	INITIATED(1),
	CANCELLED(2),
	COMPLETED(3);
	
	private int code;

	public int getCode() {
		return code;
	}

	private CustomerRequestStatus(int code) {
		this.code = code;
	}
	
	public static CustomerRequestStatus getByCode(int code) {
		for(CustomerRequestStatus customerRequestStatus : CustomerRequestStatus.values()) {
			if(customerRequestStatus.getCode() == code) {
				return customerRequestStatus;
			}
		}
		return null;
	}
}
