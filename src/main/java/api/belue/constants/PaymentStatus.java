package api.belue.constants;

public enum PaymentStatus {
	PAID(1),
	PENDING(2),
	PARTIAL_PAID(3);
	
	private int code;

	public int getCode() {
		return code;
	}

	private PaymentStatus(int code) {
		this.code = code;
	}
	
	public static PaymentStatus getByCode(int code) {
		for(PaymentStatus paymentStatus : PaymentStatus.values()) {
			if(paymentStatus.getCode() == code) {
				return paymentStatus;
			}
		}
		return null;
	}
}
