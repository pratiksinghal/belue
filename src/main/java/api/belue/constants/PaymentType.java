package api.belue.constants;

public enum PaymentType {
	PREPAID(1),
	POSTPAID(2);
	
	private int code;

	public int getCode() {
		return code;
	}

	private PaymentType(int code) {
		this.code = code;
	}

	public static PaymentType getByCode(int code) {
		for(PaymentType paymentType : PaymentType.values()) {
			if(paymentType.getCode() == code) {
				return paymentType;
			}
		}
		return null;
	}
}
